from __future__ import annotations

import uuid
from dataclasses import dataclass
from datetime import datetime, timedelta
from typing import TYPE_CHECKING, Optional, Tuple

from sqlalchemy import not_, and_, false

from aura.api.commands import gcmd_activate_user_venue_event
from aura.models import RestaurantEventPurchase, RestaurantEvent, UserVenueStatus, RestaurantEventStatus, VenueStatus
from framework.api import CommandBase, CommandParametersBase, NotFoundError, get_service_db, UnprocessableEntityError

if TYPE_CHECKING:
    pass


@dataclass
class Response:
    user_venue_event: gcmd_activate_user_venue_event.UserVenueEvent


class Parameters(CommandParametersBase):
    user_id: int
    instance_id: str
    item: str
    user_venue_status_id: int
    venue_event_id: int
    activation_uuid: uuid.UUID


class Exceptions:
    class UserVenueStatusNotFoundException(NotFoundError):
        pass

    class VenueEventNotFoundException(NotFoundError):
        pass

    class ItemNotAvailableForVenueEvent(UnprocessableEntityError):
        pass

    class ItemNotGrantedByVenueStatus(UnprocessableEntityError):
        pass

    class VenueEventNotStarted(UnprocessableEntityError):
        pass

    class UserVenueStatusNotValid(UnprocessableEntityError):
        pass


def get_restaurant_event_purchase_item(venue_event_id: int, user_id: int, item: str) -> Optional[
    RestaurantEventPurchase]:
    return get_service_db().session.query(RestaurantEventPurchase).filter(
        RestaurantEventPurchase.restaurantEventId == venue_event_id).filter(
        RestaurantEventPurchase.item == item).filter(
        RestaurantEventPurchase.userId == user_id).filter(not_(RestaurantEventPurchase.isDeleted)).one_or_none()


def get_venue_event(venue_event_id) -> RestaurantEvent:
    return get_service_db().session.query(RestaurantEvent).filter(
        RestaurantEvent.id == venue_event_id).filter(
        not_(RestaurantEvent.isDeleted)).filter(
        not_(RestaurantEvent.status == RestaurantEventStatus.HIDDEN)).one_or_none()


def get_user_venue_status(user_venue_status_id, user_id: int) -> UserVenueStatus:
    return get_service_db().session.query(UserVenueStatus).join(VenueStatus,
                                                                and_(
                                                                    UserVenueStatus.venueStatusId == VenueStatus.id,
                                                                    VenueStatus.isDeleted is False,
                                                                    not_(and_(
                                                                        VenueStatus.type == VenueStatus.Type.PRIVATE,
                                                                        VenueStatus.visible == false()))
                                                                )).filter(
        UserVenueStatus.venueStatusId == user_venue_status_id).filter(
        UserVenueStatus.userId == user_id).filter(
        not_(UserVenueStatus.isDeleted)).order_by(
        UserVenueStatus.id.desc()).limit(1).one_or_none()


def get_venue_items(parameters: Parameters) -> Tuple[RestaurantEvent, VenueStatus, UserVenueStatus]:
    if not (venue_event := get_venue_event(parameters.venue_event_id)):
        raise Exceptions.VenueEventNotFoundException(
            log_message=f'Venue event not found or hidden: {parameters.venue_event_id}')

    if not (user_venue_status := get_user_venue_status(parameters.user_venue_status_id, parameters.user_id)):
        raise Exceptions.UserVenueStatusNotFoundException(
            log_message=f'User venue status not found: {parameters.user_venue_status_id}')

    venue_status: VenueStatus = get_service_db().session.query(VenueStatus).get(user_venue_status.venueStatusId)

    return venue_event, venue_status, user_venue_status


class Command(CommandBase[Parameters, Response]):

    @staticmethod
    def _command(parameters: Parameters) -> Response:
        venue_event, venue_status, user_venue_status = get_venue_items(parameters)

        if not (parameters.item in venue_event.pricing.keys()):
            raise Exceptions.ItemNotAvailableForVenueEvent(
                log_message=f'Item {parameters.item} is not available for the venue event: {parameters.venue_event_id}')

        if venue_status.flags.get('grant' + parameters.item.capitalize()) == False:  # noqa
            raise Exceptions.ItemNotGrantedByVenueStatus(
                log_message=f'Item {parameters.item} is not granted by venue status: {venue_status.id}')

        if venue_event.startTime - timedelta(minutes=30) > datetime.now() or datetime.now() > venue_event.endTime:
            raise Exceptions.VenueEventNotStarted(
                log_message=f'Venue event not ongoing, start time {venue_event.startTime},'
                            f' end time {venue_event.endTime}, event id: {venue_event.id}')

        if user_venue_status.validFrom > datetime.now() or datetime.now() > user_venue_status.validThrough:
            raise Exceptions.UserVenueStatusNotValid(
                log_message=f'User venue status not valid at the moment, valid from {user_venue_status.validFrom},'
                            f' valid through {user_venue_status.validThrough}, id: {user_venue_status.id}')

        venue_event_item_purchase = get_restaurant_event_purchase_item(venue_event.id, parameters.user_id,
                                                                       item=parameters.item)

        if not venue_event_item_purchase:
            granted_venue_event_item_purchase = RestaurantEventPurchase(
                userId=parameters.user_id,
                restaurantEventId=venue_event.id,
                item=parameters.item,
                quantity=1,
                properties={'source': parameters.source.origin, 'venueStatusId': venue_status.id,
                            'userVenueStatusId': user_venue_status.id}
            )
            get_service_db().session.add(granted_venue_event_item_purchase)
            get_service_db().session.commit()

        command_result: gcmd_activate_user_venue_event.Response = gcmd_activate_user_venue_event.Command(
            gcmd_activate_user_venue_event.Parameters(
                user_id=parameters.user_id,
                instance_id=parameters.instance_id,
                user_venue_event_id=venue_event.id,
                item=parameters.item,
                activation_uuid=parameters.activation_uuid,
                source=parameters.source
            )
        ).execute()

        user_venue_event = command_result.user_venue_event
        return Response(user_venue_event=user_venue_event)
