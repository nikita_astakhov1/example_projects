import marshmallow
from flask import Blueprint
from flask import request

from commands import gcmd_activate_user_venue_status_event_item
from api.util_generic import encode_ids, short_generic_hashids, get_source_from_request
from framework import api
from .util_user_auth import Authentication, check_authentication

module = Blueprint('web_api_session_venue', __name__)


class UserVenueStatusEventItemActivationSchema(marshmallow.Schema):
    activationUuid = marshmallow.fields.UUID(required=True, allow_none=False)


@module.route('/user/status/<hashid:user_venue_status_id>/event/<hashid:venue_event_id>/<string:item>/activate',
              methods=['POST'])
@api.authenticate(check_authentication)
@api.validate_body_with(UserVenueStatusEventItemActivationSchema)
def venue_status_event_item_activation(authentication: Authentication, user_venue_status_id, venue_event_id, item,
                                       json_body):
    user_id = authentication.user_id

    command_result: gcmd_activate_user_venue_status_event_item.Response = \
        gcmd_activate_user_venue_status_event_item.Command(
            gcmd_activate_user_venue_status_event_item.Parameters(
                user_id=user_id,
                instance_id=authentication.instance_id,
                item=item,
                user_venue_status_id=user_venue_status_id,
                venue_event_id=venue_event_id,
                activation_uuid=json_body.activationUuid,
                source=api.CommandSource(origin=get_source_from_request(),
                                         ip_address=request.remote_addr)
            )
        ).execute()

    user_venue_event = command_result.user_venue_event
    return api.json_response(
        {'userVenueEvent': encode_ids(user_venue_event.__dict__, hashids_function=short_generic_hashids)})
