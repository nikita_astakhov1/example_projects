

@module.route('/<hashid:organization_id>/members/list', methods=['GET'])
@api.authenticate(check_authentication, authorization_function=check_organization_permissions)
def list_members(authorization, **kwargs):
    venues = authorization.current_organization.get('venues', [])
    venue = request.args.get('venue', type=str)
    venues = process_venue_id(venue, venues)
    page = request.args.get('page', default=1, type=int)
    columns = process_str_arg_list(request.args.getlist('process_str_arg_list'))
    sorting = request.args.get('sorting', default='joined', type=str)
    ordering = request.args.get('ordering', default='desc', type=str)
    name = request.args.get('name', default='', type=str)
    venue_status = process_id_arg_list(request.args.getlist('venueStatus'))
    members = filter_org_members(venues, None, venue_status, None, None,
                                 users_filtering_object(page, columns, sorting, process_ordering(ordering),
                                                        name.rstrip().lower()))
    data = compile_org_members_basics(venues, members[0])
    data['pagination'] = {'total_members': members[1], 'page': page, 'total_pages': ceil(members[1] / 100)}
    return api.json_response(encode_ids(data, authorization.current_organization.get('id', None)))


def filter_org_members(venues: List[int], benefits: Optional[List[int]], statuses: Optional[List[int]],
                       products: Optional[List[int]], events: Optional[List[int]],
                       filters: users_filtering_object) -> Tuple[list, int]:
    benefits = [] if benefits is None else benefits
    statuses = [] if statuses is None else statuses
    products = [] if products is None else products
    events = [] if events is None else events
    offset = (filters.page - 1) * 100
    select_organization_members_sql = """SELECT id_s, count(id_s) OVER () AS full_count
FROM (SELECT DISTINCT u.id                                                                                   AS id_s,
                      CASE
                          WHEN :sorting = 'joined' THEN COALESCE((SELECT mem."memberSince"
                                                                  FROM (SELECT MIN(rua."addedTime")                        as "memberSince",
                                                                               MAX(rua.properties ->> 'partnerEmailAlias') as "email"
                                                                        FROM restaurant_user_attribute rua
                                                                        WHERE rua.user_id = u.id
                                                                          AND rua.attribute = 'JOIN'
                                                                          AND NOT rua.is_deleted
                                                                          AND rua.restaurant_id = ANY (:venues)
                                                                        GROUP BY rua.user_id, rua.restaurant_id
                                                                        LIMIT 1) as mem),
                                                                 NULL) END                                   AS "memberSince",
                      CASE
                          WHEN :sorting = 'name' THEN CASE
                                                          WHEN u."firstName" IS NOT NULL
                                                              THEN CONCAT(u."firstName", ' ', u."lastName")
                                                          ELSE u."display_name" END END                      as "name",
                      CASE WHEN :sorting = 'cards' THEN COUNT(DISTINCT uvs.id) END                           as "cardsCounts",
                      CASE
                          WHEN :sorting = 'purchases' THEN COALESCE((SELECT COALESCE(SUM(pt.amount), 0)
                                                                     FROM payment_transaction pt
                                                                     WHERE pt."user_id" = u.id
                                                                       AND pt."status" = 'PAYMENT_SUCCESS'
                                                                       AND (pt.order_metadata ->> 'venue_id')::integer = ANY (:venues)
                                                                       AND NOT pt.is_deleted
                                                                       AND pt."reverted_amount" = 0),
                                                                    0) END                                   as "totalSpent",
                      CASE WHEN :sorting = 'benefits' THEN COUNT(DISTINCT bea.id) END                        as "benefitCounts"
      FROM "user" u
               LEFT JOIN user_venue_status uvs
               JOIN venue_status vs ON uvs."venueStatusId" = vs.id
          AND vs."restaurantId" = ANY (:venues) ON u.id = uvs."userId" AND (uvs."validThrough" >= NOW()
          OR uvs."validThrough" IS NULL) AND NOT uvs.is_deleted
               LEFT JOIN restaurant_user_attribute rua ON rua."restaurant_id" = ANY (:venues) AND u.id = rua.user_id
          AND rua.attribute = 'JOIN' AND NOT rua.is_deleted
          AND rua.restaurant_id = ANY (:venues) AND rua."removedTime" IS NULL AND NOT rua.is_deleted
               LEFT JOIN benefit_activation bea
               JOIN benefit be ON bea."benefit_id" = be.id
          AND be."restaurantId" = ANY (:venues) ON rua."user_id" = bea."user_id" AND NOT bea.is_deleted
               LEFT JOIN user_venue_product_activation uvpa
               JOIN user_venue_product uvp ON uvpa."userVenueProductId" = uvp.id
               JOIN venue_product vp ON uvp."venueProductId" = vp.id AND vp."restaurantId" = ANY (:venues)
                    ON rua."user_id" = uvp."userId" AND NOT uvpa.is_deleted AND NOT uvp.is_deleted AND NOT vp.is_deleted
               LEFT JOIN restaurant_event_purchase rep
               JOIN restaurant_event re ON rep."restaurant_event_id" = re.id
          AND re."restaurantId" = ANY (:venues)
                    ON rua."user_id" = rep."user_id" AND NOT rep.is_deleted AND NOT re.is_deleted
      WHERE NOT u.is_deleted
        AND (uvs."userId" IS NOT NULL OR rua.user_id IS NOT NULL OR bea."user_id" IS NOT NULL
          OR uvpa."userVenueProductId" IS NOT NULL OR rep."user_id" IS NOT NULL)
        AND CASE
                WHEN length(:name) > 0 THEN CASE
                                                WHEN u."firstName" IS NOT NULL THEN (lower(u."firstName") ~ :name
                                                    OR lower(u."lastName") ~ :name)
                                                ELSE lower(u."display_name") ~ :name END
                ELSE TRUE = TRUE END
        AND CASE WHEN array_length(CAST(:statuses AS int[]), 1) > 0 THEN vs.id = any (:statuses) ELSE TRUE = TRUE END
        AND CASE WHEN array_length(CAST(:benefits AS int[]), 1) > 0 THEN be.id = any (:benefits) ELSE TRUE = TRUE END
        AND CASE WHEN array_length(CAST(:products AS int[]), 1) > 0 THEN vp.id = any (:products) ELSE TRUE = TRUE END
        AND CASE WHEN array_length(CAST(:events AS int[]), 1) > 0 THEN re.id = any (:events) ELSE TRUE = TRUE END
      GROUP BY u.id
      ORDER BY CASE
                   WHEN :sorting = 'joined' THEN COALESCE((SELECT mem."memberSince"
                                                           FROM (SELECT MIN(rua."addedTime")
                                                                                                                    as "memberSince",
                                                                        MAX(rua.properties ->> 'partnerEmailAlias') as "email"
                                                                 FROM restaurant_user_attribute rua
                                                                 WHERE rua.user_id = u.id
                                                                   AND rua.attribute = 'JOIN'
                                                                   AND NOT rua.is_deleted
                                                                   AND rua.restaurant_id = ANY (:venues)
                                                                 GROUP BY rua.user_id, rua.restaurant_id
                                                                 LIMIT 1) as mem),
                                                          Null) END {order},
          CASE WHEN :sorting = 'purchases' THEN COALESCE((SELECT COALESCE(SUM(pt.amount), 0)
        FROM payment_transaction pt WHERE pt."user_id" = u.id AND pt."status" = 'PAYMENT_SUCCESS'
        AND (pt.order_metadata ->> 'venue_id')::integer = ANY (:venues) AND NOT pt.is_deleted
        AND pt."reverted_amount" = 0), 0) END {order}, CASE WHEN :sorting = 'benefits'
        THEN COUNT(DISTINCT bea.id) END {order}, CASE WHEN :sorting = 'cards' THEN COUNT(DISTINCT uvs.id) END {order},
CASE WHEN :sorting = 'name' THEN CASE WHEN u."firstName" IS NOT NULL THEN CONCAT(u."firstName", ' ', u."lastName")
                                  ELSE u."display_name" END END {order}) id_s
LIMIT 100 OFFSET :offset
""".format(order=filters.ordering)
    data = api.get_service_db().engine.execute(
        text(select_organization_members_sql), venues=venues, benefits=benefits, statuses=statuses, products=products,
        events=events, sorting=filters.sorting, offset=offset, name=filters.search_name
    ).fetchall()
    try:
        total = data[0][1]
    except IndexError:
        total = 0
    return [row[0] for row in data], total


def compile_org_members_basics(venues: List[int], members: List[int]) -> dict:
    select_organization_members_sql = """SELECT COALESCE((SELECT json_agg(members)
                 FROM (SELECT u.id                                                                 AS id,
                              CASE
                                  WHEN u."firstName" IS NOT NULL THEN CONCAT(u."firstName", ' ', u."lastName")
                                  ELSE u."display_name" END                                        AS "name",
                              u.last_seen                                                          AS "lastSeen",
                              u.birthday                                                           AS "birthDay",
                              u.gender,
                              u."profileImages",
                              COALESCE((select json_agg(stat)
                                        FROM (SELECT uvs."venueStatusId" as id,
                                                     uvs."validFrom"     as "validFrom",
                                                     uvs."validThrough"  as "validThrough",
                                                     uvs."origin"        as "origin"
                                              FROM user_venue_status uvs
                                                       JOIN venue_status vs
                                                            ON uvs."venueStatusId" = vs.id AND vs."restaurantId" = ANY (:venues)
                                              WHERE uvs."userId" = u.id
                                                AND (uvs."validThrough" >= NOW() OR uvs."validThrough" IS NULL)
                                                AND NOT uvs.is_deleted) stat)
                                  , '[]')                                                          AS "venueStatuses",
                              COALESCE((select json_agg(mem)
                                        FROM (SELECT rua.restaurant_id                           as id,
                                                     MIN(rua."addedTime")                        as "memberSince",
                                                     MAX(rua.properties ->> 'partnerEmailAlias') as email
                                              FROM restaurant_user_attribute rua
                                              WHERE rua.user_id = u.id
                                                AND rua.attribute = 'JOIN'
                                                AND rua.restaurant_id = ANY (:venues)
                                                AND NOT rua.is_deleted
                                              GROUP BY rua.user_id, rua.restaurant_id) mem),
                                       '[]')                                                       AS "venueMemberships",
                              (SELECT COALESCE(COUNT(DISTINCT bea.id), 0)
                               FROM benefit_activation bea
                                        JOIN benefit be
                                             ON bea."benefit_id" = be.id AND be."restaurantId" = ANY (:venues) AND
                                                bea."user_id" = u.id AND NOT bea.is_deleted)
                                                                                                   AS "totalBenefitsActivations",
                              (SELECT COALESCE(SUM(pt.amount), 0)
                               FROM payment_transaction pt
                               WHERE pt."user_id" = u.id
                                 AND (pt.order_metadata ->> 'venue_id')::integer = ANY (:venues)
                                 AND NOT pt.is_deleted
                                 AND pt."status" = 'PAYMENT_SUCCESS'
                                 AND pt."reverted_amount" = 0)                                     AS "totalPurchases"
                       FROM "user" u
                       WHERE NOT u.is_deleted
                         AND CASE
                                 WHEN array_length(CAST(:user_ids AS int[]), 1) > 0 THEN u.id = ANY (:user_ids)
                                 ELSE TRUE = FALSE END
                       GROUP BY u.id
                       ORDER BY array_position(:user_ids, u.id)) members),
                '[]') AS members"""
    data = api.get_service_db().engine.execute(text(select_organization_members_sql),
                                               venues=venues,
                                               user_ids=members
                                               ).fetchall()
    result = dict(next(iter(data), {}))
    return result


@module.route('/<hashid:organization_id>/members/<hashid:u_id>/timeline', methods=['GET'])
@api.authenticate(check_authentication, authorization_function=check_organization_permissions)
@api.validate_request_args(MembersSchema)
def get_user_timeline(authentication, authorization, u_id, request_args, **kwargs):
    venues = authorization.current_organization.get('venues', [])
    timeline = compile_timeline(venues, u_id)
    return api.json_response(encode_ids(timeline, authorization.current_organization.get('id', None)))


def compile_timeline(venues, member_id):
    select_member_timeline_sql = """SELECT COALESCE((SELECT json_agg(timeline)
                 FROM (SELECT u.id                                                                               AS id,
                              (SELECT COALESCE(SUM(pt.amount), 0)
                               FROM payment_transaction pt
                               WHERE pt."user_id" = :user_id
                                 AND (pt.order_metadata ->> 'venue_id')::integer = ANY (:venues)
                                 AND NOT pt.is_deleted)                                                          AS "totalPurchases",
                              COALESCE(COUNT(DISTINCT bea.id) FILTER (WHERE bea.id IS NOT NULL),
                                       0)                                                                        AS "totalBenefitsActivations",
                              COALESCE(json_agg(DISTINCT
                                       jsonb_build_object('id', uvs."venueStatusId", 'restaurantId', vs."restaurantId",
                                                          'pricePayed', COALESCE(pt."amount", 0), 'type', vs."type",
                                                          'actionDate', uvs."validFrom",
                                                          'origin', uvs."origin", 'isSubscription',
                                                          uvs."isSubscription")) FILTER
                                           (WHERE uvs.id IS NOT NULL and case
                                                                             when pt.id is not null then
                                                                                     (pt.order_metadata -> 'VENUE_STATUS_PURCHASE_1' ->> 'venue_status_id')::integer =
                                                                                     uvs."venueStatusId" END),
                                       '[]')                                                                     AS "venueStatuses",
                              COALESCE(json_agg(DISTINCT jsonb_build_object('id', rua.restaurant_id,
                                                                            'actionDate', rua."addedTime"))
                                       FILTER (WHERE rua.id IS NOT NULL),
                                       '[]')                                                                     AS "venueMemberships",
                              COALESCE(json_agg(DISTINCT jsonb_build_object('id', bea."benefit_id",
                                                                            'restaurantId', be."restaurantId",
                                                                            'actionDate', bea."timestamp")) FILTER
                                           (WHERE bea.id IS NOT NULL),
                                       '[]')                                                                     AS "venueBenefitsActivations",
                              COALESCE(json_agg(DISTINCT jsonb_build_object('id', uvp."venueProductId", 'restaurantId',
                                                                            vp."restaurantId", 'pricePayed',
                                                                            COALESCE(pt."amount", 0), 'actionDate',
                                                                            uvp."created_at")) FILTER
                                           (WHERE uvp.id IS NOT NULL
                                      AND uvp."venueProductId" =
                                          (pt.order_metadata -> 'VENUE_PRODUCT_PURCHASE_1' ->> 'venue_product_id')::integer),
                                       '[]')                                                                     AS "venueProductPurchase",
                              COALESCE(json_agg(DISTINCT jsonb_build_object('id', uvp."venueProductId",
                                                                            'restaurantId', vp."restaurantId",
                                                                            'actionDate', uvpa."activationTime")) FILTER
                                           (WHERE uvpa.id IS NOT NULL AND uvp.id IS NOT NULL AND
                                                  uvpa."activationTime" IS NOT NULL),
                                       '[]')                                                                     AS "venueProductActivation",
                              COALESCE(json_agg(DISTINCT jsonb_build_object('id', rep."restaurant_event_id",
                                                                            'restaurantId', re."restaurantId",
                                                                            'pricePayed', COALESCE(pt."amount", 0),
                                                                            'actionDate', rep."created_at", 'item',
                                                                            rep."item", 'quantity', rep."quantity"))
                                       FILTER
                                           (WHERE rep.id IS NOT NULL
                                           AND rep."restaurant_event_id" =
                                               (pt.order_metadata -> 'EVENT_PURCHASE_1' ->> 'restaurant_event_id')::integer),
                                       '[]')                                                                     AS "venueEventPurchase",
                              COALESCE(json_agg(DISTINCT jsonb_build_object('id', rep."restaurant_event_id",
                                                                            'restaurantId', re."restaurantId",
                                                                            'actionDate', rep."usedTimestamp", 'item',
                                                                            rep."item")) FILTER
                                           (WHERE rep.id IS NOT NULL AND rep."usedTimestamp" IS NOT NULL),
                                       '[]')                                                                     AS "venueEventActivation"
                       FROM "user" u
                                LEFT JOIN user_venue_status uvs
                                JOIN venue_status vs
                                     ON uvs."venueStatusId" = vs.id AND vs."restaurantId" = ANY (:venues)
                                     ON u.id = uvs."userId"
                                         AND NOT uvs.is_deleted
                                LEFT JOIN restaurant_user_attribute rua ON u.id = rua.user_id AND rua.attribute = 'JOIN'
                           AND rua.restaurant_id = ANY (:venues) AND NOT rua.is_deleted
                                LEFT JOIN benefit_activation bea
                                JOIN benefit be
                                     ON bea."benefit_id" = be.id AND be."restaurantId" = ANY (:venues)
                                     ON u.id = bea."user_id" AND NOT bea.is_deleted
                                LEFT JOIN user_venue_product_activation uvpa
                                JOIN user_venue_product uvp ON uvpa."userVenueProductId" = uvp.id
                                JOIN venue_product vp
                                     ON uvp."venueProductId" = vp.id AND vp."restaurantId" = ANY (:venues)
                                     ON u.id = uvp."userId" AND NOT uvpa.is_deleted AND NOT uvp.is_deleted
                                LEFT JOIN restaurant_event_purchase rep
                                JOIN restaurant_event re
                                     ON rep."restaurant_event_id" = re.id AND re."restaurantId" = ANY (:venues)
                                     ON u.id = rep."user_id"
                                         AND NOT rep.is_deleted
                                LEFT JOIN payment_transaction pt ON u.id = pt."user_id"
                           AND (pt.order_metadata ->> 'venue_id')::integer = ANY (:venues) AND NOT pt.is_deleted
                           AND pt."status" = 'PAYMENT_SUCCESS'
                       WHERE u.id = :user_id
                         AND NOT u.is_deleted
                         AND (uvs."userId" IS NOT NULL OR rua.user_id IS NOT NULL
                           OR bea."user_id" IS NOT NULL OR uvp."userId" IS NOT NULL OR rep."user_id" IS NOT NULL
                           OR pt."user_id" IS NOT NULL)
                       GROUP BY u.id, rua."properties", rua."created_at"
                       ORDER BY rua."created_at" ASC) timeline), '[]')
           AS timeline_data
    """
    data = api.get_service_db().engine.execute(text(select_member_timeline_sql), venues=venues,
                                               user_id=member_id).fetchall()
    return dict(next(iter(data), {}))

