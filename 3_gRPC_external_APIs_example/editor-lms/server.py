import grpc
import logging
from concurrent import futures
from signal import signal, SIGTERM

from protos.editor import service_pb2_grpc

from app.interceptors.requests_logger import RequestsLogger
from app.interceptors.error_logger import ErrorLogger
from app.servicers.general import EditorGeneralServicer
from app.servicers.magic_video import EditorMagicConverterServicer

import sentry_sdk
from app.config import settings, environment


def serve():
    interceptors = [ErrorLogger(), RequestsLogger()]

    server = grpc.server(
        futures.ThreadPoolExecutor(max_workers=10),
        interceptors=interceptors
    )
    service_pb2_grpc.add_EditorServicer_to_server(
        EditorGeneralServicer(), server
    )
    service_pb2_grpc.add_EditorMagicConverterServicer_to_server(
        EditorMagicConverterServicer(), server
    )

    server.add_insecure_port('[::]:50057')
    server.start()
    logging.info("Server started and listening on [::]:50057")
    logging.info(f" started in {environment} environment")

    def handle_sigterm(*_):
        logging.warn("Received shutdown signal")
        all_rpcs_done_event = server.stop(30)
        all_rpcs_done_event.wait(30)
        logging.warn("Shut down gracefully")

    signal(SIGTERM, handle_sigterm)
    try:
        server.wait_for_termination()
    except KeyboardInterrupt:
        handle_sigterm()


if __name__ == '__main__':
    logging.basicConfig(level="DEBUG")

    if settings.sentry_enabled:
        logging.info("sentry enabled")
        sentry_sdk.init(settings.sentry_uri, traces_sample_rate=0.1)

    serve()
