import logging
import grpc
import queue
import time
import threading

from protos.editor import service_pb2_grpc
from protos.editor.Editor.UpdateCourse import UpdateCourse_pb2
from protos.editor.Editor.ConvertMedia import ConvertMedia_pb2

from config import settings


def _channel(host : str, tls : bool):
    if tls:
        credentials = grpc.ssl_channel_credentials()
        return grpc.secure_channel(host, credentials)
    else:
        return grpc.insecure_channel(host)


def test_stream_request_iterator():
    did2 = "WyIweDQ3MGE5ZDA0ZjZkMWMyYzJmYmU1YjFjZDI3YTQ5ZThjNDc3MWNiYzQxMTUyZTZhMmFiY2RiMzQ5ODYwODQzMmY1YTBhZjU1YzVlZTNkYTRmYzVmNGJkYTFhZjVmYWNlOWIzNjE1NTYzNzI2MDRjNjBkMmQ5MjlkNzRjZDY5ZWYwMWMiLCJ7XCJpYXRcIjoxNjQ5NTE2MjYxLFwiZXh0XCI6MTA2NDk1MTYyNjEsXCJpc3NcIjpcImRpZDpldGhyOjB4Mjg2ZjI1RGQwOTI0ODAxMWQ4ZDk4OTNCYTJEYzVDQkNBN2QyOGFhM1wiLFwic3ViXCI6XCI2MFBPU1NTVGZ2TGp3R1JQNnpZTWgxLWt3NC1fenh3eDJESjhRVjZIUm8wPVwiLFwiYXVkXCI6XCJza0hJRXAyNVlkMGhvaHhrYWpiaTc4cGZaZ2JmUTdKQ3BtSmhCUm1XWkpJPVwiLFwibmJmXCI6MTY0OTUxNjI2MSxcInRpZFwiOlwiNDBhODMyOWQtZmRlZC00ZmY1LWIyMzctYjY4YThjNzlmMTI5XCIsXCJhZGRcIjpcIjB4M2NjMTIwMTIzOGRjZDY5YWEwMzZiZGE3ZTdlMTY1NmFlN2EyNmY3NGY3MjVjNzBhMTI2YjkyNDRhODE1MTZlYTM5YmU3MGIyNWQxYzJiYTM1Y2VlZmIyODE4NjFmNTVlZmI2NjU1MmUwN2EwOWI1YWQ3YWQ1ODliMzU3NDcxMDQxY1wifSJd"
    for i in range(0, 15):
        time.sleep(5)
        req = UpdateCourse_pb2.UpdateCourseRequest(token=did2, team="ade3f918-d335-41ef-af9c-363a8e722e50",
                                               course="ade3f918-d335-41ef-af9c-363a8e722e50",
                                               message="Hi I'm first client " + str(i))
        yield req


def test_stream(stub: service_pb2_grpc.EditorStub):
    print("Starting stream check")
    did2 = "WyIweDQ3MGE5ZDA0ZjZkMWMyYzJmYmU1YjFjZDI3YTQ5ZThjNDc3MWNiYzQxMTUyZTZhMmFiY2RiMzQ5ODYwODQzMmY1YTBhZjU1YzVlZTNkYTRmYzVmNGJkYTFhZjVmYWNlOWIzNjE1NTYzNzI2MDRjNjBkMmQ5MjlkNzRjZDY5ZWYwMWMiLCJ7XCJpYXRcIjoxNjQ5NTE2MjYxLFwiZXh0XCI6MTA2NDk1MTYyNjEsXCJpc3NcIjpcImRpZDpldGhyOjB4Mjg2ZjI1RGQwOTI0ODAxMWQ4ZDk4OTNCYTJEYzVDQkNBN2QyOGFhM1wiLFwic3ViXCI6XCI2MFBPU1NTVGZ2TGp3R1JQNnpZTWgxLWt3NC1fenh3eDJESjhRVjZIUm8wPVwiLFwiYXVkXCI6XCJza0hJRXAyNVlkMGhvaHhrYWpiaTc4cGZaZ2JmUTdKQ3BtSmhCUm1XWkpJPVwiLFwibmJmXCI6MTY0OTUxNjI2MSxcInRpZFwiOlwiNDBhODMyOWQtZmRlZC00ZmY1LWIyMzctYjY4YThjNzlmMTI5XCIsXCJhZGRcIjpcIjB4M2NjMTIwMTIzOGRjZDY5YWEwMzZiZGE3ZTdlMTY1NmFlN2EyNmY3NGY3MjVjNzBhMTI2YjkyNDRhODE1MTZlYTM5YmU3MGIyNWQxYzJiYTM1Y2VlZmIyODE4NjFmNTVlZmI2NjU1MmUwN2EwOWI1YWQ3YWQ1ODliMzU3NDcxMDQxY1wifSJd"
    # my_event_stream = stub.UpdateCourse(test_stream_request_iterator())
    rq = UpdateCourse_pb2.UpdatesRequest(token=did2, team="ade3f918-d335-41ef-af9c-363a8e722e51", course="ade3f918-d335-41ef-af9c-363a8e722e51")
    for response in stub.UpdatesStream(rq):
        print("The response from update: ", response)
    # send_queue.push(StreamingMessage())
    # response = next(my_event_stream)
    # response = stub.UpdateCourse(req)


def test_media_converter(stub: service_pb2_grpc.EditorMagicConverterStub):
    print("STARTING MEDIA CONVERSION")
    did2 = "WyIweDQ3MGE5ZDA0ZjZkMWMyYzJmYmU1YjFjZDI3YTQ5ZThjNDc3MWNiYzQxMTUyZTZhMmFiY2RiMzQ5ODYwODQzMmY1YTBhZjU1YzVlZTNkYTRmYzVmNGJkYTFhZjVmYWNlOWIzNjE1NTYzNzI2MDRjNjBkMmQ5MjlkNzRjZDY5ZWYwMWMiLCJ7XCJpYXRcIjoxNjQ5NTE2MjYxLFwiZXh0XCI6MTA2NDk1MTYyNjEsXCJpc3NcIjpcImRpZDpldGhyOjB4Mjg2ZjI1RGQwOTI0ODAxMWQ4ZDk4OTNCYTJEYzVDQkNBN2QyOGFhM1wiLFwic3ViXCI6XCI2MFBPU1NTVGZ2TGp3R1JQNnpZTWgxLWt3NC1fenh3eDJESjhRVjZIUm8wPVwiLFwiYXVkXCI6XCJza0hJRXAyNVlkMGhvaHhrYWpiaTc4cGZaZ2JmUTdKQ3BtSmhCUm1XWkpJPVwiLFwibmJmXCI6MTY0OTUxNjI2MSxcInRpZFwiOlwiNDBhODMyOWQtZmRlZC00ZmY1LWIyMzctYjY4YThjNzlmMTI5XCIsXCJhZGRcIjpcIjB4M2NjMTIwMTIzOGRjZDY5YWEwMzZiZGE3ZTdlMTY1NmFlN2EyNmY3NGY3MjVjNzBhMTI2YjkyNDRhODE1MTZlYTM5YmU3MGIyNWQxYzJiYTM1Y2VlZmIyODE4NjFmNTVlZmI2NjU1MmUwN2EwOWI1YWQ3YWQ1ODliMzU3NDcxMDQxY1wifSJd"
    media_url = "https://storiesstatic.s3.us-west-2.amazonaws.com/media/uploads/158-what-is-machine-learning.mp4"
    req = ConvertMedia_pb2.ConvertMediaRequest(
        token=did2, team_id="ade3f918-d335-41ef-af9c-363a8e722e50", course_id="ade3f918-d335-41ef-af9c-363a8e722e50",
        editor_id="ade3f918-d335-41ef-af9c-363a8e722e50", media_url=media_url
    )
    response = stub.ConvertMedia(req)
    print(">>>!!!The media conversion response is: ", response)


def run():
    print("Going to start")
    with _channel(settings.client_grpc_host, settings.client_grpc_tls) as channel:
        # stub = service_pb2_grpc.EditorMagicConverterStub(channel)
        # test_media_converter(stub)
        stub = service_pb2_grpc.EditorStub(channel)
        test_stream(stub)
        #threading.Thread(target=test_stream(stub), daemon=True).start()


if __name__ == '__main__':
    logging.basicConfig()
    run()
