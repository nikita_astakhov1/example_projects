from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from app.config import settings

from app.models.converter import Base


def init_db_connection() -> sessionmaker:
    """MUST BE CALLED ONLY ONCE"""
    _engine = create_engine(
        settings.db_url
    )

    # Generate all tables if doesn't exists
    Base.metadata.create_all(_engine)

    return sessionmaker(autocommit=False, autoflush=False, bind=_engine)
