from typing import List

from sqlalchemy.orm.session import Session

from app.database.abstract import MagicConverterAbstractObject
from app.database.core import AlchemyCore, session_maker

from app.models.converter import (MediaMagicModel, MediaCutModel)
from app.models.converter_entities import (MediaMagicEntity, MediaMagicPart)


class MagicCutObject(MagicConverterAbstractObject, AlchemyCore):
    model = MediaCutModel
    entity = MediaMagicPart


class MagicConversionObject(MagicConverterAbstractObject, AlchemyCore):
    model = MediaMagicModel
    entity = MediaMagicEntity
    chunk_model = MediaCutModel
    chunk_entity = MediaMagicPart

    @session_maker
    def add_chunks(self, session: Session, instance: MediaMagicEntity, instances: List[MediaMagicPart]):
        conversion_instance = session.query(self.model).filter_by(id=instance.id).first()
        if conversion_instance:
            for chunk in instances:
                chunk_instance = session.query(self.chunk_model).filter_by(id=chunk.id).first()
                if chunk_instance:
                    conversion_instance.chunks.append(chunk_instance)
                    session.add(chunk_instance)

            session.commit()
            session.refresh(conversion_instance)

            return self.entity(**conversion_instance.dict())
        return None
