from typing import List, Optional
from uuid import UUID
from sqlalchemy.orm.session import Session

from app.database.abstract import StructureAbstractObject
from app.database.core import AlchemyCore, session_maker

from app.models.structure import (
    CourseEditorModel, CourseSectionModel, CourseLessonModel, LessonTextContentModel,
    LessonRoadmapContentModel, RoadmapContentInstance, LessonMediaContentModel, LessonTestContentModel
)
from app.models.sctructure_entities import (
    CourseEditorEntity, CourseSectionEntity, CourseLessonEntity, LessonTextContentEntity,
    LessonRoadmapContentEntity, RoadmapInstanceEntity, LessonMediaInstanceEntity, LessonTestInstanceEntity,
    CourseLessonSaveEntity
)


class EditorObject(StructureAbstractObject, AlchemyCore):
    model = CourseEditorModel
    entity = CourseEditorEntity
    section_model = CourseSectionModel
    section_entity = CourseSectionEntity

    @session_maker
    def add_sections(self, session: Session, editor_id: str, instances: List[CourseSectionEntity]):
        editor_instance = session.query(self.model).filter_by(id=editor_id).first()
        if editor_instance:
            for section in instances:
                section_obj = SectionObject().create_section(data=section)
                section_instance = session.query(self.section_model).filter_by(id=section_obj.id).first()
                if section_instance:
                    editor_instance.sections.append(section_instance)
                    session.add(section_instance)
            session.commit()
            session.refresh(editor_instance)
            return self.entity(**editor_instance.dict())
        return None


class SectionObject(StructureAbstractObject, AlchemyCore):
    model = CourseSectionModel
    entity = CourseSectionEntity
    lesson_model = CourseLessonModel
    lesson_entity = CourseLessonEntity

    @session_maker
    def get_previous(self, session: Session, editor_id: str, order: int) -> Optional[str]:
        after_order = order - 1
        if after_order - 1 >= 0:
            instance = session.query(self.model).filter_by(
                editor_id=editor_id, order=after_order
            ).order_by(self.model.created_at.desc()).first()
            if instance:
                return instance.external_id
        return None

    @session_maker
    def get_complete_instance_external(self, session: Session, editor_id: str, external_id: str) -> Optional[entity]:
        instance = session.query(self.model).filter_by(
            editor_id=editor_id, external_id=external_id
        ).order_by(self.model.created_at.desc()).first()
        if instance:
            processed_lessons = []
            all_lessons = instance.lessons
            instance.lessons = []
            response_instance = self.entity(**instance.dict())
            for lesson in all_lessons:
                text_id = lesson.text_content
                roadmap_id = lesson.roadmap_content
                media_id = lesson.media_content
                test_id = lesson.test_content

                lesson.text_content = None
                lesson.roadmap_content = None
                lesson.media_content = None
                lesson.test_content = None

                lesson_entity = self.lesson_entity(**lesson.dict())

                if text_id:
                    text_obj = LessonTextObject().get(id=text_id)
                    lesson_entity.text_content = text_obj

                if roadmap_id:
                    roadmap_obj = LessonRoadmapObject().get_complete(id=roadmap_id)
                    lesson_entity.roadmap_content = roadmap_obj

                if media_id:
                    media_obj = LessonMediaObject().get(id=media_id)
                    lesson_entity.media_content = media_obj

                if test_id:
                    test_obj = LessonTestObject().get(id=test_id)
                    lesson_entity.test_content = test_obj

                processed_lessons.append(lesson_entity)
            response_instance.lessons = processed_lessons

            return response_instance
        return None

    @session_maker
    def create_section(self, session: Session, data: CourseSectionEntity):
        inp = data.dict()
        del inp["id"]
        del inp["created_at"]
        del inp["updated_at"]
        del inp["lessons"]
        section_obj = self.model(**inp)
        session.add(section_obj)
        session.commit()
        session.refresh(section_obj)
        saved_section = self.entity(**section_obj.dict())  # here will be no lessons and other data

        # now going to iterate through lessons and add them here
        saved_lessons = []
        for lesson in data.lessons:
            save_lesson_entity = CourseLessonSaveEntity(
                section_id=saved_section.id, external_id=lesson.external_id,
                author_id=lesson.author_id, title=lesson.title, order=lesson.order,
                type=lesson.type, complexity=lesson.complexity
            )
            if lesson.text_content is not None:
                text_obj = LessonTextObject().save_instances(instances=[lesson.text_content])[0]
                save_lesson_entity.text_content = text_obj.id
            if lesson.roadmap_content is not None:
                roadmap_obj = LessonRoadmapObject().save_instances(instances=[lesson.roadmap_content])[0]
                val_objs = RoadmapValueObject().save_instances(instances=lesson.roadmap_content.values)
                LessonRoadmapObject().add_values(instance=roadmap_obj, instances=val_objs)
                save_lesson_entity.roadmap_content = roadmap_obj.id
            if lesson.media_content is not None:
                media_obj = LessonMediaObject().save_instances(instances=[lesson.media_content])[0]
                save_lesson_entity.media_content = media_obj.id
            if lesson.test_content is not None:
                test_obj = LessonTestObject().save_instances(instances=[lesson.test_content])[0]
                save_lesson_entity.test_content = test_obj.id
            lesson_ent = LessonObject().save_instances(instances=[save_lesson_entity])[0]
            saved_lessons.append(lesson_ent)

        self.add_lessons(instance=saved_section, instances=saved_lessons)
        return saved_section

    @session_maker
    def add_lessons(self, session: Session, instance: CourseSectionEntity, instances: List[CourseLessonSaveEntity]):
        section_instance = session.query(self.model).filter_by(id=instance.id).first()
        if section_instance:
            for lesson in instances:
                lesson_instance = session.query(self.lesson_model).filter_by(id=lesson.id).first()
                if lesson_instance:
                    section_instance.lessons.append(lesson_instance)
                    session.add(lesson_instance)

            session.commit()
            session.refresh(section_instance)

            return self.entity(**section_instance.dict())
        return None


class LessonObject(StructureAbstractObject, AlchemyCore):
    model = CourseLessonModel
    entity = CourseLessonSaveEntity


class LessonTextObject(StructureAbstractObject, AlchemyCore):
    model = LessonTextContentModel
    entity = LessonTextContentEntity


class RoadmapValueObject(StructureAbstractObject, AlchemyCore):
    model = RoadmapContentInstance
    entity = RoadmapInstanceEntity


class LessonRoadmapObject(StructureAbstractObject, AlchemyCore):
    model = LessonRoadmapContentModel
    entity = LessonRoadmapContentEntity
    value_model = RoadmapContentInstance
    value_entity = RoadmapInstanceEntity

    @session_maker
    def get_complete(self, session: Session, **params) -> Optional[entity]:
        instance = session.query(self.model).filter_by(**params).order_by(self.model.created_at.desc()).first()
        if instance:
            values_all = instance.values
            values_list = []
            instance.values = []
            response = self.entity(**instance.dict())
            for val in values_all:
                obj = session.query(self.value_model).filter_by(id=val.id).first()
                values_list.append(obj)
            response.values = values_list
            return response

        return None

    @session_maker
    def add_values(self, session: Session, instance: LessonRoadmapContentEntity, instances: List[RoadmapInstanceEntity]):
        roadmap_instance = session.query(self.model).filter_by(id=instance.id).first()
        if roadmap_instance:
            for value in instances:
                value_instance = session.query(self.value_model).filter_by(id=value.id).first()
                if value_instance:
                    roadmap_instance.values.append(value_instance)
                    session.add(value_instance)

            session.commit()
            session.refresh(roadmap_instance)

            return self.entity(**roadmap_instance.dict())
        return None


class LessonMediaObject(StructureAbstractObject, AlchemyCore):
    model = LessonMediaContentModel
    entity = LessonMediaInstanceEntity


class LessonTestObject(StructureAbstractObject, AlchemyCore):
    model = LessonTestContentModel
    entity = LessonTestInstanceEntity
