import enum


class LessonTypeEnum(enum.Enum):
    TEXT = 0
    ROADMAP = 1
    MEDIA = 2
    TEST = 3


class LessonComplexityEnum(enum.Enum):
    EASY = 0
    MID = 1
    HARD = 2


class UpdateTypeEnum(enum.Enum):
    CREATE = 0
    UPDATE = 1
    DELETE = 2
