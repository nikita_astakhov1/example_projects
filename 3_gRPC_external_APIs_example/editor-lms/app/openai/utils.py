
def get_the_chunk_word(number):
    index = number - 1
    if index < 0:
        index = 0
    elif index > 49:
        index = 49
    words = [
        "One",	"Two", "Three", "Four", "Five", "Six", "Seven", "Eight",
        "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen",
        "Seventeen", "Eighteen", "Nineteen", "Twenty", "Twenty-one", "Twenty-two", "Twenty-three",
        "Twenty-four", "Twenty-five", "Twenty-six", "Twenty-seven", "Twenty-eight", "Twenty-nine", "Thirty",
        "Thirty-one", "Thirty-two", "Thirty-three", "Thirty-four", "Thirty-five", "Thirty-six", "Thirty-seven",
        "Thirty-eight", "Thirty-nine", "Forty", "Forty-one", "Forty-two", "Forty-three", "Forty-four",
        "Forty-five", "Forty-six", "Forty-seven", "Forty-eight", "Forty-nine", "Fifty"
    ]
    return words[index]


def compile_array_from_parts(text):
    paragraphs = text.split("\n\n")
    result = []
    for i in paragraphs:
        i.lstrip(' ')
        if i != "":
            result.append(i)
    return result


def compile_summarization(text):
    paragraphs = text.split("\n\n")
    result = []
    for i in paragraphs:
        i.lstrip(' ')
        if i != "":
            result.append(i)
    heading = ""
    paragraph = ""
    if len(result) == 1:
        if len(result[0]) > 100:
            paragraph = result[0]
        else:
            heading = result[0]
    else:
        for index, q in enumerate(result):
            if index == 0:
                heading = q.capitalize()
            elif index == 1:
                paragraph = q.capitalize()
    return heading, paragraph
