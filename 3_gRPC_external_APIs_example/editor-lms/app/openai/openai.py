import openai
from app.openai.utils import (
    get_the_chunk_word, compile_array_from_parts,
    compile_summarization
)

openai.api_key = "sk-YOUR_KEY_HERE"


def logical_parts_extractor(text):
    ai_request = openai.Completion.create(
        engine="text-davinci-002",
        max_tokens=2000,
        prompt=find_logical_parts(text),
        temperature=0.15
    )

    if ai_request.choices[0].finish_reason == "stop":
        openai_id = ai_request.id
        processed_text = compile_array_from_parts(ai_request.choices[0].text)
        return openai_id, processed_text
    else:
        return None, None


def find_logical_parts(text):
    # chunks = get_the_chunk_word(chunks)
    # strip the text
    return """Split complete text into logical parts. 
            Text: {}""".format(text)
    # return """Cut all text into exactly {} logical parts and mark each produced part by two semicolons.
    #        Text: {}""".format(chunks, text)
    # return """Find {} logical parts in the text and mark them by a number.
    #    Return only first five words from each logical part.
    #    Text: {}""".format(chunks, text)


def summarization_extractor(paragraph):
    ai_request = openai.Completion.create(
        engine="text-davinci-002",
        max_tokens=500,
        prompt=prepare_summarization(paragraph),
        temperature=0.55
    )
    title, text = compile_summarization(ai_request.choices[0].text)
    return title, text


def prepare_summarization(text):
    return """Summarize a text in a first person with a title and two sentences.
        Text: {}""".format(text)
