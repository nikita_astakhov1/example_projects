from math import floor


def calculate_chunks(duration) -> int:
    result = floor(int(duration) / 60)
    if result <= 0:
        result = 1
    return result
