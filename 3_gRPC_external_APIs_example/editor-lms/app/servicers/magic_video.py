import grpc

from protos.editor import service_pb2_grpc
from app.servicers.clients.authorize import authorize_user
from protos.editor.Editor.ConvertMedia.ConvertMedia_pb2 import ConvertToSectionRequest

from app.methods.magic_video import convert_media, process_chunks


class EditorMagicConverterServicer(service_pb2_grpc.EditorMagicConverterServicer):
    """
    The process is changing a little:
    1 First upload the long video
    1.1 Create an empty section(client simply draw: internal id, order)
    1.2 Upload video to s3 (client)
    1.3 Upload the data
    1.4 RETURN: timeframes for the cut and converter_id
    QUESTION: do I need to return some data to an update stream?
    I think no.
    2 Second part is to upload and process the array of links for a video.
    1. Client cuts and uploads videos to the s3
    2. Process transcribes and apply other Open AI magic
    3. Internally create a new section with all lessons.
    4. RETURN: push section to a structure update. Probably push will be made under the name of an initial requester.
    """

    def ConvertMediaInitial(self, request, context):
        if request.token is None:
            context.abort(code=grpc.StatusCode.UNAUTHENTICATED, details="Auth token is not valid")
        user = authorize_user(token=request.token)
        if user is not None and user.active:
            code, message, response = convert_media(
                user_id=user.id, team_id=request.team_id,
                course_id=request.course_id, editor_id=request.editor_id,
                section_id=request.section_id, order=request.order,
                media_url=request.media_url
            )
            context.set_code(code)
            context.set_details(message)
            return response
        context.abort(code=grpc.StatusCode.UNAUTHENTICATED, details="User not found")

    def ConvertMediaCuts(self, request: ConvertToSectionRequest, context):
        if request.token is None:
            context.abort(code=grpc.StatusCode.UNAUTHENTICATED, details="Auth token is not valid")
        user = authorize_user(token=request.token)
        if user is not None and user.active:
            code, message, response = process_chunks(data=request, user_id=str(user.id))

            # send update to a stream servicer under the name of this client
            context.set_code(code)
            context.set_details(message)
            return response

        context.abort(code=grpc.StatusCode.UNAUTHENTICATED, details="User not found")
