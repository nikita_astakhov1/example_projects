import logging
import grpc


from protos.team import service_pb2_grpc as team_pb2_grpc
from protos.team.TeamUsers.CheckUserRoles import CheckUserRoles_pb2
from protos.team.TeamCourses.AddCourse import AddCourse_pb2

from app.config import settings


def _channel(host: str, tls: bool):
    if tls:
        credentials = grpc.ssl_channel_credentials()
        return grpc.secure_channel(host, credentials)
    else:
        return grpc.insecure_channel(host)


def get_permissions(stub: team_pb2_grpc.TeamUsersStub, user_id, team_id):
    return stub.CheckUserRoles(CheckUserRoles_pb2.CheckRoleRequest(user_id=str(user_id), team_id=str(team_id)))


def compile_member_access(user_id, team_id):
    logging.basicConfig()
    with _channel(settings.teams_grpc_host, settings.client_grpc_tls) as channel:
        stub = team_pb2_grpc.TeamUsersStub(channel)
        return get_permissions(stub, user_id, team_id)


def save_course(stub: team_pb2_grpc.TeamCoursesStub, course_id, course_name, team_id, team_user_id):
    return stub.AddCourse(
        AddCourse_pb2.AddCourseRequest(
            course_id=course_id, course_name=course_name,
            team_id=team_id, team_user_id=team_user_id
        )
    )


def add_course_to_team(course_id, course_name, team_id, team_user_id):
    logging.basicConfig()
    with _channel(settings.teams_grpc_host, settings.client_grpc_tls) as channel:
        stub = team_pb2_grpc.TeamCoursesStub(channel)
        return save_course(stub, course_id, course_name, team_id, team_user_id)
