import logging
import grpc

from protos import profile_pb2
from protos.profile import service_pb2_grpc as profile_pb2_grpc
from protos.profile.Profile.InternalGetProfile import InternalGetProfile_pb2


from app.config import settings


def _channel(host: str, tls: bool):
    if tls:
        credentials = grpc.ssl_channel_credentials()
        return grpc.secure_channel(host, credentials)
    else:
        return grpc.insecure_channel(host)


def get_profile(stub: profile_pb2_grpc.ProfileStub, user_id):
    return stub.InternalGetProfile(InternalGetProfile_pb2.InternalGetProfileRequest(auth_id=str(user_id)))


def request_profile_info(user_id):
    logging.basicConfig()
    with _channel(settings.profile_grpc_host, settings.client_grpc_tls) as channel:
        stub = profile_pb2_grpc.ProfileStub(channel)
        return get_profile(stub, user_id)
