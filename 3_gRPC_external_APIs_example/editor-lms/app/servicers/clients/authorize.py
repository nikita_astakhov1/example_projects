import logging
import grpc

from protos.authorization import service_pb2_grpc as auth_pb2_grpc
from protos.authorization.CheckToken.ReturnUser import ReturnUser_pb2

from app.config import settings


def _channel(host: str, tls: bool):
    if tls:
        credentials = grpc.ssl_channel_credentials()
        return grpc.secure_channel(host, credentials)
    else:
        return grpc.insecure_channel(host)


def go_and_check(stub: auth_pb2_grpc.CheckTokenStub, token):
    return stub.ReturnUser(ReturnUser_pb2.AuthRequest(token=token))


def go_and_check_details(stub: auth_pb2_grpc.CheckTokenStub, token):
    return stub.ReturnUserDetails(ReturnUser_pb2.AuthRequest(token=token))


def authorize_user(token):
    logging.basicConfig()
    with _channel(settings.auth_grpc_host, settings.client_grpc_tls) as channel:
        stub = auth_pb2_grpc.CheckTokenStub(channel)
        return go_and_check(stub, token)


def authorize_user_details(token):
    logging.basicConfig()
    with _channel(settings.auth_grpc_host, settings.client_grpc_tls) as channel:
        stub = auth_pb2_grpc.CheckTokenStub(channel)
        return go_and_check_details(stub, token)
