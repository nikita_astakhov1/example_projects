import grpc

from protos.editor import service_pb2_grpc
from protos.editor.Editor.UpdateCourse import UpdateCourse_pb2

from app.servicers.clients.authorize import authorize_user
from app.methods.structure import push_structure_update, create_empty_editor, stream_structure_update

from app.constants import UpdateTypeEnum


class EditorGeneralServicer(service_pb2_grpc.EditorServicer):

    def __init__(self):
        self.clients = []
        self.updates = []

    def CreateEditor(self, request, context):
        if request.token is None:
            context.abort(code=grpc.StatusCode.UNAUTHENTICATED, details="Auth token is not valid")
        user = authorize_user(token=request.token)
        if user is not None and user.active:
            code, message, response = create_empty_editor(course_id=request.course_id, team_id=request.team_id)
            context.set_code(code)
            context.set_details(message)
            return response
        context.abort(code=grpc.StatusCode.UNAUTHENTICATED, details="User not found")

    def UpdatesStream(self, request_iterator, context):
        if request_iterator.token is None:
            context.abort(code=grpc.StatusCode.UNAUTHENTICATED, details="Auth token is not valid")
        user = authorize_user(token=request_iterator.token)

        # check here if the user in the team and can be connected

        client = {"user_id": user.id, "peer": str(context.peer()), "editor_id": request_iterator.editor_id}
        if client not in self.clients:
            self.clients.append(client)
        last_index = 0
        if user is not None and user.active:
            while True:
                while len(self.updates) > last_index:
                    n = self.updates[last_index]
                    last_index += 1
                    if n["editor_id"] == request_iterator.editor_id:
                        # get the data here and yield it
                        data = stream_structure_update(
                            editor_id=n["editor_id"], action_type=n["type"],
                            section_id=n["section_id"], updated_by=n["updated_by"]
                        )
                        yield data

    def PushUpdate(self, request: UpdateCourse_pb2.UpdateEditorRequest, context):
        if request.token is None:
            context.abort(code=grpc.StatusCode.UNAUTHENTICATED, details="Auth token is not valid")
        user = authorize_user(token=request.token)
        if user is not None and user.active:
            code, message, response = push_structure_update(data=request, user_id=user.id)
            context.set_code(code)
            context.set_details(message)
            if code == grpc.StatusCode.OK:
                try:
                    update_type = request.type
                except AttributeError:
                    update_type = UpdateTypeEnum.CREATE
                update = {
                    "editor_id": request.editor_id, "type": update_type,
                    "section_id": request.payload.id, "updated_by": str(user.id)
                }
                self.updates.append(update)
            return response
        context.abort(code=grpc.StatusCode.UNAUTHENTICATED, details="User not found")
