from typing import Optional, List
from uuid import UUID
from app.models.core_entity import BaseModelEntity
from app.constants import LessonTypeEnum, LessonComplexityEnum


class LessonTestInstanceEntity(BaseModelEntity):
    text: str
    answers: List[str]


class LessonMediaInstanceEntity(BaseModelEntity):
    video_url: str
    speaker_image: str
    transcribe_id: str


class RoadmapInstanceEntity(BaseModelEntity):
    order: int
    name: str


class LessonRoadmapContentEntity(BaseModelEntity):
    heading: str
    values: Optional[List[RoadmapInstanceEntity]]


class LessonTextContentEntity(BaseModelEntity):
    image_url: str
    heading: str
    paragraph: str


class CourseLessonEntity(BaseModelEntity):
    section_id: Optional[UUID]
    external_id: Optional[UUID]
    author_id: Optional[UUID]
    title: str
    order: int
    type: LessonTypeEnum
    complexity: LessonComplexityEnum
    text_content: Optional[LessonTextContentEntity]
    roadmap_content: Optional[LessonRoadmapContentEntity]
    media_content: Optional[LessonMediaInstanceEntity]
    test_content: Optional[LessonTestInstanceEntity]


class CourseLessonSaveEntity(CourseLessonEntity):
    text_content: Optional[UUID]
    roadmap_content: Optional[UUID]
    media_content: Optional[UUID]
    test_content: Optional[UUID]


class CourseSectionEntity(BaseModelEntity):
    external_id: Optional[UUID]
    author_id: Optional[UUID]
    title: str
    order: int
    auto_creation_id: str  # this is an id of magic converter
    lessons: Optional[List[CourseLessonEntity]]


class CourseEditorEntity(BaseModelEntity):
    course_id: Optional[UUID]
    team_id: Optional[UUID]
    default_bucket_path: str
    sections: Optional[List[CourseSectionEntity]]
