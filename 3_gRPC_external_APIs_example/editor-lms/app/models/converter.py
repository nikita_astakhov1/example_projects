import datetime
import uuid

from sqlalchemy.orm import relationship
from sqlalchemy import Column, DateTime, String, Boolean, Integer, ForeignKey
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.ext.declarative import declarative_base, DeclarativeMeta
from sqlalchemy.sql import func


Base: DeclarativeMeta = declarative_base()


class BaseModel(Base):
    __abstract__ = True

    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4, index=True)
    created_at = Column(DateTime(timezone=True), default=datetime.datetime.utcnow, server_default=func.now())
    updated_at = Column(DateTime(timezone=True), default=datetime.datetime.utcnow, onupdate=func.now())

    def dict(self) -> dict:
        return self.__dict__


class MediaCutModel(BaseModel):
    __tablename__ = "media_magic_cut"

    conversion_id = Column(UUID(as_uuid=True), ForeignKey('media_magic_converter.id', ondelete='CASCADE'))
    order = Column(Integer, nullable=False, default=0)
    start = Column(Integer, nullable=False, default=0)
    end = Column(Integer, nullable=False, default=0)


class MediaMagicModel(BaseModel):
    __tablename__ = "media_magic_converter"

    user_id = Column(UUID(as_uuid=True), primary_key=False, nullable=True, default=None)
    team_id = Column(UUID(as_uuid=True), primary_key=False, nullable=True, default=None)
    course_id = Column(UUID(as_uuid=True), primary_key=False, nullable=True, default=None)
    editor_id = Column(UUID(as_uuid=True), primary_key=False, nullable=True, default=None)
    section_id = Column(String, nullable=False, default="")
    order = Column(Integer, nullable=False, default=0)
    original_video_url = Column(String, nullable=False, default="")
    conversion_id = Column(String, nullable=False, default="")
    duration = Column(Integer, nullable=False, default=0)
    processing_id = Column(String, nullable=False, default="")
    completed = Column(Boolean, nullable=False, default=False)
    chunks = relationship("MediaCutModel", backref="media_converter_chunks")
