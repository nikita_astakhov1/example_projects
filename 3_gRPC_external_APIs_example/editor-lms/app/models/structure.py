import datetime
import uuid

from sqlalchemy.orm import relationship
from sqlalchemy import Column, DateTime, String, Integer, ForeignKey, Enum as SqlAlchemyEnum
from sqlalchemy.dialects.postgresql import UUID, ARRAY
from sqlalchemy.ext.declarative import declarative_base, DeclarativeMeta
from app.constants import LessonTypeEnum, LessonComplexityEnum
from sqlalchemy.sql import func


Base: DeclarativeMeta = declarative_base()


class BaseModel(Base):
    __abstract__ = True

    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4, index=True)
    created_at = Column(DateTime(timezone=True), default=datetime.datetime.utcnow, server_default=func.now())
    updated_at = Column(DateTime(timezone=True), default=datetime.datetime.utcnow, onupdate=func.now())

    def dict(self) -> dict:
        return self.__dict__


class LessonTestContentModel(BaseModel):
    __tablename__ = "lesson_test_content"

    text = Column(String, nullable=False, default="")
    answers = Column(ARRAY(String, as_tuple=False, dimensions=None, zero_indexes=False))


class LessonMediaContentModel(BaseModel):
    __tablename__ = "lesson_media_content"

    video_url = Column(String, nullable=False, default="")
    speaker_image = Column(String, nullable=False, default="")
    transcribe_id = Column(String, nullable=False, default="")


class RoadmapContentInstance(BaseModel):
    __tablename__ = "roadmap_content_instance"

    roadmap_id = Column(
        UUID(as_uuid=True), ForeignKey('lesson_roadmap_content.id', ondelete='CASCADE'),
        nullable=True
    )
    order = Column(Integer, nullable=False, default=0)
    name = Column(String, nullable=False, default="")


class LessonRoadmapContentModel(BaseModel):
    __tablename__ = "lesson_roadmap_content"

    heading = Column(String, nullable=False, default="")
    values = relationship("RoadmapContentInstance", backref="roadmap_instances")


class LessonTextContentModel(BaseModel):
    __tablename__ = "lesson_text_content"

    image_url = Column(String, nullable=False, default="")
    heading = Column(String, nullable=False, default="")
    paragraph = Column(String, nullable=False, default="")


class CourseLessonModel(BaseModel):
    __tablename__ = "section_lesson"

    section_id = Column(
        UUID(as_uuid=True), ForeignKey('course_section.id', ondelete='CASCADE'),
        nullable=True
    )
    external_id = Column(String, nullable=False, default="")
    author_id = Column(UUID(as_uuid=True), primary_key=False, nullable=True, default=None)
    title = Column(String, nullable=False, default="")
    order = Column(Integer, nullable=False, default=0)
    type = Column(SqlAlchemyEnum(LessonTypeEnum))
    complexity = Column(SqlAlchemyEnum(LessonComplexityEnum))
    text_content = Column(
        UUID(as_uuid=True), ForeignKey('lesson_text_content.id', ondelete='CASCADE'),
        nullable=True
    )
    roadmap_content = Column(
        UUID(as_uuid=True), ForeignKey('lesson_roadmap_content.id', ondelete='CASCADE'),
        nullable=True
    )
    media_content = Column(
        UUID(as_uuid=True), ForeignKey('lesson_media_content.id', ondelete='CASCADE'),
        nullable=True
    )
    test_content = Column(
        UUID(as_uuid=True), ForeignKey('lesson_test_content.id', ondelete='CASCADE'),
        nullable=True
    )


class CourseSectionModel(BaseModel):
    __tablename__ = "course_section"

    editor_id = Column(
        UUID(as_uuid=True), ForeignKey('course_editor.id', ondelete='CASCADE'),
        nullable=True
    )
    external_id = Column(String, nullable=False, default="")
    author_id = Column(UUID(as_uuid=True), primary_key=False, nullable=True, default=None)
    auto_creation_id = Column(String, nullable=False, default="")
    title = Column(String, nullable=False, default="")
    order = Column(Integer, nullable=False, default=0)
    lessons = relationship("CourseLessonModel", backref="section_lessons")


class CourseEditorModel(BaseModel):
    __tablename__ = "course_editor"

    course_id = Column(UUID(as_uuid=True), primary_key=False, nullable=True, default=None)
    team_id = Column(UUID(as_uuid=True), primary_key=False, nullable=True, default=None)
    default_bucket_path = Column(String, nullable=False, default="")
    sections = relationship("CourseSectionModel", backref="editor_sections")
