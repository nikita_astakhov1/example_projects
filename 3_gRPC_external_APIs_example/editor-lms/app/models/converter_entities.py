from typing import Optional, List
from uuid import UUID
from app.models.core_entity import BaseModelEntity, BaseEntity


class MediaMagicPart(BaseModelEntity):
    conversion_id: Optional[UUID]
    order: int
    start: int
    end: int


class MediaMagicEntity(BaseModelEntity):
    completed: bool
    user_id: Optional[UUID]
    team_id: Optional[UUID]
    course_id: Optional[UUID]
    editor_id: Optional[UUID]
    section_id: Optional[UUID]
    order: int
    original_video_url: str
    conversion_id: str
    duration: int
    processing_id: str
    chunks: Optional[List[MediaMagicPart]]


class ProcessedChunk(BaseEntity):
    media_url: str
    transcribe_id: str
    heading: str
    paragraph: str
    name: str


class ProcessingEntity(BaseEntity):
    chunks: List[ProcessedChunk]
