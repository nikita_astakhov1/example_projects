from grpc import StatusCode
from app.transcribe.transcribe import get_transcribe
from app.openai.openai import logical_parts_extractor, summarization_extractor
from app.models.converter_entities import MediaMagicEntity, MediaMagicPart, ProcessedChunk, ProcessingEntity
from app.models.sctructure_entities import (
    CourseSectionEntity, CourseLessonEntity, LessonTextContentEntity, LessonRoadmapContentEntity,
    RoadmapInstanceEntity, LessonMediaInstanceEntity, LessonTestInstanceEntity
)
from app.constants import LessonTypeEnum, LessonComplexityEnum
from app.database.converter import MagicConversionObject, MagicCutObject

from protos.editor.Editor.ConvertMedia import ConvertMedia_pb2
from protos.editor.Editor.ConvertMedia.ConvertMedia_pb2 import ConvertToSectionRequest


def convert_media(user_id, team_id, course_id, editor_id, section_id, order,  media_url):
    """
    1. Upload video to AssemblyAI ++
    1.1 Get Sentences, Complete Text, Transcribe ID, Duration ++
    2. Calculate a number of chunks from media duration //not necessary
    2.1 Upload completed text to Open AI ++
    2.2 Get first sentences of each logical part of transcription ++
    2.3 Go through Transcribe sentences JSON and find start and end of each part
    2.4 Get summarization of each logical part with heading and sentences
    :return: Array of logical parts with start end heading and text
    """

    # media_url = "https://storiesstatic.s3.us-west-2.amazonaws.com/media/uploads/158-what-is-machine-learning.mp4"

    # AssembleAI
    duration, transcribe_id, sentences, text = get_transcribe(media_url)
    # Open AI
    openai_id, processed_text = logical_parts_extractor(text)

    if openai_id is None:
        return StatusCode.CANCELLED, "Media failed to process", ConvertMedia_pb2.ConvertMediaResponse()

    conversion_data = MediaMagicEntity(
        completed=True, user_id=user_id, team_id=team_id, course_id=course_id, editor_id=editor_id,
        section_id=section_id, order=order,
        original_video_url=media_url, conversion_id=transcribe_id,
        duration=duration, processing_id=openai_id, chunks=[]
    )

    # save conversion obj
    conv_db_obj = MagicConversionObject().save_instances(instances=[conversion_data])[0]

    # compiling parts
    parts = compile_parts(sentences, processed_text, conv_db_obj.id)

    # add chunks to a conversion model
    MagicConversionObject().add_chunks(instance=conv_db_obj, instances=parts)
    conversion_data.chunks = parts

    return StatusCode.OK, "Successfully converted", convert_to_grpc(conversion_data)


def compile_parts(sentences, parts, conversion_id):
    result = []
    # iterate through each part and find necessary data
    sentences_data = sentences['sentences']
    for index, part in enumerate(parts):
        data = find_obj_for_start(part, sentences_data)
        if data is not None:
            # title, text = summarization_extractor(part)
            data_last = find_obj_for_end(part, sentences_data)
            if data_last is not None:
                processed = MediaMagicPart(
                    conversion_id=conversion_id, order=index + 1, start=data['start'],
                    end=data_last['end']
                )
                result.append(processed)
    db_objs = MagicCutObject().save_instances(instances=result)
    return db_objs


def find_obj_for_start(text, sentences_data):
    part_first = get_beginning_of_string(text)
    try:
        return next(v for v in sentences_data if v['text'].startswith(part_first))
    except:
        return None


def get_beginning_of_string(text):
    try:
        return text[:20]
    except IndexError:
        return text[:5]


def find_obj_for_end(text, sentences_data):
    part_last = get_ending_of_string(text)
    try:
        return next(v for v in sentences_data if v['text'].endswith(part_last))
    except:
        return None


def get_ending_of_string(text):
    try:
        return text[-20:]
    except IndexError:
        return text[-5:]


def convert_to_grpc(data: MediaMagicEntity):
    chunks = []
    for ch in data.chunks:
        processed = ConvertMedia_pb2.ConvertedMediaCut(
            order=ch.order, start=str(ch.start),
            end=str(ch.end)
        )
        chunks.append(processed)
    result = ConvertMedia_pb2.ConvertMediaResponse(
        completed=data.completed, team_id=str(data.team_id),
        course_id=str(data.course_id), editor_id=str(data.editor_id),
        section_id=str(data.section_id), order=str(data.order), conversion_id=str(data.id),
        cuts=chunks
    )
    return result


"""
Processing parts of a section
"""


# data: ConvertToSectionRequest, user_id: str
def process_chunks():
    """
    Main purposes of a method:
    1 – Work with logical chunks
    1.0 Create transcribes for each chunk
    1.1. Create a text with summarization for a chunk
    2 – Create a roadmap from all video. Max 6 main parts.
    3 – Assets
    3.0 Analyze all text and create several questions with answers(probably)
    RETURN compiled section as push from a client

    :param data:
    :param user_id:
    :return:
    """
    links = [
        "https://storiesstatic.s3.us-west-2.amazonaws.com/media/uploads/splitted_0.mp4",
        "https://storiesstatic.s3.us-west-2.amazonaws.com/media/uploads/splitted_1.mp4",
        "https://storiesstatic.s3.us-west-2.amazonaws.com/media/uploads/splitted_2.mp4",
        "https://storiesstatic.s3.us-west-2.amazonaws.com/media/uploads/splitted_3.mp4",
        "https://storiesstatic.s3.us-west-2.amazonaws.com/media/uploads/splitted_4.mp4",
        "https://storiesstatic.s3.us-west-2.amazonaws.com/media/uploads/splitted_5.mp4",
        "https://storiesstatic.s3.us-west-2.amazonaws.com/media/uploads/splitted_6.mp4",
        "https://storiesstatic.s3.us-west-2.amazonaws.com/media/uploads/splitted_7.mp4",
        "https://storiesstatic.s3.us-west-2.amazonaws.com/media/uploads/splitted_8.mp4",
        "https://storiesstatic.s3.us-west-2.amazonaws.com/media/uploads/splitted_9.mp4",
        "https://storiesstatic.s3.us-west-2.amazonaws.com/media/uploads/splitted_10.mp4",
        "https://storiesstatic.s3.us-west-2.amazonaws.com/media/uploads/splitted_11.mp4",
        "https://storiesstatic.s3.us-west-2.amazonaws.com/media/uploads/splitted_12.mp4",
        "https://storiesstatic.s3.us-west-2.amazonaws.com/media/uploads/splitted_13.mp4",
    ]
    processed_chunks = []
    for chunk in links:
        duration, transcribe_id, sentences, text = get_transcribe(chunk)
        title, text = summarization_extractor(text)
        if title != "" and title != " ":
            t = title
        else:
            t = "Unnamed lesson"
        res = ProcessedChunk(
            media_url=chunk,
            transcribe_id=transcribe_id, heading=title, paragraph=text,
            name=t
        )
        processed_chunks.append(res)

    processed_lessons = []
    for index, lesson in enumerate(processed_chunks):
        m_c = LessonMediaInstanceEntity(video_url=lesson.media_url, speaker_image="", transcribe_id=lesson.transcribe_id)
        r_m = CourseLessonEntity(
            title=lesson.name, order=index, type=LessonTypeEnum.MEDIA, complexity=LessonComplexityEnum.MID,
            media_content=m_c
        )
        t_c = LessonTextContentEntity(image_url="", heading=lesson.heading, paragraph=lesson.paragraph)
        r_t = CourseLessonEntity(
            title=lesson.name, order=index+1, type=LessonTypeEnum.TEXT, complexity=LessonComplexityEnum.MID,
            text_content=t_c
        )
        processed_lessons.append(r_m)
        processed_lessons.append(r_t)
    res = CourseSectionEntity(title="Intro section", order=0, auto_creation_id="", lessons=processed_lessons)
    return res
