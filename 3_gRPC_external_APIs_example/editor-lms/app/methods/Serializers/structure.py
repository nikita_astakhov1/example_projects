from uuid import UUID
from protos.editor.Editor.UpdateCourse.UpdateCourse_pb2 import (
    UpdateEditorRequest, UpdateEditorResponse, RoadmapContent,
    EditorSection, EditorLesson, TestContent, MediaContent,
    TextContent
)
from app.models.sctructure_entities import (
    CourseSectionEntity, CourseLessonEntity, LessonTextContentEntity,
    LessonRoadmapContentEntity, RoadmapInstanceEntity,
    LessonMediaInstanceEntity, LessonTestInstanceEntity
)
from app.constants import LessonTypeEnum, LessonComplexityEnum


def process_structure_push(data: UpdateEditorRequest, user_id: str) -> CourseSectionEntity:
    lessons = []
    for index, lesson in enumerate(data.payload.lessons):
        roadmap = None
        media = None
        test = None
        try:
            les = lesson.text_content
            text = LessonTextContentEntity(
                image_url=les.image_url, heading=les.heading,
                paragraph=les.paragraph
            )
        except AttributeError:
            text = None
        try:
            v = lesson.roadmap_content
            vals = []
            for ind, val in enumerate(v.values):
                a = RoadmapInstanceEntity(order=ind, name=val.name)
                vals.append(a)
            roadmap = LessonRoadmapContentEntity(heading=v.heading, values=vals)
        except AttributeError:
            text = None
        try:
            m = lesson.media_content
            media = LessonMediaInstanceEntity(
                video_url=m.video_url, speaker_image=m.speaker_image,
                transcribe_id=m.transcribe_id
            )
        except AttributeError:
            text = None
        try:
            a = lesson.test_content
            ans = [a for a in a.answers]
            test = LessonTestInstanceEntity(text=a.text, answers=ans)
        except AttributeError:
            text = None
        try:
            type = lesson.type
        except AttributeError:
            type = LessonTypeEnum.TEXT
        try:
            compx = lesson.complexity
        except AttributeError:
            compx = LessonComplexityEnum.EASY

        res = CourseLessonEntity(
            external_id=lesson.id, author_id=UUID(user_id), title=lesson.title,
            order=index, type=type, complexity=compx,
            text_content=text, roadmap_content=roadmap, media_content=media, test_content=test
        )
        lessons.append(res)
    try:
        magic = data.payload.auto_creation_id
    except AttributeError:
        magic = ""
    return CourseSectionEntity(
        external_id=data.payload.id, author_id=UUID(user_id),
        title=data.payload.title, order=data.payload.order, auto_creation_id=magic,
        lessons=lessons
    )


def process_structure_pull(
        data: CourseSectionEntity, updated_by: str, after_id: str, type: int,
        editor_id: str
) -> UpdateEditorResponse:
    lessons = []
    for lesson in data.lessons:
        text = None
        if lesson.text_content:
            text = TextContent(
                id=str(lesson.text_content.id),
                image_url=lesson.text_content.image_url,
                heading=lesson.text_content.heading,
                paragraph=lesson.text_content.paragraph
            )
        roadmap = None
        if lesson.roadmap_content:
            vals = [RoadmapContent.Values(order=val.order, name=val.name) for val in lesson.roadmap_content.values]
            roadmap = RoadmapContent(
                id=str(lesson.roadmap_content.id), heading=lesson.roadmap_content.heading,
                values=vals
            )
        media = None
        if lesson.media_content:
            media = MediaContent(
                id=str(lesson.media_content.id), video_url=lesson.media_content.video_url,
                speaker_image=lesson.media_content.speaker_image, transcribe_id=lesson.media_content.transcribe_id
            )
        test = None
        if lesson.test_content:
            ans = [TestContent.Answers(answer=a) for a in lesson.test_content.answers]
            test = TestContent(
                id=str(lesson.test_content.id), text=lesson.test_content.text,
                answers=ans
            )
        processed = EditorLesson(
            id=str(lesson.external_id), title=lesson.title, order=lesson.order,
            type=lesson.type.value, complexity=lesson.complexity.value,
            text_content=text,
            roadmap_content=roadmap,
            media_content=media,
            test_content=test
        )
        lessons.append(processed)
    payload = EditorSection(
        id=str(data.external_id), title=data.title, order=data.order,
        auto_creation_id=str(data.auto_creation_id), lessons=lessons
    )
    return UpdateEditorResponse(
        editor_id=editor_id, updated_by=updated_by, type=type, after_id=after_id, payload=payload
    )
