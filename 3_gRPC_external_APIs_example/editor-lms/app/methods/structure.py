from grpc import StatusCode
from protos.editor.Editor.UpdateCourse.UpdateCourse_pb2 import (
    UpdateEditorRequest, PushResponse, CreateEditorResponse
)

from app.constants import UpdateTypeEnum
from app.methods.Serializers.structure import process_structure_push, process_structure_pull
from app.database.structure import EditorObject, SectionObject
from app.models.sctructure_entities import CourseEditorEntity


def create_empty_editor(course_id, team_id):
    ins = [
        CourseEditorEntity(
            course_id=course_id, team_id=team_id,
            default_bucket_path=str(team_id) + "/" + str(course_id), sections=[]
        )
    ]
    obj = EditorObject().save_instances(instances=ins)[0]
    return StatusCode.OK, "Editor model created", CreateEditorResponse(team_id=str(team_id), editor_id=str(obj.id))


def push_structure_update(data: UpdateEditorRequest, user_id: str):
    """
    What this method is about? What the purpose?
    --> Save changes in a course structure in the database.
    --> The actions?
    Find necessary instance of editor structure and save update
    It can send an error, that will be visible to a client, but the stream will not react on changes.
    # Entities
    # Database
    # RPC
    What types of structure updates can be presented and on what levels?
    Section[Create, Delete, Update] ,,later will be order change
    Lesson[Create, Delete, Update(including order change)] -> We will completly remove and create new section
    How this actually works? On any update?
    How delete actually works? It must be in a one request, I don't want to write requests for each action.

    Final response in RPC stream need to be: Delete at[uuid], Insert this[uuid] after[uuid], Change at[uuid].
    Send sections completely.
    :return:
    """
    code = StatusCode.OK
    message = "Successfully updated"
    response = PushResponse()

    # obj from proto data
    serialized = process_structure_push(data=data, user_id=user_id)
    if data.type == UpdateTypeEnum.CREATE.value:
        # add at a last position
        EditorObject().add_sections(editor_id=data.editor_id, instances=[serialized])
        return StatusCode.OK, "Section successfully created", PushResponse()
    elif data.type == UpdateTypeEnum.UPDATE.value:
        # update section basic info, delete all lessons and add from serializer
        pass
    elif data.type == UpdateTypeEnum.DELETE.value:
        # delete the section and all assigned data
        pass
    else:
        pass
    return code, message, response


def stream_structure_update(editor_id, action_type, section_id, updated_by):
    if action_type == UpdateTypeEnum.CREATE.value:
        # add at a last position
        obj = SectionObject().get_complete_instance_external(editor_id=editor_id, external_id=section_id)
        previous_obj = SectionObject().get_previous(editor_id=editor_id, order=obj.order)
        response = process_structure_pull(
            data=obj, updated_by=updated_by, after_id=previous_obj,
            type=int(action_type), editor_id=editor_id
        )
        return response
    elif action_type == UpdateTypeEnum.UPDATE.value:
        # update section basic info, delete all lessons and add from serializer
        return None
    elif action_type == UpdateTypeEnum.DELETE.value:
        # delete the section and all assigned data
        return None
    else:
        return None
