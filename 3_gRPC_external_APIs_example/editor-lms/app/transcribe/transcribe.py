from app.transcribe import utils


def get_transcribe(upload_url):
    key = "YOUR_KEY"

    duration = None
    transcribe_id = None
    sentences = None
    text = ""

    header = {
        'authorization': key,
        'content-type': 'application/json'
    }

    # Request a transcription
    transcript_response = utils.request_transcript(upload_url, header)
    transcribe_id = transcript_response['id']

    # Create a polling endpoint that will let us check when the transcription is complete
    polling_endpoint = utils.make_polling_endpoint(transcript_response)

    # Wait until the transcription is complete
    utils.wait_for_completion(polling_endpoint, header)

    # Request the sentences with complete brickdown of the transcript
    sentences = utils.get_sentences(polling_endpoint, header)

    # Request the paragraphs of the transcript
    paragraphs = utils.get_paragraphs(polling_endpoint, header)
    for para in paragraphs:
        text += para['text']
    duration = sentences['audio_duration']

    # Development prints
    # print(sentences)
    # print("The ID is: ", transcribe_id)
    # print("The duration is: ", duration)
    # print("The complete text is: ", "\n", text)
    return duration, transcribe_id, sentences, text
