import logging
import grpc
import time

from protos.editor import service_pb2_grpc
from protos.editor.Editor.UpdateCourse import UpdateCourse_pb2
from protos.editor.Editor.ConvertMedia import ConvertMedia_pb2

from config import settings
from constants import UpdateTypeEnum, LessonTypeEnum, LessonComplexityEnum


def generate_push_request():
    did2 = "WyIweDQ3MGE5ZDA0ZjZkMWMyYzJmYmU1YjFjZDI3YTQ5ZThjNDc3MWNiYzQxMTUyZTZhMmFiY2RiMzQ5ODYwODQzMmY1YTBhZjU1YzVlZTNkYTRmYzVmNGJkYTFhZjVmYWNlOWIzNjE1NTYzNzI2MDRjNjBkMmQ5MjlkNzRjZDY5ZWYwMWMiLCJ7XCJpYXRcIjoxNjQ5NTE2MjYxLFwiZXh0XCI6MTA2NDk1MTYyNjEsXCJpc3NcIjpcImRpZDpldGhyOjB4Mjg2ZjI1RGQwOTI0ODAxMWQ4ZDk4OTNCYTJEYzVDQkNBN2QyOGFhM1wiLFwic3ViXCI6XCI2MFBPU1NTVGZ2TGp3R1JQNnpZTWgxLWt3NC1fenh3eDJESjhRVjZIUm8wPVwiLFwiYXVkXCI6XCJza0hJRXAyNVlkMGhvaHhrYWpiaTc4cGZaZ2JmUTdKQ3BtSmhCUm1XWkpJPVwiLFwibmJmXCI6MTY0OTUxNjI2MSxcInRpZFwiOlwiNDBhODMyOWQtZmRlZC00ZmY1LWIyMzctYjY4YThjNzlmMTI5XCIsXCJhZGRcIjpcIjB4M2NjMTIwMTIzOGRjZDY5YWEwMzZiZGE3ZTdlMTY1NmFlN2EyNmY3NGY3MjVjNzBhMTI2YjkyNDRhODE1MTZlYTM5YmU3MGIyNWQxYzJiYTM1Y2VlZmIyODE4NjFmNTVlZmI2NjU1MmUwN2EwOWI1YWQ3YWQ1ODliMzU3NDcxMDQxY1wifSJd"
    lessons = [
        UpdateCourse_pb2.EditorLesson(
            id="ade3f111-d335-11ef-af1c-363a8e722e61", title="Ахуеть урок", order=1, type=LessonTypeEnum.TEXT.value,
            complexity=LessonComplexityEnum.MID.value,
            text_content=UpdateCourse_pb2.TextContent(
                id="ade3f111-d335-22ef-af2c-363a8e722e51", image_url="",
                heading="Четвертая блять секция",
                paragraph="И ахуительный текст тут!"
            )
        ),
        UpdateCourse_pb2.EditorLesson(
            id="ade3f111-d335-33ef-af3c-363a8e722e62", title="Второй пиздатый урок", order=1, type=LessonTypeEnum.MEDIA.value,
            complexity=LessonComplexityEnum.MID.value,
            text_content=UpdateCourse_pb2.TextContent(
                id="ade3f111-d335-44ef-af4c-363a8e722e52", image_url="",
                heading="Круто пиздец",
                paragraph="С ума можно сайти магия блять"
            ),
            media_content=UpdateCourse_pb2.MediaContent(
                id="ade3f111-d335-55ef-af5c-363a8e722e75", video_url="", speaker_image="", transcribe_id="no"
            )
        )
    ]
    payload = UpdateCourse_pb2.EditorSection(
        id="ade3f245-d335-11ef-af6c-363a8e722e92", title="HH section",
        order=1, auto_creation_id="",
        lessons=[]
    )
    # team_id="FB09CF03-5883-4665-935A-3516C57ECDB5", editor_id="679cec3f-2178-471b-8e9a-d986f7b94a46"
    # team_id="ade3f918-d335-41ef-af9c-363a8e722e50", editor_id="bea47b78-a2b1-4ba2-a0f0-b7b611910f8b",
    return UpdateCourse_pb2.UpdateEditorRequest(
        token=did2, team_id="ade3f918-d335-41ef-af9c-363a8e722e50", editor_id="0b6f3d36-5f78-4675-960f-858e09963276",
        type=UpdateTypeEnum.CREATE.value,
        payload=payload
    )


def _channel(host : str, tls : bool):
    if tls:
        credentials = grpc.ssl_channel_credentials()
        return grpc.secure_channel(host, credentials)
    else:
        return grpc.insecure_channel(host)


def test_stream_request_iterator():
    did2 = "WyIweDQ3MGE5ZDA0ZjZkMWMyYzJmYmU1YjFjZDI3YTQ5ZThjNDc3MWNiYzQxMTUyZTZhMmFiY2RiMzQ5ODYwODQzMmY1YTBhZjU1YzVlZTNkYTRmYzVmNGJkYTFhZjVmYWNlOWIzNjE1NTYzNzI2MDRjNjBkMmQ5MjlkNzRjZDY5ZWYwMWMiLCJ7XCJpYXRcIjoxNjQ5NTE2MjYxLFwiZXh0XCI6MTA2NDk1MTYyNjEsXCJpc3NcIjpcImRpZDpldGhyOjB4Mjg2ZjI1RGQwOTI0ODAxMWQ4ZDk4OTNCYTJEYzVDQkNBN2QyOGFhM1wiLFwic3ViXCI6XCI2MFBPU1NTVGZ2TGp3R1JQNnpZTWgxLWt3NC1fenh3eDJESjhRVjZIUm8wPVwiLFwiYXVkXCI6XCJza0hJRXAyNVlkMGhvaHhrYWpiaTc4cGZaZ2JmUTdKQ3BtSmhCUm1XWkpJPVwiLFwibmJmXCI6MTY0OTUxNjI2MSxcInRpZFwiOlwiNDBhODMyOWQtZmRlZC00ZmY1LWIyMzctYjY4YThjNzlmMTI5XCIsXCJhZGRcIjpcIjB4M2NjMTIwMTIzOGRjZDY5YWEwMzZiZGE3ZTdlMTY1NmFlN2EyNmY3NGY3MjVjNzBhMTI2YjkyNDRhODE1MTZlYTM5YmU3MGIyNWQxYzJiYTM1Y2VlZmIyODE4NjFmNTVlZmI2NjU1MmUwN2EwOWI1YWQ3YWQ1ODliMzU3NDcxMDQxY1wifSJd"
    for i in range(0, 15):
        time.sleep(5)
        req = UpdateCourse_pb2.UpdateEditorRequest(token=did2, team="ade3f918-d335-41ef-af9c-363a8e722e50",
                                               course="ade3f918-d335-41ef-af9c-363a8e722e50",
                                               message="Hi I'm first client " + str(i))
        yield req


def test_stream(stub: service_pb2_grpc.EditorStub):
    did2 = "WyIweDQ3MGE5ZDA0ZjZkMWMyYzJmYmU1YjFjZDI3YTQ5ZThjNDc3MWNiYzQxMTUyZTZhMmFiY2RiMzQ5ODYwODQzMmY1YTBhZjU1YzVlZTNkYTRmYzVmNGJkYTFhZjVmYWNlOWIzNjE1NTYzNzI2MDRjNjBkMmQ5MjlkNzRjZDY5ZWYwMWMiLCJ7XCJpYXRcIjoxNjQ5NTE2MjYxLFwiZXh0XCI6MTA2NDk1MTYyNjEsXCJpc3NcIjpcImRpZDpldGhyOjB4Mjg2ZjI1RGQwOTI0ODAxMWQ4ZDk4OTNCYTJEYzVDQkNBN2QyOGFhM1wiLFwic3ViXCI6XCI2MFBPU1NTVGZ2TGp3R1JQNnpZTWgxLWt3NC1fenh3eDJESjhRVjZIUm8wPVwiLFwiYXVkXCI6XCJza0hJRXAyNVlkMGhvaHhrYWpiaTc4cGZaZ2JmUTdKQ3BtSmhCUm1XWkpJPVwiLFwibmJmXCI6MTY0OTUxNjI2MSxcInRpZFwiOlwiNDBhODMyOWQtZmRlZC00ZmY1LWIyMzctYjY4YThjNzlmMTI5XCIsXCJhZGRcIjpcIjB4M2NjMTIwMTIzOGRjZDY5YWEwMzZiZGE3ZTdlMTY1NmFlN2EyNmY3NGY3MjVjNzBhMTI2YjkyNDRhODE1MTZlYTM5YmU3MGIyNWQxYzJiYTM1Y2VlZmIyODE4NjFmNTVlZmI2NjU1MmUwN2EwOWI1YWQ3YWQ1ODliMzU3NDcxMDQxY1wifSJd"
    # my_event_stream = stub.UpdateCourse(test_stream_request_iterator())
    rq = UpdateCourse_pb2.UpdatesRequest(token=did2, team="ade3f918-d335-41ef-af9c-363a8e722e51", course="ade3f918-d335-41ef-af9c-363a8e722e51")
    for response in stub.UpdatesStream(rq):
        print("The response from update: ", response)
    # send_queue.push(StreamingMessage())
    # response = next(my_event_stream)
    # response = stub.UpdateCourse(req)


def test_send_message(stub: service_pb2_grpc.EditorStub):
    res = stub.PushUpdate(generate_push_request())
    return res


def test_media_converter(stub: service_pb2_grpc.EditorMagicConverterStub):
    did2 = "WyIweDQ3MGE5ZDA0ZjZkMWMyYzJmYmU1YjFjZDI3YTQ5ZThjNDc3MWNiYzQxMTUyZTZhMmFiY2RiMzQ5ODYwODQzMmY1YTBhZjU1YzVlZTNkYTRmYzVmNGJkYTFhZjVmYWNlOWIzNjE1NTYzNzI2MDRjNjBkMmQ5MjlkNzRjZDY5ZWYwMWMiLCJ7XCJpYXRcIjoxNjQ5NTE2MjYxLFwiZXh0XCI6MTA2NDk1MTYyNjEsXCJpc3NcIjpcImRpZDpldGhyOjB4Mjg2ZjI1RGQwOTI0ODAxMWQ4ZDk4OTNCYTJEYzVDQkNBN2QyOGFhM1wiLFwic3ViXCI6XCI2MFBPU1NTVGZ2TGp3R1JQNnpZTWgxLWt3NC1fenh3eDJESjhRVjZIUm8wPVwiLFwiYXVkXCI6XCJza0hJRXAyNVlkMGhvaHhrYWpiaTc4cGZaZ2JmUTdKQ3BtSmhCUm1XWkpJPVwiLFwibmJmXCI6MTY0OTUxNjI2MSxcInRpZFwiOlwiNDBhODMyOWQtZmRlZC00ZmY1LWIyMzctYjY4YThjNzlmMTI5XCIsXCJhZGRcIjpcIjB4M2NjMTIwMTIzOGRjZDY5YWEwMzZiZGE3ZTdlMTY1NmFlN2EyNmY3NGY3MjVjNzBhMTI2YjkyNDRhODE1MTZlYTM5YmU3MGIyNWQxYzJiYTM1Y2VlZmIyODE4NjFmNTVlZmI2NjU1MmUwN2EwOWI1YWQ3YWQ1ODliMzU3NDcxMDQxY1wifSJd"
    media_url = "https://storiesstatic.s3.us-west-2.amazonaws.com/media/uploads/158-what-is-machine-learning.mp4"
    req = ConvertMedia_pb2.ConvertMediaRequest(
        token=did2, team_id="ade3f918-d335-41ef-af9c-363a8e722e50", course_id="ade3f918-d335-41ef-af9c-363a8e722e50",
        editor_id="ade3f918-d335-41ef-af9c-363a8e722e50", media_url=media_url
    )
    response = stub.ConvertMediaInitial(req)


def test_create_section(stub: service_pb2_grpc.EditorStub):
    did2 = "WyIweDQ3MGE5ZDA0ZjZkMWMyYzJmYmU1YjFjZDI3YTQ5ZThjNDc3MWNiYzQxMTUyZTZhMmFiY2RiMzQ5ODYwODQzMmY1YTBhZjU1YzVlZTNkYTRmYzVmNGJkYTFhZjVmYWNlOWIzNjE1NTYzNzI2MDRjNjBkMmQ5MjlkNzRjZDY5ZWYwMWMiLCJ7XCJpYXRcIjoxNjQ5NTE2MjYxLFwiZXh0XCI6MTA2NDk1MTYyNjEsXCJpc3NcIjpcImRpZDpldGhyOjB4Mjg2ZjI1RGQwOTI0ODAxMWQ4ZDk4OTNCYTJEYzVDQkNBN2QyOGFhM1wiLFwic3ViXCI6XCI2MFBPU1NTVGZ2TGp3R1JQNnpZTWgxLWt3NC1fenh3eDJESjhRVjZIUm8wPVwiLFwiYXVkXCI6XCJza0hJRXAyNVlkMGhvaHhrYWpiaTc4cGZaZ2JmUTdKQ3BtSmhCUm1XWkpJPVwiLFwibmJmXCI6MTY0OTUxNjI2MSxcInRpZFwiOlwiNDBhODMyOWQtZmRlZC00ZmY1LWIyMzctYjY4YThjNzlmMTI5XCIsXCJhZGRcIjpcIjB4M2NjMTIwMTIzOGRjZDY5YWEwMzZiZGE3ZTdlMTY1NmFlN2EyNmY3NGY3MjVjNzBhMTI2YjkyNDRhODE1MTZlYTM5YmU3MGIyNWQxYzJiYTM1Y2VlZmIyODE4NjFmNTVlZmI2NjU1MmUwN2EwOWI1YWQ3YWQ1ODliMzU3NDcxMDQxY1wifSJd"
    req = UpdateCourse_pb2.CreateEditorRequest(
        token=did2, team_id="ade3f918-d335-41ef-af9c-363a8e722e50", course_id="ade3f918-d335-41ef-af9c-363a8e722e53"
    )
    response = stub.CreateEditor(req)


def run():
    a = "stage.lms.storiesed.com:443"
    with _channel(a, True) as channel:
        # stub = service_pb2_grpc.EditorMagicConverterStub(channel)
        # test_media_converter(stub)
        stub = service_pb2_grpc.EditorStub(channel)
        # test_create_section(stub)
        test_send_message(stub)
        #threading.Thread(target=test_stream(stub), daemon=True).start()


if __name__ == '__main__':
    logging.basicConfig()
    run()
