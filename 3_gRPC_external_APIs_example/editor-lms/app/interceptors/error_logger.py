import logging
from grpc_interceptor import ServerInterceptor


class ErrorLogger(ServerInterceptor):
    def intercept(self, method, request, context, method_name):
        try:
            return method(request, context)
        except Exception as e:
            logging.error(e)
            raise e
