#!/usr/bin/env bash

find ./protos -type f -name "*.proto" -print0 | while IFS= read -r -d $'\0' file
do
  dir=$(echo "$file" | sed 's/\.\/protos\///')
  dir=$(dirname "$dir")
  python -m grpc_tools.protoc -I. --grpc_python_out=./dist/python --python_out=./dist/python  "$file"
done
