#!/usr/bin/env bash

find ./protos -type f -name "*.proto" -print0 | while IFS= read -r -d $'\0' file
do
  protoc --swift_opt=Visibility=Public --swift_out=./dist/swift/Sources --grpc-swift_out=./dist/swift/Sources -I./ "$file"
done
