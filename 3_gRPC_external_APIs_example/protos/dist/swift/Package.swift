// swift-tools-version:5.2
import PackageDescription

let package = Package(
    name: "protos",
    products: [
        .library(name: "protos", targets: ["protos"]),
    ],
    targets: [
        .target(
            name: "protos",
            dependencies: []),
    ]
)
