import setuptools
import os

version = os.environ.get("CI_COMMIT_TAG", os.environ.get("CI_PIPELINE_IID") or "0-unknown")

setuptools.setup(
    name="protos",
    version=version,
    author="Storiesed",
    author_email="admin@storiesed.com",
    description="gRPC related stuff",
    include_package_data=True,
    packages=setuptools.find_namespace_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.8',
    install_requires = [
        'grpcio==1.43.0',
        'grpcio-tools==1.43.0',
        'protobuf==3.19.4',
    ],
)
