#!/usr/bin/env bash
set -e

pushd app
alembic upgrade head
popd

python server.py
