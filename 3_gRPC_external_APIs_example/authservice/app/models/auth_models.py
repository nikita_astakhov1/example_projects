import datetime
import uuid

from sqlalchemy import Column, String, Boolean, DateTime
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.ext.declarative import declarative_base, DeclarativeMeta
from sqlalchemy.sql import func

Base: DeclarativeMeta = declarative_base()


class BaseModel(Base):
    __abstract__ = True

    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4, index=True)
    created_at = Column(DateTime(timezone=True), default=datetime.datetime.utcnow, server_default=func.now())
    updated_at = Column(DateTime(timezone=True), default=datetime.datetime.utcnow, onupdate=func.now())

    def dict(self) -> dict:
        return self.__dict__


class UserModel(BaseModel):
    __tablename__ = "user_model"

    active = Column(Boolean, nullable=False, default=True)
    internal_team = Column(Boolean, nullable=False, default=False)
    email = Column(String, nullable=False, default="")
    issuer = Column(String, nullable=False, default="")
