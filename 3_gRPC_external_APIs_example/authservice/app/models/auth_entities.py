import datetime
from typing import Optional
from uuid import UUID

from pydantic import BaseModel as Base


class BaseEntity(Base):
    def dict(self, **kwargs):
        d = super().dict(**kwargs)

        for key, value in d.items():
            if type(value) == UUID:
                d[key] = str(value)

        return d


class BaseModelEntity(BaseEntity):
    id: Optional[UUID]
    created_at: Optional[datetime.datetime]
    updated_at: Optional[datetime.datetime]


class UserEntity(BaseModelEntity):
    active: bool
    internal_team: bool
    email: str
    issuer: str


class CreateUserEntityRequest(BaseEntity):
    active: bool
    internal_team: bool
    email: str
    issuer: str


class AuthorizedUserEntityResponse(BaseEntity):
    new: bool


class AuthorizedUserEntityRequest(BaseEntity):
    token: str
    lms: bool


class AuthTokenRequestEntity(BaseEntity):
    token: str


class AuthTokenResponseEntity(BaseEntity):
    id: Optional[UUID]
    active: bool


class AuthTokenDetailsResponseEntity(BaseEntity):
    id: Optional[UUID]
    active: bool
    email: str
