

class BaseGrpcException(Exception):

    def __init__(self, code: str, message: str, status: str = "error"):
        super().__init__(message)
        self.code = "authorization:" + code
        self.message = message
        self.status = status


class InputException(BaseGrpcException):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
