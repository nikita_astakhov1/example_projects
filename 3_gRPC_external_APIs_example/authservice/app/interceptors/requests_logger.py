import logging
from datetime import datetime

from grpc_interceptor import ServerInterceptor


class RequestsLogger(ServerInterceptor):

    def intercept(self, method, request, context, method_name):
        logging.info(f"[{datetime.now()}] {method_name} | {request}".strip())

        return method(request, context)
