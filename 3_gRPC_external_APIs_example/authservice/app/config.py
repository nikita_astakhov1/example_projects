import os

from dynaconf import Dynaconf

environment = os.getenv("PYTHON_ENV", "development")

settings = Dynaconf(
    envvar_prefix="STORIESED",
    settings_files=['settings.toml', '.secrets.toml'],
    environments=True,
    env=environment
)

# `envvar_prefix` = export envvars with `export DYNACONF_FOO=bar`.
# `settings_files` = Load this files in the order.
