import logging
import grpc

from protos.authorization import service_pb2_grpc
from protos.authorization.AuthorizationService.AuthorizeUser.AuthorizeUser_pb2 import AuthorizeUserRequest

from config import settings

"""
!!!!!Methods for testing purpose
"""


def _channel(host : str, tls : bool):
    if tls:
        credentials = grpc.ssl_channel_credentials()
        return grpc.secure_channel(host, credentials)
    else:
        return grpc.insecure_channel(host)


def check_auth(stub : service_pb2_grpc.AuthorizationServiceStub):
    response = stub.AuthorizeUser(AuthorizeUserRequest(token=stub.token, lms=True))


def run():
    with _channel(settings.client_grpc_host, settings.client_grpc_tls) as channel:
        stub = service_pb2_grpc.CheckTokenStub(channel)
        check_token(stub)


if __name__ == '__main__':
    logging.basicConfig()
    run()
