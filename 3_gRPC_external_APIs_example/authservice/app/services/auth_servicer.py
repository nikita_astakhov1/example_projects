import grpc
from protos.authorization import service_pb2_grpc
from protos.authorization.CheckToken.ReturnUser.ReturnUser_pb2 import AuthResponse
from protos.authorization.CheckToken.ReturnUserDetails.ReturnUserDetails_pb2 import AuthDetailsResponse

from app.models.auth_entities import AuthTokenRequestEntity
from app.magic.processor import MagicService


class AuthorizerServicer(service_pb2_grpc.CheckTokenServicer):

    def ReturnUser(self, request, context):
        if request.token is None:
            context.abort(code=grpc.StatusCode.UNAUTHENTICATED, details="Auth token is not valid")
        data = AuthTokenRequestEntity(token=request.token)
        user, code, message = MagicService().return_user(request=data)
        context.set_code(code)
        context.set_details(message)
        if user is not None:
            return AuthResponse(id=str(user.id), active=user.active)
        return AuthResponse()

    def ReturnUserDetails(self, request, context):
        if request.token is None:
            context.abort(code=grpc.StatusCode.UNAUTHENTICATED, details="Auth token is not valid.")
        data = AuthTokenRequestEntity(token=request.token)
        user, code, message = MagicService().return_user_details(request=data)
        context.set_code(code)
        context.set_details(message)
        if user is not None:
            return AuthDetailsResponse(id=str(user.id), active=user.active, email=user.email)
        return AuthDetailsResponse()
