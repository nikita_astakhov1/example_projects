import grpc
from protos.authorization import service_pb2_grpc
from protos.authorization.AuthorizationService.AuthorizeUser.AuthorizeUser_pb2 import AuthorizedUserResponse

from app.models.auth_entities import AuthorizedUserEntityRequest
from app.magic.processor import MagicService


class TeacherAuthServicer(service_pb2_grpc.AuthorizationServiceServicer):

    def AuthorizeUser(self, request, context):

        if request.token is None:
            context.abort(code=grpc.StatusCode.UNAUTHENTICATED, details="Auth token is not valid")

        data = AuthorizedUserEntityRequest(token=request.token, lms=request.lms)
        result, code, message = MagicService().authorize(request=data)
        context.set_code(code)
        context.set_details(message)
        if result is not None:
            return AuthorizedUserResponse(new=result.new)
        return AuthorizedUserResponse()
