from functools import wraps
from typing import List
from uuid import UUID

from sqlalchemy.orm.session import Session

from app.db import init_db_connection
from app.models.auth_entities import (
    BaseModelEntity, CreateUserEntityRequest, AuthTokenResponseEntity,
    AuthTokenDetailsResponseEntity
)
from app.models.auth_models import UserModel

from app.database.abstract import AbstractRepo, UserModelAbstractRepo

SessionMaker = init_db_connection()


def session_maker(func):
    @wraps(func)
    def wrapper(*args, **params):
        with SessionMaker() as session:
            return func(*args, **params, session=session)

    return wrapper


class AlchemyCore(AbstractRepo):
    model = None
    entity = None

    @session_maker
    def update(
            self,
            session: Session,
            pk_key: UUID,
            **params
    ) -> None:
        session.query(self.model).filter_by(id=pk_key).update(params, synchronize_session="fetch")
        session.commit()

    @session_maker
    def save_instances(self, session: Session, instances: List[BaseModelEntity]) -> list[entity]:
        save_objects = [
            self.model(**instance.dict()) for instance in instances
        ]
        session.add_all(instances=save_objects)
        session.commit()

        for inst in save_objects:
            session.refresh(inst)

        return [
            self.entity(**instance.dict()) for instance in save_objects
        ]

    @session_maker
    def get(self, session: Session, **params) -> entity:
        instance = session.query(self.model).filter_by(**params).order_by(self.model.created_at.desc()).first()
        if instance:
            return self.entity(**instance.dict())

        return None

    @session_maker
    def exists(self, session: Session, **params) -> bool:
        return session.query(session.query(self.model).filter_by(**params).exists()).scalar()


class CreateUserObject(UserModelAbstractRepo, AlchemyCore):
    model = UserModel
    entity = CreateUserEntityRequest


class CheckUserTokenObject(UserModelAbstractRepo, AlchemyCore):
    model = UserModel
    entity = AuthTokenResponseEntity


class CheckUserDetailedTokenObject(UserModelAbstractRepo, AlchemyCore):
    model = UserModel
    entity = AuthTokenDetailsResponseEntity
