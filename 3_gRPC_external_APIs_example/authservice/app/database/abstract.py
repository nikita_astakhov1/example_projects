import abc


class AbstractRepo(abc.ABC):
    model = None
    entity = None

    def get_entity(self) -> type:
        return self.entity

    def get_model(self) -> type:
        return self.model

    @abc.abstractmethod
    def save_instances(
            self,
            instances: list,
            **params
    ) -> list:
        ...

    @abc.abstractmethod
    def get(
            self,
            **params
    ) -> entity:
        ...

    @abc.abstractmethod
    def exists(
            self,
            **params
    ) -> bool:
        ...

    @abc.abstractmethod
    def update(
            self,
            pk_key,
            **params
    ) -> None:
        ...


class UserModelAbstractRepo(AbstractRepo):
    ...
