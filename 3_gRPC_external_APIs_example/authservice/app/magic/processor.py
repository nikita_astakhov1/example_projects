from grpc import StatusCode
from magic_admin import Magic
from magic_admin.error import DIDTokenError, RequestError

from app.models.auth_entities import (
    AuthorizedUserEntityRequest, AuthorizedUserEntityResponse,
    CreateUserEntityRequest, AuthTokenRequestEntity, AuthTokenResponseEntity,
    AuthTokenDetailsResponseEntity
)
from app.database.core import CreateUserObject, CheckUserTokenObject, CheckUserDetailedTokenObject


class MagicService:

    @staticmethod
    def __magic_validate_token(did_token):
        result_code = StatusCode.INVALID_ARGUMENT
        result_message = "Authorization token is missing"

        if did_token is None:
            return None, result_code, result_message

        magic = Magic(
            api_secret_key='sk_live_HERE_IS_THE_KEY',
            retries=5,
            timeout=5,
            backoff_factor=0.01
        )

        try:
            magic.Token.validate(did_token)
            issuer = magic.Token.get_issuer(did_token)
            result_code = StatusCode.OK
            result_message = "Token is valid."
            return issuer, result_code, result_message
        except DIDTokenError as e:
            result_message = 'DID Token is invalid: {}'.format(e)
            return None, result_code, result_message

    @staticmethod
    def __magic_get_user_instance(issuer):
        result_code = StatusCode.DATA_LOSS

        magic = Magic(
            api_secret_key='sk_live_HERE_IS_THE_KEY',
            retries=5,
            timeout=5,
            backoff_factor=0.01
        )

        try:
            magic_response = magic.User.get_metadata_by_issuer(
                issuer
            )
            result_code = StatusCode.OK
            result_message = "User data requested successfully."
            return magic_response, result_code, result_message
        except RequestError as e:
            result_message = "Magic failed: {}".format(e)
            return None, result_code, result_message

    def __authorize_teacher(self, request: AuthorizedUserEntityRequest):
        result_code = StatusCode.OK

        issuer, code, message = self.__magic_validate_token(request.token)
        if issuer is not None:
            connection = CreateUserObject()
            if connection.exists(issuer=issuer):
                result_message = "User already exist."
                return AuthorizedUserEntityResponse(new=False), result_code, result_message
            else:
                user_meta, code, message = self.__magic_get_user_instance(issuer)
                email = user_meta.data['email']
                new_user = CreateUserEntityRequest(
                    active=True,
                    internal_team=False,
                    email=email,
                    issuer=issuer
                )
                connection.save_instances(instances=[new_user])
                result_message = "User successfully created"
                return AuthorizedUserEntityResponse(new=True), result_code, result_message
        else:
            return issuer, code, message

    def authorize(self, request: AuthorizedUserEntityRequest):
        if request.lms:
            return self.__authorize_teacher(request)
        else:
            return None, StatusCode.ABORTED, "Students app is not supported yet."

    def return_user(self, request: AuthTokenRequestEntity):
        issuer, code, message = self.__magic_validate_token(request.token)
        if issuer is not None:
            db_connection = CheckUserTokenObject()
            user = db_connection.get(issuer=issuer)
            return AuthTokenResponseEntity(id=user.id, active=user.active), code, message

        return issuer, code, message

    def return_user_details(self, request: AuthTokenRequestEntity):
        issuer, code, message = self.__magic_validate_token(request.token)
        if issuer is not None:
            db_connection = CheckUserDetailedTokenObject()
            user = db_connection.get(issuer=issuer)
            return AuthTokenDetailsResponseEntity(id=user.id, active=user.active, email=user.email), code, message

        return issuer, code, message
