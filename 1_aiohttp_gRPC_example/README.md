# Reviews Service

Micro srvice reviews Chaos

## System install

### Python dependencies

1. `pip install -r requirements.txt`
2. `pip install chaos-grpc==0.0.139 --extra-index-url https://pip:tUgu3p3zAHmdGmiZVrxE@gitlab.stoker-dev.com/api/v4/projects/14/packages/pypi/simple`

### System dependencies

1. [Postgresql](https://www.postgresql.org/)
    1. `sudo docker run --rm --name chaos-postgres -e POSTGRES_PASSWORD=chaos -e POSTGRES_USER=chaos -d -p 127.0.0.1:5432:5432 -v $HOME/docker/volumes/postgres:/var/lib/postgresql/data postgis/postgis`(
       install everything [postgis](https://registry.hub.docker.com/r/postgis/postgis/))
2. [Rabbitmq](https://hub.docker.com/_/rabbitmq)
    1. `sudo docker run -d --name some-rabbit -p 5672:5672 -p 5673:5673 -p 15672:15672 rabbitmq:3-management`
    2. Open [http://127.0.0.1:15672](http://127.0.0.1:15672) and login with guest:guest
    3. Done

## System variables

System variables are in `src/constants.py`

Variables that are need to be set during launch:

1. `REVIEWS_URL`(default: `[::]`) - address that server will listen to
2. `REVIEWS_PORT`(default: `8002`) - port num that server will listen to
3. `CELERY_BROKER`(default: `amqp://`) - RabbitMq
4. `API_URL`(default: `http://127.0.0.1:8000`) - API
5. `POSTGRES_HOST`(default: `localhost`) - PostgreSQL
6. `POSTGRES_USER`(default: `chaos`) - db user
7. `POSTGRES_PASSWORD`(default: `chaos`) - db password
8. `POSTGRES_PORT`(default: `5432`) - db port
9. `REVIEWS_DB_NAME`(default: `reviews`) - db name
10. `DEVELOPER_MODE`(default: `False`) - Developer mode

### Start

After completing all previous steps you can launch the system.

1. Choose `src` as source path in pycharm
2. Run `src/main.py`
3. If you see `Server starting at ...` that mean that launch is successful

## Logs

All logs are in `review.log`

Test logs are in `test.log`

## Exceptions

Prefix: `reviews:`

- PREMISES_REVIEW_EXISTS - Review is already exist.
- PREMISES_REVIEW_NOT_FOUND - Review is not exist
- PREMISES_NOT_EXISTS - Venue is not exist
- USER_NO_ACCESS - User can't perform this action(reasons will be in message)
- PREMISES_RATING_NOT_FOUND - Rating is not found
- USER_REVIEW_EXISTS - Review is already exist.
- USER_REVIEW_NOT_FOUND - Review is not found
- CHAOS_REVIEW_EXISTS - Company review is already exist.
- REVIEW_ANSWER_EXISTS - Response to review is already exist.
- PAGINATION_PARAMS - Pagination params input error
- MARKS_PARAMS - Marks are not inputted correctly
- WRONG_INPUT_DATA - something missing in the request

## Migrations

For migrations is used `alembic==1.7.4`

Migrations folder `src/migration`

Alembic settings are in `src/alembic.ini`

Upgrade db `alembic upgrade head`, is located in `src`

### Migrations Test

Migrations tests are in `src/migration/tests`

To launch migrations tests run `src/migration/test/__init__.py`

### System variables

1. `TEST_POSTGRES_HOSTL`(default: `localhost`) - test db address
2. `TEST_POSTGRES_USER`(default: `chaos`) - test db user
3. `TEST_POSTGRES_PASSWORD`(default: `chaos`) - test db password
4. `TEST_POSTGRES_PORT`(default: `5432`) - test db port
5. `TEST_DATABASE_NAME_SUFFIX`(default: `chaos_test`) - test db suffix

## Tests

Tests can only check critical system errors. Business logic check is not performed.

All tests are running on mock data, without connection to real environment.

To run tests `src/tests/__init__.py`. 
Tests must be shutdown manually.

## Fixtures

Fixtures are in `src/fixtures/`, upload script is also there
