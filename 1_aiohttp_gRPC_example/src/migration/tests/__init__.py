"""
Test can find forgotten downgrade methods, undeleted data types in downgrade
methods, typos and many other errors.

Does not require any maintenance - you just add it once to check 80% of typos
and mistakes in migrations forever.
"""
from alembic.command import downgrade, upgrade
from alembic.config import Config
from alembic.script import Script, ScriptDirectory
from yarl import URL

from constants import TEST_DATABASE_CONNECT_URL
from migration.tests.utils import alembic_config_from_url, tmp_database


def get_revisions(db_url):
    # Create Alembic configuration object
    # (we don't need database for getting revisions list)
    config = alembic_config_from_url(db_url)

    # Get directory object with Alembic migrations
    revisions_dir = ScriptDirectory.from_config(config)

    # Get & sort migrations, from first to last
    revisions = list(revisions_dir.walk_revisions('base', 'heads'))
    revisions.reverse()

    return config, revisions


def test_migration_stairway(alembic_config: Config, revision: Script):
    upgrade(alembic_config, revision.revision)

    # We need -1 for downgrading first migration (its down_revision is None)
    downgrade(alembic_config, revision.down_revision or '-1')
    upgrade(alembic_config, revision.revision)


if __name__ == '__main__':
    with tmp_database(URL(TEST_DATABASE_CONNECT_URL)) as db_url:
        config, revisions = get_revisions(db_url)

        for rev in revisions:
            test_migration_stairway(config, rev)

    print("ALL DONE")
