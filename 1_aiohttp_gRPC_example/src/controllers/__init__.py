import inject

from chaos_grpc.reviews.create_chaos_review.create_chaos_review_pb2 import \
    CreateChaosReviewResponse, _CreateChaosReviewResponse
from chaos_grpc.reviews.create_premises_review.create_premises_review_pb2 import \
    CreatePremisesReviewResponse, _CreatePremisesReviewResponse
from chaos_grpc.reviews.create_premises_review_answer.create_premises_review_answer_pb2 import \
    CreatePremisesReviewAnswerResponse, _CreatePremisesReviewAnswerResponse
from chaos_grpc.reviews.create_user_review.create_user_review_pb2 import \
    CreateUserReviewResponse, _CreateUserReviewResponse
from chaos_grpc.reviews.create_user_review_answer.create_user_review_answer_pb2 import \
    CreateUserReviewAnswerResponse, _CreateUserReviewAnswerResponse
from chaos_grpc.reviews.get_all_premises_review.get_all_premises_review_pb2 import \
    GetAllPremisesReviewResponse, _PaginatedGetAllPremisesReviewResponse
from chaos_grpc.reviews.get_all_user_reviews.get_all_user_reviews_pb2 import \
    GetAllUserReviewsResponse, _PaginatedGetAllUserReviewsResponse
from chaos_grpc.reviews.get_chaos_reviews.get_chaos_reviews_pb2 import \
    GetChaosReviewsResponse, _GetChaosReviewsResponse
from chaos_grpc.reviews.get_premises_rating.get_premises_rating_pb2 import \
    GetPremisesRatingResponse, _GetPremisesRatingResponse
from chaos_grpc.reviews.get_premises_review_details.get_premises_review_details_pb2 import \
    GetPremisesReviewDetailsResponse, _GetPremisesReviewDetailsResponse
from chaos_grpc.reviews.get_premises_reviews_by_owner.get_premises_reviews_by_owner_pb2 import \
    GetPremisesReviewsByOwnerResponse, _PaginatedGetPremisesReviewsByOwnerResponse
from chaos_grpc.reviews.inner.inner_get_premises_rating.inner_get_premises_rating_pb2 import \
    InnerGetPremisesRatingResponse, _InnerGetPremisesRatingResponse
from chaos_grpc.reviews.service_pb2_grpc import ReviewsServicer
from chaos_grpc.reviews.update_premises_rating.update_premises_rating_pb2 import \
    UpdatePremisesRatingResponse, _UpdatePremisesRatingResponse
from services.review import ReviewService
from utils import grpc_exception_logger, grpc_request_logger


class ReviewController(ReviewsServicer):

    @inject.autoparams("review_service")
    def __init__(
            self,
            review_service: ReviewService
    ):
        self._review_service = review_service

    @grpc_request_logger
    @grpc_exception_logger
    async def get_premises_review_details(self, request, context):
        return GetPremisesReviewDetailsResponse(
            payload=(await self._review_service.get_premises_review_details(request))
                .to(_GetPremisesReviewDetailsResponse),
            status="200"
        )

    @grpc_request_logger
    @grpc_exception_logger
    async def get_all_premises_review(self, request, context):
        return GetAllPremisesReviewResponse(
            payload=(await self._review_service.get_all_premises_reviews(request))
                .to(_PaginatedGetAllPremisesReviewResponse),
            status="200"
        )

    @grpc_request_logger
    @grpc_exception_logger
    async def get_premises_reviews_by_owner(self, request, context):
        return GetPremisesReviewsByOwnerResponse(
            payload=(await self._review_service.get_premises_reviews_by_owner(request))
                .to(_PaginatedGetPremisesReviewsByOwnerResponse),
            status="200"
        )

    @grpc_request_logger
    @grpc_exception_logger
    async def create_premises_review(self, request, context):
        return CreatePremisesReviewResponse(
            payload=(await self._review_service.create_premises_review(request))
                .to(_CreatePremisesReviewResponse),
            status="200"
        )

    @grpc_request_logger
    @grpc_exception_logger
    async def get_premises_rating(self, request, context):
        return GetPremisesRatingResponse(
            payload=(await self._review_service.get_premises_rating(request))
                .to(_GetPremisesRatingResponse),
            status="200"
        )

    @grpc_request_logger
    @grpc_exception_logger
    async def update_premises_rating(self, request, context):
        return UpdatePremisesRatingResponse(
            payload=(await self._review_service.update_premises_rating(request))
                .to(_UpdatePremisesRatingResponse),
            status="200"
        )

    @grpc_request_logger
    @grpc_exception_logger
    async def create_premises_review_answer(self, request, context):
        return CreatePremisesReviewAnswerResponse(
            payload=(await self._review_service.create_premises_review_answer(request))
                .to(_CreatePremisesReviewAnswerResponse),
            status="200"
        )

    @grpc_request_logger
    @grpc_exception_logger
    async def get_all_user_reviews(self, request, context):
        return GetAllUserReviewsResponse(
            payload=(await self._review_service.get_all_user_reviews(request))
                .to(_PaginatedGetAllUserReviewsResponse),
            status="200"
        )

    @grpc_request_logger
    @grpc_exception_logger
    async def create_user_review(self, request, context):
        return CreateUserReviewResponse(
            payload=(await self._review_service.create_user_review(request))
                .to(_CreateUserReviewResponse),
            status="200"
        )

    @grpc_request_logger
    @grpc_exception_logger
    async def create_user_review_answer(self, request, context):
        return CreateUserReviewAnswerResponse(
            payload=(await self._review_service.create_user_review_answer(request))
                .to(_CreateUserReviewAnswerResponse),
            status="200"
        )

    @grpc_request_logger
    @grpc_exception_logger
    async def create_chaos_review(self, request, context):
        return CreateChaosReviewResponse(
            payload=(await self._review_service.create_chaos_review(request))
                .to(_CreateChaosReviewResponse),
            status="200"
        )

    @grpc_request_logger
    @grpc_exception_logger
    async def get_chaos_reviews(self, request, context):
        return GetChaosReviewsResponse(
            payload=(await self._review_service.get_chaos_reviews(request))
                .to(_GetChaosReviewsResponse),
            status="200"
        )

    # -----------------------------------INNER METHODS------------------------------------------

    @grpc_request_logger
    @grpc_exception_logger
    async def inner_get_premises_rating(self, request, context):
        return InnerGetPremisesRatingResponse(
            payload=(await self._review_service.inner_get_premises_rating(request))
                .to(_InnerGetPremisesRatingResponse),
            status="200"
        )
