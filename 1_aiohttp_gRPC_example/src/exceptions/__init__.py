

class BaseGrpcException(Exception):

    def __init__(self, code: str, message: str, status: str = "error"):
        super().__init__(message)
        self.code = "reviews:" + code
        self.message = message
        self.status = status
