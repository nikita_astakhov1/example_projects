import enum
import os

# HOST CONFIG
_REVIEWS_URL = os.environ.get('REVIEWS_URL', '[::]')
_REVIEWS_PORT = os.environ.get('REVIEWS_PORT', '8002')
REVIEWS_ADDRESS = address = f'{_REVIEWS_URL}:{_REVIEWS_PORT}'

# SYSTEM CONFIG
PRINT_REQUESTS = True
PRINT_EXCEPTION = True
PROJECT_NAME = "chaos"
DEVELOPER_MODE = os.environ.get('DEVELOPER_MODE', 'False').lower() in ('true', '1', 't')

# CELERY CONFIG
CELERY_BROKER = os.environ.get('CELERY_BROKER', 'amqp://')

# API CONFIG
API_URL = os.environ.get('API_URL', 'http://127.0.0.1:8000')

# DATABASE CONFIG
_POSTGRES_HOST = os.environ.get('POSTGRES_HOST', 'localhost')
_POSTGRES_USER = os.environ.get('POSTGRES_USER', 'chaos')
_POSTGRES_PASSWORD = os.environ.get('POSTGRES_PASSWORD', 'chaos')
_POSTGRES_PORT = os.environ.get('POSTGRES_PORT', 5432)
_REVIEWS_DB_NAME = os.environ.get('REVIEWS_DB_NAME', 'reviews')
DATABASE_CONNECT_URL = f"postgresql://{_POSTGRES_USER}:{_POSTGRES_PASSWORD}@{_POSTGRES_HOST}:{_POSTGRES_PORT}/{_REVIEWS_DB_NAME}"
print(DATABASE_CONNECT_URL)
# TESTS
# Migrations
_TEST_POSTGRES_HOST = os.environ.get('TEST_POSTGRES_HOST', 'localhost')
_TEST_POSTGRES_USER = os.environ.get('TEST_POSTGRES_USER', 'chaos')
_TEST_POSTGRES_PASSWORD = os.environ.get('TEST_POSTGRES_PASSWORD', 'chaos')
_TEST_POSTGRES_PORT = os.environ.get('TEST_POSTGRES_PORT', 5432)

TEST_DATABASE_CONNECT_URL = f"postgresql://{_POSTGRES_USER}:{_POSTGRES_PASSWORD}@{_POSTGRES_HOST}:{_POSTGRES_PORT}"

TEST_DATABASE_NAME_SUFFIX = os.environ.get('TEST_DATABASE_NAME_SUFFIX', 'chaos_test')

# Service
TEST_SERVER_ADDRESS = os.environ.get('TEST_SERVER_ADDRESS', '[::]:8112')

TEST_SERVER_UPTIME = 2


class ChaosReviewExperienceEnum(enum.Enum):
    MUCH_BETTER = 0
    SLIGHTLY_BETTER = 1
    ROUGHLY_EXPECTED = 2
    MUCH_WORSE = 3
