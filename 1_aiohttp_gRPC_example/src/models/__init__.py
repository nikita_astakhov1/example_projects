"""Object for database"""
import datetime
import uuid

from sqlalchemy import Column, String, Boolean, DateTime, Integer, Float, ForeignKey, Enum as SqlAlchemyEnum
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.ext.declarative import declarative_base, DeclarativeMeta
from sqlalchemy.orm import relationship
from sqlalchemy.sql import func

from constants import ChaosReviewExperienceEnum

Base: DeclarativeMeta = declarative_base()


class BaseModel(Base):
    __abstract__ = True

    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4, index=True)
    created_at = Column(DateTime(timezone=True), default=datetime.datetime.utcnow, server_default=func.now())
    updated_at = Column(DateTime(timezone=True), default=datetime.datetime.utcnow, onupdate=func.now())

    def dict(self) -> dict:
        return self.__dict__


class PremisesRatingModel(BaseModel):
    __tablename__ = "premises_rating"

    premises_id = Column(String, nullable=False, default=False)
    rating = Column(Float)
    count_reviews = Column(Integer, default=1)
    storage_conditions = Column(Float, default=5)
    matches_the_description = Column(Float, default=5)
    accessibility = Column(Float, default=5)
    communication = Column(Float, default=5)


class PremisesReviewModel(BaseModel):
    __tablename__ = "premises_reviews"

    author_id = Column(String, nullable=False, default=False)
    premises_id = Column(String, nullable=False, default=False)
    order_id = Column(String, nullable=False, default=False)
    owner_id = Column(String, nullable=False, default=False)
    is_visible = Column(Boolean, nullable=False, default=False)
    text = Column(String(2048), nullable=False, default="")

    storage_conditions = Column(Integer, nullable=False, default=5)
    matches_the_description = Column(Integer, nullable=False, default=5)
    accessibility = Column(Integer, nullable=False, default=5)
    communication = Column(Integer, nullable=False, default=5)

    is_counted = Column(Boolean, nullable=False, default=False)

    answer = relationship("PremisesReviewAnswerModel", backref="premises_review", uselist=False)


class PremisesReviewAnswerModel(BaseModel):
    __tablename__ = "premises_review_answer"

    author_id = Column(String, nullable=False, default=False)
    text = Column(String(2048), nullable=False, default="")

    premises_review_id = Column(UUID(as_uuid=True), ForeignKey('premises_reviews.id', ondelete='CASCADE'),
                                nullable=False)


class UserReviewModel(BaseModel):
    __tablename__ = "user_reviews"

    author_id = Column(String, nullable=False, default=False)
    premises_id = Column(String, nullable=False, default=False)
    order_id = Column(String, nullable=False, default=False)
    about_user_id = Column(String, nullable=False, default=False)
    is_visible = Column(Boolean, nullable=False, default=False)
    text = Column(String(2048), nullable=False, default="")

    answer = relationship("UserReviewAnswerModel", backref="user_review", uselist=False)


class UserReviewAnswerModel(BaseModel):
    __tablename__ = "user_review_answer"

    author_id = Column(String, nullable=False, default=False)
    text = Column(String(2048), nullable=False, default="")

    user_review_id = Column(UUID(as_uuid=True), ForeignKey('user_reviews.id', ondelete='CASCADE'), nullable=False)


class ChaosReviewModel(BaseModel):
    __tablename__ = "chaos_reviews"

    author_id = Column(String, nullable=False, default=False)
    order_id = Column(String, nullable=False, default=False)
    text = Column(String(2048), nullable=False, default="")
    mark = Column(Integer, nullable=False, default=5)
    storage_experience = Column(SqlAlchemyEnum(ChaosReviewExperienceEnum))
