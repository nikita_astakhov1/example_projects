import logging
import traceback
from datetime import datetime
from functools import wraps
from typing import Callable

import pydantic.error_wrappers
from chaos_grpc.base_pb2 import Response, Error

from constants import PRINT_EXCEPTION, PRINT_REQUESTS
from exceptions import BaseGrpcException


def grpc_exception_logger(func: Callable = None, raise_exception: bool = False):
    """
    Check how decorator is called:
    """

    def exception_logger_wrapper(func: Callable):
        @wraps(func)
        async def wrapper(*args, **kwargs):
            try:
                return await func(*args, **kwargs)
            except BaseGrpcException as e:
                return Response(
                    error=Error(
                        code=e.code,
                        message=e.message
                    ),
                    status=e.status
                )
            except pydantic.error_wrappers.ValidationError as e:
                return Response(
                    error=Error(
                        code="reviews:WRONG_INPUT_DATA",
                        message="Wrong input data"
                    ),
                    status="error"
                )
            except Exception as e:
                logging.log(logging.ERROR, e)

                if PRINT_EXCEPTION:
                    traceback.print_exc()

                if raise_exception:
                    raise e

                return Response(
                    error=Error(
                        code="error",
                        message=f"Server error at {datetime.now()}"
                    ),
                    status="server error"
                )

        return wrapper

    if func is None:
        return exception_logger_wrapper
    else:
        return exception_logger_wrapper(func)


def grpc_request_logger(func: Callable):
    """
    All incoming requests must have parameter 'request'(must be first), and must have a field 'request_id',
    and return response with a field 'status'

    Example:
    @grpc_request_logger
    async def some_route(request, ...):
        ...
    """


    if PRINT_REQUESTS:
        @wraps(func)
        async def grpc_request_logger_wrapper(*args, **kwargs):
            request_id = args[1].request_id

            if request_id is None or request_id == "":
                request_id = "NO_REQUEST_ID"

            print(f"REQUEST: {datetime.now()} - {func.__name__} - {request_id}")
            response = await func(*args, **kwargs)

            response_info = response.status
            if response.status != '200':
                response_info += " - " + response.error.code
            print(f"RESPONSE: {datetime.now()} - {func.__name__} - {request_id} - {response_info}")

            return response

        return grpc_request_logger_wrapper
    else:
        return func
