import json
from functools import wraps
from typing import Callable

from db import init_db_connection
from models import PremisesRatingModel, PremisesReviewModel, UserReviewModel

SessionMaker = init_db_connection()


def optional(func: Callable) -> Callable:
    @wraps(func)
    def wrapper(*args, **kwargs):
        try:
            func(*args, **kwargs)
        except Exception as e:
            print("\033[91m" + e.__class__.__name__ + "\033[0m")
        else:
            print("Correct")

    return wrapper


@optional
def upload_premises_rating():
    with open('premises_rating.json') as file:
        ratings = json.load(file)

        session = SessionMaker()
        session.add_all(
            instances=[
                PremisesRatingModel(**rating) for rating in ratings
            ]
        )
        session.commit()


@optional
def upload_premises_reviews():
    with open('premises_review.json') as file:
        reviews = json.load(file)

        session = SessionMaker()
        session.add_all(
            instances=[
                PremisesReviewModel(**review) for review in reviews
            ]
        )
        session.commit()


@optional
def upload_user_reviews():
    with open('user_review.json') as file:
        reviews = json.load(file)

        session = SessionMaker()
        session.add_all(
            instances=[
                UserReviewModel(**review) for review in reviews
            ]
        )
        session.commit()


if __name__ == '__main__':
    print("Upload premises rating...")
    upload_premises_rating()

    print("Upload premises reviews...")
    upload_premises_reviews()

    print("Upload user reviews...")
    upload_user_reviews()

    print("DONE")
