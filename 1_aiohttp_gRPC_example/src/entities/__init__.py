"""Objects to process"""
import datetime
import math
from typing import Optional
from uuid import UUID

from pydantic import BaseModel as Base

from constants import ChaosReviewExperienceEnum
from entities.exceptions import MarksInputException, PaginationParamsException


class BaseEntity(Base):
    def dict(self, **kwargs):
        d = super().dict(**kwargs)

        for key, value in d.items():
            if type(value) == UUID:
                d[key] = str(value)

        return d


class User(BaseEntity):
    id: str
    created_at: str
    firstname: str
    lastname: str
    about_me: str
    avatar_url: Optional[str]


class BaseModelEntity(BaseEntity):
    id: Optional[UUID]
    created_at: Optional[datetime.datetime]
    updated_at: Optional[datetime.datetime]


class PremisesRatingEntity(BaseModelEntity):
    premises_id: str
    rating: float
    count_reviews: float
    storage_conditions: float
    matches_the_description: float
    accessibility: float
    communication: float


class PremisesReviewEntity(BaseModelEntity):
    author_id: str
    premises_id: str
    order_id: str
    owner_id: str
    is_visible: bool = True
    text: str

    storage_conditions: int
    matches_the_description: int
    accessibility: int
    communication: int

    is_counted: bool = False


class PremisesReviewAnswerEntity(BaseModelEntity):
    author_id: str
    text: str

    premises_review_id: UUID


class UserReviewEntity(BaseModelEntity):
    author_id: str
    premises_id: str
    order_id: str
    is_visible: bool = True
    text: str
    about_user_id: str


class UserReviewAnswerEntity(BaseModelEntity):
    author_id: str
    text: str

    user_review_id: UUID


class ChaosReviewEntity(BaseModelEntity):
    author_id: str
    order_id: str
    text: str
    mark: int
    storage_experience: ChaosReviewExperienceEnum


class PageIteratorEntity:
    index: int = -1
    max: int
    items: list

    def __init__(self, items):
        super().__init__()
        self.index = -1
        self.max = len(items)
        self.items = items

    def __next__(self):
        self.index += 1
        if self.index == self.max:
            raise StopIteration
        return self.items[self.index]


class PageEntity(BaseEntity):
    items: list
    page: int
    max_page: int
    count: int
    total: int

    def __init__(self, items, page, limit, total):
        super().__init__(
            items=items,
            page=page,
            max_page=int(math.ceil(total / float(limit))),
            count=len(items),
            total=total
        )

    def __iter__(self):
        return PageIteratorEntity(items=self.items)

    def __len__(self):
        return len(self.items)

    def __getitem__(self, index):
        return self.items[index]
