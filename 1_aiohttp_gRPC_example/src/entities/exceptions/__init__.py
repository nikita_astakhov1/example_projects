from exceptions import BaseGrpcException


class InputException(BaseGrpcException):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


class MarksInputException(InputException):
    """Marks fields can't be more than 5 and less than 0"""

    def __init__(self):
        super().__init__(
            code="MARKS_PARAMS",
            message="Marks fields can't be more than 5 and less than 0"
        )


class PaginationParamsException(InputException):
    """If limit or page less than 1"""

    def __init__(self):
        super().__init__(
            code="PAGINATION_PARAMS",
            message="Limit and page can't be less than 1"
        )
