from uuid import UUID

from entities import BaseEntity


class CreateUserReviewEntityRequest(BaseEntity):
    author_id: str
    tenant_id: str
    order_id: str
    premises_id: str
    text: str


class CreateUserReviewEntityResponse(BaseEntity):
    id: UUID
    author_id: str
    about_user_id: str
    order_id: str
    premises_id: str
    created_at: str
    text: str
