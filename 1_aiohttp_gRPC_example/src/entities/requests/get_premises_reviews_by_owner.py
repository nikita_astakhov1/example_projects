from typing import Optional
from uuid import UUID

from entities import BaseEntity, User
from entities.requests import ReviewMarks, PaginationInfoEntityRequest, PaginationInfoEntityResponse, \
    ReviewAnswerEntity


class GetPremisesReviewsByOwnerEntityRequest(BaseEntity):
    pagination: PaginationInfoEntityRequest
    owner_id: str


class _PremisesReviewOwnerEntity(BaseEntity):
    id: UUID
    author: User
    created_at: str
    text: str
    marks: ReviewMarks
    answer: Optional[ReviewAnswerEntity]
    rating: float
    premises_id: str


class _GetPremisesReviewsByOwnerEntityResponse(BaseEntity):
    reviews: list


class PaginatedGetPremisesReviewsByOwnerEntityResponse(BaseEntity):
    pagination: PaginationInfoEntityResponse
    result: _GetPremisesReviewsByOwnerEntityResponse
    total_reviews: float
