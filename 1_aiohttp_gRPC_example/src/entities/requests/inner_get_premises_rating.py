from entities import BaseEntity
from entities.requests import Marks


class InnerGetPremisesRatingEntityRequest(BaseEntity):
    premises_id: str


class InnerGetPremisesRatingEntityResponse(BaseEntity):
    rating: float
    count_reviews: int
    marks: Marks
