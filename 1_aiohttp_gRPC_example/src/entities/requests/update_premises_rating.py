from entities import BaseEntity


class UpdatePremisesRatingEntityRequest(BaseEntity):
    premises_id: str
    order_id: str


class UpdatePremisesRatingEntityResponse(BaseEntity):
    pass
