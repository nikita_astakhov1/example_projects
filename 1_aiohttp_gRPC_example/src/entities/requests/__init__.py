from typing import Optional
from uuid import UUID

from pydantic import validator

from entities import BaseEntity, MarksInputException, PaginationParamsException


class ReviewMarks(BaseEntity):
    storage_conditions: int
    matches_the_description: int
    accessibility: int
    communication: int
    total: Optional[float]

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        for key, value in kwargs.items():
            if not 0 <= value <= 5:
                raise MarksInputException()

    @validator('total', always=True)
    def _calculate_total(cls, v, values):
        if v is None:
            return (values['storage_conditions'] + values['matches_the_description'] + values['accessibility'] + values[
                'communication']) / 4
        else:
            return v


class PaginationInfoEntityRequest(BaseEntity):
    page: int
    limit: int

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        if (self.page == 0) ^ (self.limit == 0) or self.page < 0 or self.limit < 0:
            raise PaginationParamsException()


class PaginationInfoEntityResponse(BaseEntity):
    page: int
    max_page: int
    count: int
    total: int


class ReviewAnswerEntity(BaseEntity):
    text: str
    review_id: UUID
    author_id: str
    created_at: str


class Marks(BaseEntity):
    storage_conditions: float
    matches_the_description: float
    accessibility: float
    communication: float
    total: Optional[float]

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        for key, value in kwargs.items():
            if not 0 <= value <= 5:
                raise MarksInputException()

    @validator('total', always=True)
    def _calculate_total(cls, v, values):
        if v is None:
            return (values['storage_conditions'] + values['matches_the_description'] + values['accessibility'] + values[
                'communication']) / 4
        else:
            return v
