from typing import Optional
from uuid import UUID

from entities import BaseEntity, User
from entities.requests import ReviewMarks, PaginationInfoEntityRequest, PaginationInfoEntityResponse, \
    ReviewAnswerEntity, Marks


class GetAllPremisesReviewEntityRequest(BaseEntity):
    pagination: PaginationInfoEntityRequest
    premises_id: str


class _PremisesReviewEntity(BaseEntity):
    id: UUID
    author: User
    created_at: str
    text: str
    marks: ReviewMarks
    answer: Optional[ReviewAnswerEntity]


class _RatingInfoEntity(BaseEntity):
    rating: float
    marks: Marks
    count_reviews: int


class _GetAllPremisesReviewEntityResponse(BaseEntity):
    reviews: list
    rating: _RatingInfoEntity


class PaginatedGetAllPremisesReviewEntityResponse(BaseEntity):
    pagination: PaginationInfoEntityResponse
    result: _GetAllPremisesReviewEntityResponse
    total_reviews: float
