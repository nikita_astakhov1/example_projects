from constants import ChaosReviewExperienceEnum
from entities import BaseEntity


class CreateChaosReviewEntityRequest(BaseEntity):
    order_id: str
    text: str
    mark: int
    storage_experience: ChaosReviewExperienceEnum
    author_id: str


class CreateChaosReviewEntityResponse(BaseEntity):
    author_id: str
    order_id: str
    created_at: str
    text: str
    mark: int
    storage_experience: ChaosReviewExperienceEnum
