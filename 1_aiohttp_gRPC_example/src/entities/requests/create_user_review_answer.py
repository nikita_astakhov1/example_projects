from uuid import UUID

from entities import BaseEntity


class CreateUserReviewAnswerEntityRequest(BaseEntity):
    author_id: str
    review_id: UUID
    text: str


class CreateUserReviewAnswerEntityResponse(BaseEntity):
    author_id: str
    review_id: UUID
    text: str
    created_at: str
