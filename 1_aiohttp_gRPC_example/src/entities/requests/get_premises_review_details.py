from uuid import UUID

from entities import BaseEntity, User
from entities.requests import ReviewMarks


class GetPremisesReviewDetailsEntityRequest(BaseEntity):
    review_id: UUID


class GetPremisesReviewDetailsEntityResponse(BaseEntity):
    id: UUID
    author: User
    created_at: str
    text: str
    marks: ReviewMarks
