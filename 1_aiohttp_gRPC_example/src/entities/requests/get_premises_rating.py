from entities import BaseEntity
from entities.requests import Marks


class GetPremisesRatingEntityRequest(BaseEntity):
    premises_id: str


class GetPremisesRatingEntityResponse(BaseEntity):
    rating: float
    count_reviews: int
    marks: Marks
