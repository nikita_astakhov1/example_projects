from typing import Optional

from entities import BaseEntity
from entities.requests import PaginationInfoEntityRequest, PaginationInfoEntityResponse, ReviewAnswerEntity


class GetAllUserReviewsEntityRequest(BaseEntity):
    pagination: PaginationInfoEntityRequest
    tenant_id: str


class _UserReviewEntity(BaseEntity):
    created_at: str
    text: str
    author_id: str
    answer: Optional[ReviewAnswerEntity]
    premises_id: str


class GetAllUserReviewsModelEntityResponse(BaseEntity):
    pagination: PaginationInfoEntityResponse
    result: list
