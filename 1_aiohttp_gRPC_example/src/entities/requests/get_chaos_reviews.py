from constants import ChaosReviewExperienceEnum
from entities import BaseEntity


class GetChaosReviewsEntityRequest(BaseEntity):
    pass


class _ChaosReviewEntity(BaseEntity):
    author_id: str
    order_id: str
    created_at: str
    text: str
    mark: int
    storage_experience: ChaosReviewExperienceEnum


class GetChaosReviewsEntityResponse(BaseEntity):
    reviews: list
