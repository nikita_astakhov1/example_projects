from uuid import UUID

from entities import BaseEntity
from entities.requests import ReviewMarks


class CreatePremisesReviewEntityRequest(BaseEntity):
    marks: ReviewMarks
    text: str
    order_id: str
    owner_id: str
    author_id: str
    premises_id: str


class CreatePremisesReviewEntityResponse(BaseEntity):
    id: UUID
    author_id: str
    order_id: str
    premises_id: str
    created_at: str
    text: str
    marks: ReviewMarks
