from uuid import UUID

from entities import BaseEntity


class CreatePremisesReviewAnswerEntityRequest(BaseEntity):
    author_id: str
    review_id: UUID
    text: str


class CreatePremisesReviewAnswerEntityResponse(BaseEntity):
    author_id: str
    review_id: UUID
    text: str
    created_at: str
