import uuid
from datetime import datetime

from constants import ChaosReviewExperienceEnum
from entities import \
    PremisesReviewEntity, PremisesRatingEntity, \
    UserReviewEntity, ChaosReviewEntity, PageEntity
from repositories.database import *


def _is_match(instance, params: dict):
    for key, value in params.items():
        if instance.__dict__.get(key) != value:
            return False

    return True


class TestRepo(AbstractRepo):

    def __init__(self):
        self._instances = self._get_test_instances()

    @classmethod
    def _get_test_instances(cls) -> list:
        return []

    def _filter(self, **params) -> list:
        result = []
        for instance in self._instances:
            if _is_match(instance, params):
                result.append(instance)

        return result

    def save_instances(self, instances: list, **params):
        for instance in instances:
            instance.id = uuid.uuid4()

        self._instances += instances

        return self._instances

    def list(self, page: int = 0, limit: int = 0, **params):
        result = self._filter(**params)

        if page and limit:
            return PageEntity(
                items=result[(page - 1) * limit: page * limit],
                page=page,
                limit=limit,
                total=len(result)
            )

        else:
            return result

    def get(self, **params):
        res = self._filter(**params)
        if len(res) == 0:
            return None
        return res[0]

    def exists(self, **params) -> bool:
        return len(self._filter(**params)) > 0

    def count(self, **params) -> int:
        return len(self._filter(**params))

    def update(
            self,
            pk_key,
            **params
    ) -> None:
        inst = self.get(id=pk_key)
        if inst:
            for key, value in params.items():
                inst.__dict__[key] = value


class PremisesReviewTestRepo(PremisesReviewAbstractRepo, TestRepo):

    @classmethod
    def _get_test_instances(cls) -> list:
        instances = [
            PremisesReviewEntity(
                id=uuid.UUID("81c173da-70cc-44f8-a7c6-42e13604af9c"),
                created_at=datetime.now(),
                updated_at=datetime.now(),
                author_id="some author_id",
                premises_id="some premises_id",
                order_id="some order_id",
                owner_id="some owner_id",
                is_visible=True,
                text="some text",
                storage_conditions=5,
                matches_the_description=5,
                accessibility=5,
                communication=5,
                is_counted=False
            ),
            PremisesReviewEntity(
                id=uuid.UUID("ed66c69f-8a70-4ffd-853b-84e2d7b588a0"),
                created_at=datetime.now(),
                updated_at=datetime.now(),
                author_id="some author_id",
                premises_id="some premises_id",
                order_id="some order_id",
                owner_id="some owner_id",
                is_visible=True,
                text="some text",
                storage_conditions=5,
                matches_the_description=5,
                accessibility=5,
                communication=5,
                is_counted=False
            )
        ]

        return instances


class PremisesReviewAnswerTestRepo(PremisesReviewAnswerAbstractRepo, TestRepo):
    ...


class UserReviewTestRepo(UserReviewAbstractRepo, TestRepo):

    @classmethod
    def _get_test_instances(cls) -> list:
        instances = [
            UserReviewEntity(
                id=uuid.UUID("56cb4cab-ec8b-4fe1-be16-c6c7e7f6499f"),
                created_at=datetime.now(),
                updated_at=datetime.now(),
                author_id="some author_id",
                premises_id="some premises_id",
                order_id="some order_id",
                about_user_id="some tenant_id",
                is_visible=True,
                text="some text"
            ),
            UserReviewEntity(
                id=uuid.UUID("18163f3a-8f12-4f0c-be53-44050629221f"),
                created_at=datetime.now(),
                updated_at=datetime.now(),
                author_id="some author_id",
                premises_id="some premises_id",
                order_id="some order_id",
                about_user_id="some tenant_id",
                is_visible=True,
                text="some text"
            )
        ]

        return instances


class UserReviewAnswerTestRepo(UserReviewAnswerAbstractRepo, TestRepo):
    ...


class ChaosReviewTestRepo(ChaosReviewAbstractRepo, TestRepo):

    def save_instances(self, instances: list, **params):
        for instance in instances:
            instance.id = uuid.uuid4()
            instance.storage_experience = ChaosReviewExperienceEnum.MUCH_BETTER

        self._instances += instances

    @classmethod
    def _get_test_instances(cls) -> list:
        instances = [
            ChaosReviewEntity(
                id=uuid.UUID("b07f17d4-0271-41b4-b783-32b3d1f5b194"),
                created_at=datetime.now(),
                updated_at=datetime.now(),
                author_id="some author_id",
                order_id="some order_id",
                text="some text",
                mark=5,
                storage_experience=ChaosReviewExperienceEnum.MUCH_BETTER
            )
        ]

        return instances


class PremisesRatingTestRepo(PremisesRatingAbstractRepo, TestRepo):

    @classmethod
    def _get_test_instances(cls) -> list:
        instances = [
            PremisesRatingEntity(
                id=uuid.UUID("98c444d4-78a5-4515-8a9b-0ff8a07056c6"),
                created_at=datetime.now(),
                updated_at=datetime.now(),
                premises_id="some premises_id",
                rating=5,
                count_reviews=0,
                storage_conditions=5,
                matches_the_description=5,
                accessibility=5,
                communication=5
            )
        ]

        return instances
