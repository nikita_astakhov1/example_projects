import unittest
# Disable warnings
import warnings

from chaos_grpc.base_pb2 import Response

from tests.requests import *
from tests.server import ReviewsServer


def warn(*args, **kwargs):
    pass


warnings.warn = warn


class BaseTestReviews(unittest.TestCase):
    reviews_server: ReviewsServer
    stub: ReviewsStub

    @classmethod
    def setUpClass(cls) -> None:
        cls.reviews_server = ReviewsServer.get_instance()
        cls.stub = cls.reviews_server.get_stub()

    def isResponseOkay(self, response: Response):
        if response.status != "200":
            info = \
                f"\n---------------------------" + \
                f"\nError response:" + \
                f"\n" + \
                f"\nStatus = '{response.status}'" + \
                f"\nCode = '{response.error.code}'" + \
                f"\nMessage = '{response.error.message}'" + \
                f"\n---------------------------"

            self.fail(info)


class TestChaosReviews(BaseTestReviews):

    def test_create_chaos_review(self):
        response = create_chaos_review(
            stub=self.stub
        )

        self.isResponseOkay(response)

    def test_get_chaos_reviews(self):
        response = get_chaos_reviews(
            stub=self.stub
        )

        self.isResponseOkay(response)


class TestPremisesReviews(BaseTestReviews):

    def test_create_premises_review(self):
        response = create_premises_review(
            stub=self.stub,
            order_id="1"
        )

        self.isResponseOkay(response)

    def test_get_all_premises_reviews(self):
        response = get_all_premises_reviews(
            stub=self.stub,
            page=1,
            limit=1,
            premises_id="some premises_id"
        )

        self.isResponseOkay(response)

    def test_get_premises_review_details(self):
        response = get_premises_review_details(
            stub=self.stub,
            review_id="81c173da-70cc-44f8-a7c6-42e13604af9c"
        )

        self.isResponseOkay(response)

    def test_create_premises_review_answer(self):
        response = create_premises_review_answer(
            stub=self.stub,
            review_id="81c173da-70cc-44f8-a7c6-42e13604af9c"
        )

        self.isResponseOkay(response)

    def test_get_premises_rating(self):
        response = get_premises_rating(
            stub=self.stub
        )

        self.isResponseOkay(response)

    def test_update_premises_rating(self):
        response = update_premises_rating(
            stub=self.stub
        )

        self.isResponseOkay(response)

    def test_inner_get_premises_rating(self):
        response = inner_get_premises_rating(
            stub=self.stub
        )

        self.isResponseOkay(response)

    def test_get_premises_reviews_by_owner(self):
        response = get_premises_reviews_by_owner(
            stub=self.stub,
            page=1,
            limit=1
        )

        self.isResponseOkay(response)


class TestUserReviews(BaseTestReviews):

    def test_create_user_review(self):
        response = create_user_review(
            stub=self.stub,
            order_id="1"
        )

        self.isResponseOkay(response)

    def test_create_user_review_answer(self):
        response = create_user_review_answer(
            stub=self.stub,
            review_id="56cb4cab-ec8b-4fe1-be16-c6c7e7f6499f",
            author_id="some tenant_id"
        )

        self.isResponseOkay(response)

    def test_get_all_user_reviews(self):
        response = get_all_user_reviews(
            stub=self.stub,
            page=1,
            limit=1,
            tenant_id="some tenant_id"
        )

        self.isResponseOkay(response)


# After completion must switch off everything manually
if __name__ == '__main__':
    unittest.main()
