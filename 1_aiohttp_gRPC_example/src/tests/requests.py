import uuid

from chaos_grpc.reviews.create_chaos_review.create_chaos_review_pb2 import \
    CreateChaosReviewRequest
from chaos_grpc.reviews.create_premises_review.create_premises_review_pb2 import \
    CreatePremisesReviewRequest
from chaos_grpc.reviews.create_premises_review_answer.create_premises_review_answer_pb2 import \
    CreatePremisesReviewAnswerRequest
from chaos_grpc.reviews.create_user_review.create_user_review_pb2 import \
    CreateUserReviewRequest
from chaos_grpc.reviews.create_user_review_answer.create_user_review_answer_pb2 import \
    CreateUserReviewAnswerRequest
from chaos_grpc.reviews.get_all_premises_review.get_all_premises_review_pb2 import \
    GetAllPremisesReviewRequest
from chaos_grpc.reviews.get_all_user_reviews.get_all_user_reviews_pb2 import \
    GetAllUserReviewsRequest
from chaos_grpc.reviews.get_chaos_reviews.get_chaos_reviews_pb2 import \
    GetChaosReviewsRequest
from chaos_grpc.reviews.get_premises_rating.get_premises_rating_pb2 import \
    GetPremisesRatingRequest
from chaos_grpc.reviews.get_premises_review_details.get_premises_review_details_pb2 import \
    GetPremisesReviewDetailsRequest
from chaos_grpc.reviews.get_premises_reviews_by_owner.get_premises_reviews_by_owner_pb2 import \
    GetPremisesReviewsByOwnerRequest
from chaos_grpc.reviews.inner.inner_get_premises_rating.inner_get_premises_rating_pb2 import \
    InnerGetPremisesRatingRequest
from chaos_grpc.reviews.review_utils_pb2 import *
from chaos_grpc.reviews.service_pb2_grpc import ReviewsStub
from chaos_grpc.reviews.update_premises_rating.update_premises_rating_pb2 import \
    UpdatePremisesRatingRequest

PRINT_ANSWER = False


def test_print_wrapper(func):
    def wrapper(*args, **kwargs):
        response = func(*args, **kwargs)

        if PRINT_ANSWER:
            print(
                f"\n---------------------------"
                f"\nResponse from server:"
                f"\n"
                f"\nStatus = '{response.status}'"
                f"\nCode = '{response.error.code}'"
                f"\nMessage = '{response.error.message}'"
                f"\nPayload = \n{response.payload}"
                f"\n---------------------------"
            )

        return response

    return wrapper


@test_print_wrapper
def get_premises_review_details(
        stub: ReviewsStub,
        review_id: str,
        request_id: str = str(uuid.uuid4())
):
    return stub.get_premises_review_details(
        GetPremisesReviewDetailsRequest(
            review_id=review_id,
            request_id=request_id
        )
    )


@test_print_wrapper
def get_all_premises_reviews(
        stub: ReviewsStub,
        page: int,
        limit: int,
        premises_id: str,
        request_id: str = str(uuid.uuid4())
):
    return stub.get_all_premises_review(
        GetAllPremisesReviewRequest(
            pagination=PaginationInfoRequest(
                page=page,
                limit=limit
            ),
            premises_id=premises_id,
            request_id=request_id
        )
    )


@test_print_wrapper
def create_premises_review(
        stub: ReviewsStub,
        storage_conditions: int = 5,
        matches_the_description: int = 5,
        accessibility: int = 5,
        communication: int = 5,
        text: str = "nice premises",
        order_id: str = "04d74bba-f75e-4e9a-a8b7-f28587a7da60",
        owner_id: str = "some owner_id",
        author_id: str = "some author_id",
        premises_id: str = "123e4567-e89b-12d3-a456-426614174000",
        request_id: str = str(uuid.uuid4())
):
    return stub.create_premises_review(
        CreatePremisesReviewRequest(
            marks=ReviewMarks(
                storage_conditions=storage_conditions,
                matches_the_description=matches_the_description,
                accessibility=accessibility,
                communication=communication,
            ),
            text=text,
            order_id=order_id,
            owner_id=owner_id,
            author_id=author_id,
            premises_id=premises_id,
            request_id=request_id
        )
    )


@test_print_wrapper
def get_premises_rating(
        stub: ReviewsStub,
        premises_id: str = "some premises_id",
        request_id: str = str(uuid.uuid4())
):
    return stub.get_premises_rating(
        GetPremisesRatingRequest(
            premises_id=premises_id,
            request_id=request_id
        )
    )


@test_print_wrapper
def update_premises_rating(
        stub: ReviewsStub,
        premises_id: str = "some premises_id",
        order_id: str = "1",
        request_id: str = str(uuid.uuid4())
):
    return stub.update_premises_rating(
        UpdatePremisesRatingRequest(
            premises_id=premises_id,
            order_id=order_id,
            request_id=request_id
        )
    )


@test_print_wrapper
def create_premises_review_answer(
        stub: ReviewsStub,
        review_id: str,
        text: str = "u too, dude",
        author_id: str = "some owner_id",
        request_id: str = str(uuid.uuid4())
):
    return stub.create_premises_review_answer(
        CreatePremisesReviewAnswerRequest(
            author_id=author_id,
            review_id=review_id,
            text=text,
            request_id=request_id
        )
    )


@test_print_wrapper
def get_all_user_reviews(
        stub: ReviewsStub,
        page: int,
        limit: int,
        tenant_id: str,
        request_id: str = str(uuid.uuid4())
):
    return stub.get_all_user_reviews(
        GetAllUserReviewsRequest(
            pagination=PaginationInfoRequest(
                page=page,
                limit=limit
            ),
            tenant_id=tenant_id,
            request_id=request_id
        )
    )


@test_print_wrapper
def create_user_review(
        stub: ReviewsStub,
        author_id: str = "some author_id",
        tenant_id: str = "some tenant_id",
        order_id: str = "1",
        premises_id: str = "some premises_id",
        text: str = "nice tenant",
        request_id: str = str(uuid.uuid4())
):
    return stub.create_user_review(
        CreateUserReviewRequest(
            author_id=author_id,
            tenant_id=tenant_id,
            order_id=order_id,
            premises_id=premises_id,
            text=text,
            request_id=request_id
        )
    )


@test_print_wrapper
def create_user_review_answer(
        stub: ReviewsStub,
        author_id: str = "some owner_id",
        review_id: str = "some review id",
        text: str = "nice user review",
        request_id: str = str(uuid.uuid4())
):
    return stub.create_user_review_answer(
        CreateUserReviewAnswerRequest(
            author_id=author_id,
            review_id=review_id,
            text=text,
            request_id=request_id
        )
    )


@test_print_wrapper
def create_chaos_review(
        stub: ReviewsStub,
        order_id: str = "1",
        text: str = "i love chaos",
        mark: int = 5,
        author_id: str = "some author_id",
        request_id: str = str(uuid.uuid4())
):
    return stub.create_chaos_review(
        CreateChaosReviewRequest(
            order_id=order_id,
            text=text,
            mark=mark,
            storage_experience=STORAGE_EXPERIENCE.ROUGHLY_EXPECTED,
            author_id=author_id,
            request_id=request_id
        )
    )


@test_print_wrapper
def get_chaos_reviews(
        stub: ReviewsStub,
        request_id: str = str(uuid.uuid4())
):
    return stub.get_chaos_reviews(
        GetChaosReviewsRequest(
            request_id=request_id
        )
    )


@test_print_wrapper
def inner_get_premises_rating(
        stub: ReviewsStub,
        premises_id: str = "some premises_id",
        request_id: str = str(uuid.uuid4())
):
    return stub.inner_get_premises_rating(
        InnerGetPremisesRatingRequest(
            premises_id=premises_id,
            request_id=request_id
        )
    )


@test_print_wrapper
def get_premises_reviews_by_owner(
        stub: ReviewsStub,
        page: int,
        limit: int,
        owner_id: str = "some owner_id",
        request_id: str = str(uuid.uuid4())
):
    return stub.get_premises_reviews_by_owner(
        GetPremisesReviewsByOwnerRequest(
            pagination=PaginationInfoRequest(
                page=page,
                limit=limit
            ),
            owner_id=owner_id,
            request_id=request_id
        )
    )


if __name__ == '__main__':
    import grpc

    channel = grpc.insecure_channel('localhost:8002')
    stub = ReviewsStub(channel)

    PRINT_ANSWER = True

    get_all_user_reviews(
        stub=stub,
        page=0,
        limit=0,
        tenant_id="some tenant_id"
    )
