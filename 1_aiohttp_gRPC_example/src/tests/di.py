import inject

from repositories.api import ApiAbstractRepository
from repositories.celery import CeleryAbstractRepository
from repositories.database import PremisesReviewAbstractRepo, UserReviewAbstractRepo, ChaosReviewAbstractRepo, \
    PremisesRatingAbstractRepo, PremisesReviewAnswerAbstractRepo, UserReviewAnswerAbstractRepo
from services.serialization import AbstractSerializationConfig, SerializationConfig
from services.serialization.grpc.serializers import serializers as grpc_serializers
from tests.api import ApiTestRepository
from tests.celery import CeleryTestRepository
from tests.database_repositories import PremisesReviewTestRepo, UserReviewTestRepo, ChaosReviewTestRepo, \
    PremisesRatingTestRepo, PremisesReviewAnswerTestRepo, UserReviewAnswerTestRepo

serializers = []
serializers += grpc_serializers


def DI():
    def di_configuration(binder):
        # Serialization
        binder.bind(AbstractSerializationConfig, SerializationConfig(serializers))

        # Database
        binder.bind(PremisesReviewAbstractRepo, PremisesReviewTestRepo())
        binder.bind(UserReviewAbstractRepo, UserReviewTestRepo())
        binder.bind(ChaosReviewAbstractRepo, ChaosReviewTestRepo())
        binder.bind(PremisesReviewAnswerAbstractRepo, PremisesReviewAnswerTestRepo())
        binder.bind(PremisesRatingAbstractRepo, PremisesRatingTestRepo())
        binder.bind(UserReviewAnswerAbstractRepo, UserReviewAnswerTestRepo())

        # Api
        binder.bind(ApiAbstractRepository, ApiTestRepository())

        # Celery
        binder.bind(CeleryAbstractRepository, CeleryTestRepository())

    inject.configure_once(di_configuration)
