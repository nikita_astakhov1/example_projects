from entities import User
from repositories.api import ApiAbstractRepository


class ApiTestRepository(ApiAbstractRepository):

    async def update_snapshot_premises_rating(self, premises_id: str, rating: float):
        pass

    async def is_premises_exists(self, premises_id: str) -> bool:
        return True

    async def has_user_order(self, user_id: str, order_id: str) -> bool:
        return True

    async def is_user_owner(self, user_id: str, premises_id: str) -> bool:
        return True

    async def get_users_info(self, user_ids: str) -> dict:
        result = {}
        for user_id in user_ids:
            result[user_id] = User(
                id=user_id,
                created_at="",
                firstname="",
                lastname="",
                about_me="",
                avatar_url=""
            )

        return result
