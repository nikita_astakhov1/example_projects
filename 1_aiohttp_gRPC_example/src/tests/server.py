import asyncio
from threading import Thread
from time import sleep
from typing import Any

import grpc

import constants
from chaos_grpc.reviews.service_pb2_grpc import ReviewsStub
from main import serve
from tests import di


constants.PRINT_REQUESTS = False


def start_service(address: str):
    # Start service using test environment(replace database repos and api services)
    # Inject our test beans over tests/di.py
    di.DI()

    loop = asyncio.get_event_loop()

    def run_server():
        loop.run_until_complete(
            serve(address)
        )

    Thread(
        target=run_server
    ).start()

    sleep(constants.TEST_SERVER_UPTIME)


class ReviewsServer:
    _instance: Any = None

    def _create_channel(self, address: str) -> None:
        self._channel = grpc.insecure_channel(address)

    def _create_stub(self) -> None:
        self._stub = ReviewsStub(self._channel)

    def __init__(self):
        # Start our service with test repos and services
        start_service(
            address=constants.TEST_SERVER_ADDRESS
        )

        # Init connection to our service
        self._create_channel(constants.TEST_SERVER_ADDRESS)
        self._create_stub()

    def get_stub(self) -> ReviewsStub:
        return self._stub

    def terminate(self) -> None:
        self._instance = None

    @classmethod
    def get_instance(cls):
        if cls._instance is None:
            cls._instance = ReviewsServer()

        return cls._instance
