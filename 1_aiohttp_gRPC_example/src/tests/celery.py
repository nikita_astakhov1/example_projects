from repositories.celery import CeleryAbstractRepository


class CeleryTestRepository(CeleryAbstractRepository):

    async def review_created(
            self,
            order_id: str,
            premises_id: str
    ):
        print(f"Send task 'worker.premises_or_tenant_review_created' order_id={order_id} premises_id={premises_id}")
