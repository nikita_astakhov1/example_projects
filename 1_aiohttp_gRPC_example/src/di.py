import inject

from constants import DEVELOPER_MODE
from repositories.api import ApiAbstractRepository, ApiRepository, ApiDeveloperRepository
from repositories.celery import CeleryAbstractRepository
from repositories.celery.celery import CeleryRepository, CeleryDeveloperRepository
from repositories.database import PremisesReviewAbstractRepo, UserReviewAbstractRepo, ChaosReviewAbstractRepo, \
    PremisesRatingAbstractRepo, PremisesReviewAnswerAbstractRepo, UserReviewAnswerAbstractRepo
from repositories.database.alchemy import PremisesReviewRepo, UserReviewRepo, ChaosReviewRepo, \
    PremisesRatingRepo, PremisesReviewAnswerRepo, UserReviewAnswerRepo
from services.serialization import AbstractSerializationConfig, SerializationConfig
from services.serialization.grpc.serializers import serializers as grpc_serializers

# All serializers that are inherits from AbstractSerializer
serializers = []
serializers += grpc_serializers


def DI():
    def di_configuration(binder):
        # Serialization
        binder.bind(AbstractSerializationConfig, SerializationConfig(serializers))

        # Database
        binder.bind(PremisesReviewAbstractRepo, PremisesReviewRepo())
        binder.bind(UserReviewAbstractRepo, UserReviewRepo())
        binder.bind(ChaosReviewAbstractRepo, ChaosReviewRepo())
        binder.bind(PremisesReviewAnswerAbstractRepo, PremisesReviewAnswerRepo())
        binder.bind(PremisesRatingAbstractRepo, PremisesRatingRepo())
        binder.bind(UserReviewAnswerAbstractRepo, UserReviewAnswerRepo())

        # Change Api and Celery to fake one, so real are not touched
        if DEVELOPER_MODE:
            print("------------------------------------------")
            print("DEVELOPER MODE: ON. NO REAL API OR CELERY REPO")
            print("------------------------------------------")
            # Api
            binder.bind(ApiAbstractRepository, ApiDeveloperRepository())

            # Celery
            binder.bind(CeleryAbstractRepository, CeleryDeveloperRepository())
        else:
            # Api
            binder.bind(ApiAbstractRepository, ApiRepository())

            # Celery
            binder.bind(CeleryAbstractRepository, CeleryRepository())

    inject.configure_once(di_configuration)
