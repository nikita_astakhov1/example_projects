from google.protobuf.json_format import MessageToDict

from chaos_grpc.reviews.update_premises_rating.update_premises_rating_pb2 import \
    UpdatePremisesRatingRequest, _UpdatePremisesRatingResponse
from entities.requests.update_premises_rating import UpdatePremisesRatingEntityRequest, \
    UpdatePremisesRatingEntityResponse
from services.serialization import AbstractSerializer


class UpdatePremisesRatingRequestToEntitySerializer(AbstractSerializer):
    input_type = UpdatePremisesRatingRequest
    output_type = UpdatePremisesRatingEntityRequest

    def convert(self, inp):
        return UpdatePremisesRatingEntityRequest(
            **MessageToDict(inp, preserving_proto_field_name=True, including_default_value_fields=True)
        )


class UpdatePremisesRatingEntityToResponseSerializer(AbstractSerializer):
    input_type = UpdatePremisesRatingEntityResponse
    output_type = _UpdatePremisesRatingResponse

    def convert(self, inp):
        return _UpdatePremisesRatingResponse(**inp.dict())
