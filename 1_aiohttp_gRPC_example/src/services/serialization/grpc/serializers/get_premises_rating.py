from google.protobuf.json_format import MessageToDict

from chaos_grpc.reviews.get_premises_rating.get_premises_rating_pb2 import \
    GetPremisesRatingRequest, _GetPremisesRatingResponse
from entities.requests.get_premises_rating import \
    GetPremisesRatingEntityRequest, GetPremisesRatingEntityResponse
from services.serialization import AbstractSerializer


class GetPremisesRatingRequestToEntitySerializer(AbstractSerializer):
    input_type = GetPremisesRatingRequest
    output_type = GetPremisesRatingEntityRequest

    def convert(self, inp):
        return GetPremisesRatingEntityRequest(
            **MessageToDict(inp, preserving_proto_field_name=True, including_default_value_fields=True)
        )


class GetPremisesRatingEntityToResponseSerializer(AbstractSerializer):
    input_type = GetPremisesRatingEntityResponse
    output_type = _GetPremisesRatingResponse

    def convert(self, inp):
        return _GetPremisesRatingResponse(**inp.dict())
