from google.protobuf.json_format import MessageToDict

from chaos_grpc.reviews.create_user_review_answer.create_user_review_answer_pb2 import \
    CreateUserReviewAnswerRequest, _CreateUserReviewAnswerResponse
from entities.requests.create_user_review_answer import CreateUserReviewAnswerEntityRequest, \
    CreateUserReviewAnswerEntityResponse
from services.serialization import AbstractSerializer


class CreateUserReviewAnswerRequestToEntitySerializer(AbstractSerializer):
    input_type = CreateUserReviewAnswerRequest
    output_type = CreateUserReviewAnswerEntityRequest

    def convert(self, inp):
        return CreateUserReviewAnswerEntityRequest(
            **MessageToDict(inp, preserving_proto_field_name=True, including_default_value_fields=True)
        )


class CreateUserReviewAnswerEntityToResponseSerializer(AbstractSerializer):
    input_type = CreateUserReviewAnswerEntityResponse
    output_type = _CreateUserReviewAnswerResponse

    def convert(self, inp):
        return _CreateUserReviewAnswerResponse(**inp.dict())
