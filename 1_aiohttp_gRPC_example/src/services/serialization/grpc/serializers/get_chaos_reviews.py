from google.protobuf.json_format import MessageToDict

from chaos_grpc.reviews.get_chaos_reviews.get_chaos_reviews_pb2 import \
    GetChaosReviewsRequest, _GetChaosReviewsResponse, _ChaosReview
from entities.requests.get_chaos_reviews import GetChaosReviewsEntityRequest, GetChaosReviewsEntityResponse
from services.serialization import AbstractSerializer


class GetChaosReviewsRequestToEntitySerializer(AbstractSerializer):
    input_type = GetChaosReviewsRequest
    output_type = GetChaosReviewsEntityRequest

    def convert(self, inp: GetChaosReviewsRequest) -> GetChaosReviewsEntityRequest:
        return GetChaosReviewsEntityRequest(
            **MessageToDict(inp, preserving_proto_field_name=True, including_default_value_fields=True)
        )


class GetChaosReviewsEntityToRequestSerializer(AbstractSerializer):
    input_type = GetChaosReviewsEntityResponse
    output_type = _GetChaosReviewsResponse

    def convert(self, inp: GetChaosReviewsEntityResponse) -> _GetChaosReviewsResponse:
        return _GetChaosReviewsResponse(
            reviews=[
                _ChaosReview(
                    author_id=review.author_id,
                    order_id=review.order_id,
                    created_at=review.created_at,
                    text=review.text,
                    mark=review.mark,
                    storage_experience=review.storage_experience.value
                )
                for review in inp.reviews
            ]
        )
