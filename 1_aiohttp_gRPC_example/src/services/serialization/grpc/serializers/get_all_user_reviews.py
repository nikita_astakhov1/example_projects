from google.protobuf.json_format import MessageToDict

from chaos_grpc.reviews.get_all_user_reviews.get_all_user_reviews_pb2 import GetAllUserReviewsRequest, \
    _PaginatedGetAllUserReviewsResponse
from entities.requests.get_all_user_reviews import GetAllUserReviewsEntityRequest, GetAllUserReviewsModelEntityResponse
from services.serialization import AbstractSerializer


class GetAllUserReviewsRequestToEntitySerializer(AbstractSerializer):
    input_type = GetAllUserReviewsRequest
    output_type = GetAllUserReviewsEntityRequest

    def convert(self, inp):
        return GetAllUserReviewsEntityRequest(
            **MessageToDict(inp, preserving_proto_field_name=True, including_default_value_fields=True)
        )


class GetAllUserReviewsEntityToResponseSerializer(AbstractSerializer):
    input_type = GetAllUserReviewsModelEntityResponse
    output_type = _PaginatedGetAllUserReviewsResponse

    def convert(self, inp):
        return _PaginatedGetAllUserReviewsResponse(**inp.dict())
