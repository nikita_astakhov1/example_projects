from google.protobuf.json_format import MessageToDict

from chaos_grpc.reviews.inner.inner_get_premises_rating.inner_get_premises_rating_pb2 import \
    InnerGetPremisesRatingRequest, _InnerGetPremisesRatingResponse
from entities.requests.inner_get_premises_rating import InnerGetPremisesRatingEntityRequest, \
    InnerGetPremisesRatingEntityResponse
from services.serialization import AbstractSerializer


class InnerGetPremisesRatingRequestToEntitySerializer(AbstractSerializer):
    input_type = InnerGetPremisesRatingRequest
    output_type = InnerGetPremisesRatingEntityRequest

    def convert(self, inp):
        return InnerGetPremisesRatingEntityRequest(
            **MessageToDict(inp, preserving_proto_field_name=True, including_default_value_fields=True)
        )


class InnerGetPremisesRatingEntityToResponseSerializer(AbstractSerializer):
    input_type = InnerGetPremisesRatingEntityResponse
    output_type = _InnerGetPremisesRatingResponse

    def convert(self, inp):
        return _InnerGetPremisesRatingResponse(**inp.dict())
