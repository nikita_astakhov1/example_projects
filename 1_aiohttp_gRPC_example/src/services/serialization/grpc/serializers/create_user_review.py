from google.protobuf.json_format import MessageToDict

from chaos_grpc.reviews.create_user_review.create_user_review_pb2 import CreateUserReviewRequest, \
    _CreateUserReviewResponse
from entities.requests.create_user_review import CreateUserReviewEntityRequest, CreateUserReviewEntityResponse
from services.serialization import AbstractSerializer


class CreateUserReviewRequestToEntitySerializer(AbstractSerializer):
    input_type = CreateUserReviewRequest
    output_type = CreateUserReviewEntityRequest

    def convert(self, inp):
        return CreateUserReviewEntityRequest(
            **MessageToDict(inp, preserving_proto_field_name=True, including_default_value_fields=True)
        )


class CreateUserReviewEntityToResponseSerializer(AbstractSerializer):
    input_type = CreateUserReviewEntityResponse
    output_type = _CreateUserReviewResponse

    def convert(self, inp):
        return _CreateUserReviewResponse(**inp.dict())
