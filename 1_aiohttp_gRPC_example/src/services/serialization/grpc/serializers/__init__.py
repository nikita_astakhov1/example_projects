from services.serialization.grpc.serializers.create_chaos_review import \
    CreateChaosReviewEntityToResponseSerializer, \
    CreateChaosReviewRequestToEntitySerializer
from services.serialization.grpc.serializers.create_premises_review import \
    CreatePremisesReviewEntityToResponseSerializer, \
    CreatePremisesReviewRequestToEntitySerializer
from services.serialization.grpc.serializers.create_premises_review_answer import \
    CreatePremisesReviewAnswerEntityToResponseSerializer, \
    CreatePremisesReviewAnswerRequestToEntitySerializer
from services.serialization.grpc.serializers.create_user_review import \
    CreateUserReviewEntityToResponseSerializer, \
    CreateUserReviewRequestToEntitySerializer
from services.serialization.grpc.serializers.create_user_review_answer import \
    CreateUserReviewAnswerEntityToResponseSerializer, \
    CreateUserReviewAnswerRequestToEntitySerializer
from services.serialization.grpc.serializers.get_all_premises_reviews import \
    GetAllPremisesReviewsEntityToRequestSerializer, \
    GetAllPremisesReviewsRequestToEntitySerializer
from services.serialization.grpc.serializers.get_all_user_reviews import \
    GetAllUserReviewsEntityToResponseSerializer, \
    GetAllUserReviewsRequestToEntitySerializer
from services.serialization.grpc.serializers.get_chaos_reviews import \
    GetChaosReviewsEntityToRequestSerializer, \
    GetChaosReviewsRequestToEntitySerializer
from services.serialization.grpc.serializers.get_premises_rating import \
    GetPremisesRatingEntityToResponseSerializer, \
    GetPremisesRatingRequestToEntitySerializer
from services.serialization.grpc.serializers.get_premises_review_details import \
    GetPremisesReviewDetailsRequestToEntitySerializer, \
    GetPremisesReviewDetailsEntityToResponseSerializer
from services.serialization.grpc.serializers.get_premises_reviews_by_owner import \
    GetPremisesReviewsByOwnerRequestToEntitySerializer, \
    GetPremisesReviewsByOwnerEntityToResponseSerializer
from services.serialization.grpc.serializers.inner_get_premises_rating import \
    InnerGetPremisesRatingEntityToResponseSerializer, \
    InnerGetPremisesRatingRequestToEntitySerializer
from services.serialization.grpc.serializers.update_premises_rating import \
    UpdatePremisesRatingEntityToResponseSerializer, \
    UpdatePremisesRatingRequestToEntitySerializer

serializers = [
    # Chaos Reviews
    GetChaosReviewsRequestToEntitySerializer,
    GetChaosReviewsEntityToRequestSerializer,

    CreateChaosReviewEntityToResponseSerializer,
    CreateChaosReviewRequestToEntitySerializer,

    # User Reviews
    CreateUserReviewEntityToResponseSerializer,
    CreateUserReviewRequestToEntitySerializer,

    GetAllUserReviewsRequestToEntitySerializer,
    GetAllUserReviewsEntityToResponseSerializer,

    CreateUserReviewAnswerRequestToEntitySerializer,
    CreateUserReviewAnswerEntityToResponseSerializer,

    # Premises Reviews
    CreatePremisesReviewRequestToEntitySerializer,
    CreatePremisesReviewEntityToResponseSerializer,

    GetAllPremisesReviewsRequestToEntitySerializer,
    GetAllPremisesReviewsEntityToRequestSerializer,

    GetPremisesReviewsByOwnerRequestToEntitySerializer,
    GetPremisesReviewsByOwnerEntityToResponseSerializer,

    GetPremisesReviewDetailsRequestToEntitySerializer,
    GetPremisesReviewDetailsEntityToResponseSerializer,

    CreatePremisesReviewAnswerRequestToEntitySerializer,
    CreatePremisesReviewAnswerEntityToResponseSerializer,

    GetPremisesRatingRequestToEntitySerializer,
    GetPremisesRatingEntityToResponseSerializer,

    UpdatePremisesRatingRequestToEntitySerializer,
    UpdatePremisesRatingEntityToResponseSerializer,

    # INNER
    InnerGetPremisesRatingRequestToEntitySerializer,
    InnerGetPremisesRatingEntityToResponseSerializer
]
