from google.protobuf.json_format import MessageToDict

from chaos_grpc.reviews.get_all_premises_review.get_all_premises_review_pb2 import \
    GetAllPremisesReviewRequest, _PaginatedGetAllPremisesReviewResponse
from entities.requests.get_all_premises_reviews import \
    GetAllPremisesReviewEntityRequest, PaginatedGetAllPremisesReviewEntityResponse
from services.serialization import AbstractSerializer


class GetAllPremisesReviewsRequestToEntitySerializer(AbstractSerializer):
    input_type = GetAllPremisesReviewRequest
    output_type = GetAllPremisesReviewEntityRequest

    def convert(self, inp):
        return GetAllPremisesReviewEntityRequest(
            **MessageToDict(inp, preserving_proto_field_name=True, including_default_value_fields=True)
        )


class GetAllPremisesReviewsEntityToRequestSerializer(AbstractSerializer):
    input_type = PaginatedGetAllPremisesReviewEntityResponse
    output_type = _PaginatedGetAllPremisesReviewResponse

    def convert(self, inp):
        return _PaginatedGetAllPremisesReviewResponse(**inp.dict())
