from chaos_grpc.reviews.create_chaos_review.create_chaos_review_pb2 import \
    CreateChaosReviewRequest, _CreateChaosReviewResponse
from constants import ChaosReviewExperienceEnum
from entities.requests.create_chaos_review import \
    CreateChaosReviewEntityRequest, CreateChaosReviewEntityResponse
from services.serialization import AbstractSerializer


class CreateChaosReviewRequestToEntitySerializer(AbstractSerializer):
    input_type = CreateChaosReviewRequest
    output_type = CreateChaosReviewEntityRequest

    def convert(self, inp):
        return CreateChaosReviewEntityRequest(
            order_id=inp.order_id,
            text=inp.text,
            mark=inp.mark,
            author_id=inp.author_id,
            storage_experience=ChaosReviewExperienceEnum(inp.storage_experience)
        )


class CreateChaosReviewEntityToResponseSerializer(AbstractSerializer):
    input_type = CreateChaosReviewEntityResponse
    output_type = _CreateChaosReviewResponse

    def convert(self, inp):
        return _CreateChaosReviewResponse(
            storage_experience=inp.storage_experience.value,
            **inp.dict(exclude={'storage_experience'})
        )
