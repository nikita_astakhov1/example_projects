from google.protobuf.json_format import MessageToDict

from chaos_grpc.reviews.get_premises_review_details.get_premises_review_details_pb2 import \
    GetPremisesReviewDetailsRequest, _GetPremisesReviewDetailsResponse
from entities.requests.get_premises_review_details import \
    GetPremisesReviewDetailsEntityRequest, GetPremisesReviewDetailsEntityResponse
from services.serialization import AbstractSerializer


class GetPremisesReviewDetailsRequestToEntitySerializer(AbstractSerializer):
    input_type = GetPremisesReviewDetailsRequest
    output_type = GetPremisesReviewDetailsEntityRequest

    def convert(self, inp):
        return GetPremisesReviewDetailsEntityRequest(
            **MessageToDict(inp, preserving_proto_field_name=True, including_default_value_fields=True)
        )


class GetPremisesReviewDetailsEntityToResponseSerializer(AbstractSerializer):
    input_type = GetPremisesReviewDetailsEntityResponse
    output_type = _GetPremisesReviewDetailsResponse

    def convert(self, inp):
        return _GetPremisesReviewDetailsResponse(**inp.dict())
