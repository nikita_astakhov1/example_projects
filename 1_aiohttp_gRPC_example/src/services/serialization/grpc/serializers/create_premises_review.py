from google.protobuf.json_format import MessageToDict

from chaos_grpc.reviews.create_premises_review.create_premises_review_pb2 import \
    CreatePremisesReviewRequest, _CreatePremisesReviewResponse
from entities.requests.create_premises_review import \
    CreatePremisesReviewEntityRequest, CreatePremisesReviewEntityResponse
from services.serialization import AbstractSerializer


class CreatePremisesReviewRequestToEntitySerializer(AbstractSerializer):
    input_type = CreatePremisesReviewRequest
    output_type = CreatePremisesReviewEntityRequest

    def convert(self, inp):
        return CreatePremisesReviewEntityRequest(
            **MessageToDict(inp, preserving_proto_field_name=True, including_default_value_fields=True)
        )


class CreatePremisesReviewEntityToResponseSerializer(AbstractSerializer):
    input_type = CreatePremisesReviewEntityResponse
    output_type = _CreatePremisesReviewResponse

    def convert(self, inp):
        return _CreatePremisesReviewResponse(**inp.dict())
