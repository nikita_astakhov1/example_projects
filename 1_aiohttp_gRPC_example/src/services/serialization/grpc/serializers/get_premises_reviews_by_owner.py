from google.protobuf.json_format import MessageToDict

from chaos_grpc.reviews.get_premises_reviews_by_owner.get_premises_reviews_by_owner_pb2 import \
    GetPremisesReviewsByOwnerRequest, _PaginatedGetPremisesReviewsByOwnerResponse
from entities.requests.get_premises_reviews_by_owner import \
    GetPremisesReviewsByOwnerEntityRequest, PaginatedGetPremisesReviewsByOwnerEntityResponse
from services.serialization import AbstractSerializer


class GetPremisesReviewsByOwnerRequestToEntitySerializer(AbstractSerializer):
    input_type = GetPremisesReviewsByOwnerRequest
    output_type = GetPremisesReviewsByOwnerEntityRequest

    def convert(self, inp):
        return GetPremisesReviewsByOwnerEntityRequest(
            **MessageToDict(inp, preserving_proto_field_name=True, including_default_value_fields=True)
        )


class GetPremisesReviewsByOwnerEntityToResponseSerializer(AbstractSerializer):
    input_type = PaginatedGetPremisesReviewsByOwnerEntityResponse
    output_type = _PaginatedGetPremisesReviewsByOwnerResponse

    def convert(self, inp):
        return _PaginatedGetPremisesReviewsByOwnerResponse(**inp.dict())
