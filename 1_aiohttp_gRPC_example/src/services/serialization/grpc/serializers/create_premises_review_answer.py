from google.protobuf.json_format import MessageToDict

from chaos_grpc.reviews.create_premises_review_answer.create_premises_review_answer_pb2 import \
    CreatePremisesReviewAnswerRequest, _CreatePremisesReviewAnswerResponse
from entities.requests.create_premises_review_answer import \
    CreatePremisesReviewAnswerEntityRequest, CreatePremisesReviewAnswerEntityResponse
from services.serialization import AbstractSerializer


class CreatePremisesReviewAnswerRequestToEntitySerializer(AbstractSerializer):
    input_type = CreatePremisesReviewAnswerRequest
    output_type = CreatePremisesReviewAnswerEntityRequest

    def convert(self, inp):
        return CreatePremisesReviewAnswerEntityRequest(
            **MessageToDict(inp, preserving_proto_field_name=True, including_default_value_fields=True)
        )


class CreatePremisesReviewAnswerEntityToResponseSerializer(AbstractSerializer):
    input_type = CreatePremisesReviewAnswerEntityResponse
    output_type = _CreatePremisesReviewAnswerResponse

    def convert(self, inp):
        return _CreatePremisesReviewAnswerResponse(**inp.dict())
