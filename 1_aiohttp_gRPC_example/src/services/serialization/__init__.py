import abc
from functools import wraps
from typing import Any, Callable, get_type_hints

import inject

from services.serialization.exceptions import *


class AbstractSerializer(abc.ABC):
    input_type: Type
    output_type: Type

    @abc.abstractmethod
    def convert(self, inp):
        ...

    @classmethod
    def get_sign(cls) -> (str, str):
        return (
            str(cls.input_type),
            str(cls.output_type)
        )


class AbstractSerializationConfig(abc.ABC):

    @abc.abstractmethod
    def convert(self, inp: Any, output_type: Type) -> Any:
        """Convert object"""


class SerializationConfig(AbstractSerializationConfig):
    _dict: dict

    def __init__(self, serializers: list):
        self._dict = SerializationConfig._generate_base_dict(serializers)

    @staticmethod
    def _generate_base_dict(serializers: list) -> dict:
        d = {}
        for serializer in serializers:
            d[serializer.get_sign()] = serializer()

        return d

    def convert(self, inp: Any, output_type: Type) -> Any:
        if type(inp) == output_type:
            return inp

        res = self._dict.get(
            (
                str(type(inp)),
                str(output_type)
            )
        )

        if res is None:
            raise SerializationMethodNotFoundException(input_type=type(inp), output_type=output_type)

        return res.convert(inp)


class AbstractSerializationService(abc.ABC):
    _instance = None

    @classmethod
    def get_instance(cls, *args, **kwargs):
        if cls._instance is None:
            cls._instance = cls(*args, **kwargs)

        return cls._instance

    @abc.abstractmethod
    def convert(self, input, output_type: Type):
        ...

    @abc.abstractmethod
    def serialize_method(self, func: Callable):
        """Decorator to deserialize input of function"""
        ...


class SerializableObject:

    def __init__(self, serialization_service: AbstractSerializationService, data: Any):
        self._serialization_service = serialization_service
        self._data = data

    def get(self):
        """Get data without serialization"""
        return self._data

    def to(self, output_type: Type):
        """Serialize data to output_type using serialization service"""
        return self._serialization_service.convert(
            input=self._data,
            output_type=output_type
        )


class SerializationService(AbstractSerializationService):
    _instance = None

    @inject.autoparams("config")
    def __init__(
            self,
            config: AbstractSerializationConfig
    ):
        self._config = config

    def convert(self, input, output_type: Type):
        return self._config.convert(
            inp=input,
            output_type=output_type
        )

    @staticmethod
    def _get_request_type(func: Callable) -> Type:
        t = get_type_hints(func).get("request")

        if t is None:
            raise RequestParamNotFoundException(func_name=func.__name__)

        return t

    def serialize_method(self, func: Callable):

        input_type = self._get_request_type(func)

        @wraps(func)
        async def wrapper(obj, request, *args, **kwargs) -> SerializableObject:
            return SerializableObject(
                serialization_service=self,
                data=await func(
                    obj,
                    self._config.convert(
                        inp=request,
                        output_type=input_type
                    ),
                    *args,
                    **kwargs
                )
            )

        return wrapper
