from typing import Type


class SerializationMethodNotFoundException(Exception):
    """Exception raises if there is no method for serialization"""

    def __init__(self, input_type: Type, output_type: Type):
        super().__init__(
            f"Serialization method for {str(input_type)} -> {str(output_type)} not found"
        )


class RequestParamNotFoundException(Exception):
    """Exception raises if there is no request param for serialization"""

    def __init__(self, func_name: str):
        super().__init__(
            f"Function {func_name} has no param 'request' for serialization. Just remove the decorator"
        )
