from exceptions import BaseGrpcException


class PremisesReviewNotFoundException(BaseGrpcException):
    def __init__(self, premises_review_id: str):
        super().__init__(
            message=f"Premises review id: {premises_review_id} not found",
            code="PREMISES_REVIEW_NOT_FOUND"
        )


class PremisesReviewExistsException(BaseGrpcException):
    def __init__(self, order_id: str):
        super().__init__(
            message=f"Premises review with order_id: {order_id} already exists",
            code="PREMISES_REVIEW_EXISTS"
        )


class ReviewAnswerExistsException(BaseGrpcException):
    def __init__(self, review_type: str, review_id: str):
        super().__init__(
            message=f"{review_type.capitalize()} review {review_id} already has answer",
            code="REVIEW_ANSWER_EXISTS"
        )


class PremisesNotExistsException(BaseGrpcException):
    def __init__(self, premises_id: str):
        super().__init__(
            message=f"Premises id: {premises_id} doesn't exists",
            code="PREMISES_NOT_EXISTS"
        )


class PremisesRatingNotFound(BaseGrpcException):
    def __init__(self, premises_id: str):
        super().__init__(
            message=f"Premises rating id: {premises_id}",
            code="PREMISES_RATING_NOT_FOUND"
        )


class UserNoAccessException(BaseGrpcException):
    def __init__(self, reason: str):
        super().__init__(
            message=f"User has no access to this operation: {reason}",
            code="USER_NO_ACCESS"
        )


class UserHasNotOrderException(UserNoAccessException):
    def __init__(self, user_id: str, order_id: str):
        super().__init__(
            reason=f"User id: {user_id} has not order id: {order_id}"
        )


class UserReviewExists(BaseGrpcException):
    def __init__(self, order_id: str):
        super().__init__(
            message=f"User review with order_id: {order_id} already exists",
            code="USER_REVIEW_EXISTS"
        )


class UserReviewNotExistsException(BaseGrpcException):
    def __init__(self, user_review_id: str):
        super().__init__(
            message=f"User review id: {user_review_id} not found",
            code="USER_REVIEW_NOT_FOUND"
        )


class ChaosReviewExists(BaseGrpcException):
    def __init__(self, order_id: str):
        super().__init__(
            message=f"Chaos review id: {order_id} already exists",
            code="CHAOS_REVIEW_EXISTS"
        )
