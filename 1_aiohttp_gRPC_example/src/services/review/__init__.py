import inject

from entities import PageEntity
from entities import PremisesRatingEntity, PremisesReviewEntity, PremisesReviewAnswerEntity, \
    UserReviewEntity, UserReviewAnswerEntity, ChaosReviewEntity
from entities.requests import ReviewMarks, PaginationInfoEntityResponse, ReviewAnswerEntity, Marks
from entities.requests.create_chaos_review import \
    CreateChaosReviewEntityResponse, CreateChaosReviewEntityRequest
from entities.requests.create_premises_review import \
    CreatePremisesReviewEntityRequest, CreatePremisesReviewEntityResponse
from entities.requests.create_premises_review_answer import \
    CreatePremisesReviewAnswerEntityRequest, CreatePremisesReviewAnswerEntityResponse
from entities.requests.create_user_review import \
    CreateUserReviewEntityRequest, CreateUserReviewEntityResponse
from entities.requests.create_user_review_answer import \
    CreateUserReviewAnswerEntityRequest, CreateUserReviewAnswerEntityResponse
from entities.requests.get_all_premises_reviews import \
    GetAllPremisesReviewEntityRequest, PaginatedGetAllPremisesReviewEntityResponse, _GetAllPremisesReviewEntityResponse, \
    _RatingInfoEntity, _PremisesReviewEntity
from entities.requests.get_all_user_reviews import \
    GetAllUserReviewsEntityRequest, GetAllUserReviewsModelEntityResponse, _UserReviewEntity
from entities.requests.get_chaos_reviews import \
    GetChaosReviewsEntityResponse, GetChaosReviewsEntityRequest, _ChaosReviewEntity
from entities.requests.get_premises_rating import \
    GetPremisesRatingEntityRequest, GetPremisesRatingEntityResponse
from entities.requests.get_premises_review_details import \
    GetPremisesReviewDetailsEntityRequest, GetPremisesReviewDetailsEntityResponse
from entities.requests.get_premises_reviews_by_owner import \
    GetPremisesReviewsByOwnerEntityRequest, PaginatedGetPremisesReviewsByOwnerEntityResponse, \
    _GetPremisesReviewsByOwnerEntityResponse, \
    _PremisesReviewOwnerEntity
from entities.requests.inner_get_premises_rating import \
    InnerGetPremisesRatingEntityRequest, InnerGetPremisesRatingEntityResponse
from entities.requests.update_premises_rating import \
    UpdatePremisesRatingEntityRequest, UpdatePremisesRatingEntityResponse
from repositories.api import ApiAbstractRepository
from repositories.celery import CeleryAbstractRepository
from repositories.database import PremisesReviewAbstractRepo, UserReviewAbstractRepo, ChaosReviewAbstractRepo, \
    UserReviewAnswerAbstractRepo, PremisesRatingAbstractRepo, PremisesReviewAnswerAbstractRepo
from services.review.exceptions import *
from services.serialization import SerializationService

serialization_service = SerializationService.get_instance()


def _get_pagination_entity(reviews) -> PaginationInfoEntityResponse:
    if isinstance(reviews, PageEntity):
        return PaginationInfoEntityResponse(
            page=reviews.page,
            max_page=reviews.max_page,
            count=reviews.count,
            total=reviews.total
        )

    return PaginationInfoEntityResponse(
        page=1,
        max_page=1,
        count=len(reviews),
        total=len(reviews)
    )


class ReviewService:

    @inject.autoparams(
        "premises_rep",
        "user_rep",
        "chaos_rep",
        "premises_answ_rep",
        "user_answ_rep",
        "premises_rating_rep",
        "api_rep",
        "celery_rep"
    )
    def __init__(
            self,
            premises_rep: PremisesReviewAbstractRepo,
            user_rep: UserReviewAbstractRepo,
            chaos_rep: ChaosReviewAbstractRepo,
            premises_answ_rep: PremisesReviewAnswerAbstractRepo,
            user_answ_rep: UserReviewAnswerAbstractRepo,
            premises_rating_rep: PremisesRatingAbstractRepo,
            api_rep: ApiAbstractRepository,
            celery_rep: CeleryAbstractRepository
    ):
        self._premises_rep = premises_rep
        self._user_rep = user_rep
        self._chaos_rep = chaos_rep
        self._premises_answ_rep = premises_answ_rep
        self._user_answ_rep = user_answ_rep
        self._premises_rating_rep = premises_rating_rep
        self._api_rep = api_rep
        self._celery_rep = celery_rep

    @serialization_service.serialize_method
    async def get_premises_review_details(self, request: GetPremisesReviewDetailsEntityRequest) -> \
            GetPremisesReviewDetailsEntityResponse:
        review = self._premises_rep.get(id=request.review_id, is_visible=True)

        if review is None:
            raise PremisesReviewNotFoundException(
                premises_review_id=str(request.review_id)
            )

        return GetPremisesReviewDetailsEntityResponse(
            id=review.id,
            author=(await self._api_rep.get_users_info([review.author_id]))[review.author_id],
            created_at=str(review.created_at),
            text=review.text,
            marks=ReviewMarks(
                storage_conditions=review.storage_conditions,
                matches_the_description=review.matches_the_description,
                accessibility=review.accessibility,
                communication=review.communication
            )
        )

    @serialization_service.serialize_method
    async def get_all_premises_reviews(self, request: GetAllPremisesReviewEntityRequest) -> \
            PaginatedGetAllPremisesReviewEntityResponse:

        reviews = self._premises_rep.list(
            page=request.pagination.page,
            limit=request.pagination.limit,
            premises_id=request.premises_id,
            is_visible=True
        )

        reviews_response = []
        total_reviews = 0

        # Getting premises rating
        rate = self._premises_rating_rep.get(premises_id=request.premises_id)

        if rate is None:
            raise PremisesRatingNotFound(premises_id=request.premises_id)

        if len(reviews) == 0:
            return PaginatedGetAllPremisesReviewEntityResponse(
                total_reviews=0,
                pagination=_get_pagination_entity(reviews),
                result=_GetAllPremisesReviewEntityResponse(
                    rating=_RatingInfoEntity(
                        rating=rate.rating,
                        marks=Marks(
                            storage_conditions=rate.storage_conditions,
                            matches_the_description=rate.matches_the_description,
                            accessibility=rate.accessibility,
                            communication=rate.communication
                        ),
                        count_reviews=rate.count_reviews
                    ),
                    reviews=[]
                )
            )

        authors = await self._api_rep.get_users_info(
            user_ids=list(set([review.author_id for review in reviews]))
        )
        for review in reviews:
            answ = self._premises_answ_rep.get(premises_review_id=review.id)
            answ_m = None
            if answ:
                answ_m = ReviewAnswerEntity(
                    text=answ.text,
                    review_id=review.id,
                    author_id=answ.author_id,
                    created_at=str(answ.created_at)
                )

            reviews_response.append(
                _PremisesReviewEntity(
                    id=review.id,
                    author=authors[review.author_id],
                    created_at=str(review.created_at),
                    text=str(review.text),
                    marks=ReviewMarks(
                        storage_conditions=review.storage_conditions,
                        matches_the_description=review.matches_the_description,
                        accessibility=review.accessibility,
                        communication=review.communication
                    ),
                    answer=answ_m
                )
            )

            total_reviews += \
                (
                        review.storage_conditions + review.matches_the_description + review.accessibility + review.communication
                ) / 4

        return PaginatedGetAllPremisesReviewEntityResponse(
            total_reviews=total_reviews,
            pagination=_get_pagination_entity(reviews),
            result=_GetAllPremisesReviewEntityResponse(
                rating=_RatingInfoEntity(
                    rating=rate.rating,
                    marks=Marks(
                        storage_conditions=rate.storage_conditions,
                        matches_the_description=rate.matches_the_description,
                        accessibility=rate.accessibility,
                        communication=rate.communication
                    ),
                    count_reviews=rate.count_reviews
                ),
                reviews=reviews_response
            )
        )

    @serialization_service.serialize_method
    async def get_premises_reviews_by_owner(self, request: GetPremisesReviewsByOwnerEntityRequest) -> \
            PaginatedGetPremisesReviewsByOwnerEntityResponse:
        reviews = self._premises_rep.list(
            page=request.pagination.page,
            limit=request.pagination.limit,
            is_visible=True,
            owner_id=request.owner_id
        )

        reviews_response = []
        total_reviews = 0

        # Getting list of premises to get their ratings
        premises_ids = list(set([review.premises_id for review in reviews]))
        rates = {}

        for premises_id in premises_ids:
            rate = self._premises_rating_rep.get(premises_id=premises_id)
            if rate is None:
                raise PremisesRatingNotFound(premises_id=premises_id)
            rates[premises_id] = rate.rating

        authors = await self._api_rep.get_users_info(
            user_ids=list(set([review.author_id for review in reviews]))
        )
        for review in reviews:
            answ = self._premises_answ_rep.get(premises_review_id=review.id)
            answ_m = None
            if answ:
                answ_m = ReviewAnswerEntity(
                    text=answ.text,
                    review_id=review.id,
                    author_id=answ.author_id,
                    created_at=str(answ.created_at)
                )

            reviews_response.append(
                _PremisesReviewOwnerEntity(
                    id=review.id,
                    author=authors[review.author_id],
                    created_at=str(review.created_at),
                    text=str(review.text),
                    marks=ReviewMarks(
                        storage_conditions=review.storage_conditions,
                        matches_the_description=review.matches_the_description,
                        accessibility=review.accessibility,
                        communication=review.communication
                    ),
                    answer=answ_m,
                    rating=rates[review.premises_id],
                    premises_id=review.premises_id
                )
            )

            total_reviews += \
                (
                        review.storage_conditions + review.matches_the_description + review.accessibility + review.communication
                ) / 4

        return PaginatedGetPremisesReviewsByOwnerEntityResponse(
            total_reviews=total_reviews,
            pagination=_get_pagination_entity(reviews),
            result=_GetPremisesReviewsByOwnerEntityResponse(
                reviews=reviews_response
            )
        )

    @serialization_service.serialize_method
    async def create_premises_review(self, request: CreatePremisesReviewEntityRequest) -> \
            CreatePremisesReviewEntityResponse:

        if self._premises_rep.exists(order_id=request.order_id):
            raise PremisesReviewExistsException(order_id=request.order_id)

        if not await self._api_rep.is_premises_exists(premises_id=request.premises_id):
            raise PremisesNotExistsException(premises_id=request.premises_id)

        if not await self._api_rep.has_user_order(user_id=request.author_id, order_id=request.order_id):
            raise UserHasNotOrderException(user_id=request.author_id, order_id=request.order_id)

        instance = PremisesReviewEntity(
            author_id=request.author_id,
            premises_id=request.premises_id,
            order_id=request.order_id,
            owner_id=request.owner_id,
            is_visible=True,
            text=request.text,
            storage_conditions=request.marks.storage_conditions,
            matches_the_description=request.marks.matches_the_description,
            accessibility=request.marks.accessibility,
            communication=request.marks.communication
        )

        instance = self._premises_rep.save_instances(instances=[instance])[0]

        self._celery_rep.review_created(
            order_id=instance.order_id,
            premises_id=instance.premises_id
        )

        return CreatePremisesReviewEntityResponse(
            id=instance.id,
            author_id=instance.author_id,
            order_id=instance.order_id,
            premises_id=instance.premises_id,
            created_at=str(instance.created_at),
            text=instance.text,
            marks=ReviewMarks(
                storage_conditions=instance.storage_conditions,
                accessibility=instance.accessibility,
                communication=instance.communication,
                matches_the_description=instance.matches_the_description
            )
        )

    @serialization_service.serialize_method
    async def get_premises_rating(self, request: GetPremisesRatingEntityRequest) -> \
            GetPremisesRatingEntityResponse:
        rate = self._premises_rating_rep.get(premises_id=request.premises_id)

        if not rate:
            raise PremisesRatingNotFound(premises_id=request.premises_id)

        return GetPremisesRatingEntityResponse(
            rating=rate.rating,
            count_reviews=rate.count_reviews,
            marks=Marks(
                storage_conditions=rate.storage_conditions,
                matches_the_description=rate.matches_the_description,
                accessibility=rate.accessibility,
                communication=rate.communication
            ),
        )

    @serialization_service.serialize_method
    async def update_premises_rating(self, request: UpdatePremisesRatingEntityRequest) -> \
            UpdatePremisesRatingEntityResponse:

        premises_review = self._premises_rep.get(
            order_id=request.order_id,
            premises_id=request.premises_id,
            is_counted=False
        )

        tenant_review = self._user_rep.exists(order_id=request.order_id, premises_id=request.premises_id)

        if premises_review and tenant_review:
            premises_review.is_counted = True
            self._premises_rep.update(pk_key=premises_review.id, is_counted=True)

            rating = self._premises_rating_rep.get(premises_id=request.premises_id)

            if rating:
                rating.storage_conditions = (
                                                    rating.storage_conditions * rating.count_reviews + premises_review.storage_conditions) \
                                            / (rating.count_reviews + 1)
                rating.matches_the_description = (
                                                         rating.matches_the_description * rating.count_reviews + premises_review.matches_the_description) \
                                                 / (rating.count_reviews + 1)
                rating.accessibility = (rating.accessibility * rating.count_reviews + premises_review.accessibility) \
                                       / (rating.count_reviews + 1)
                rating.communication = (rating.communication * rating.count_reviews + premises_review.communication) \
                                       / (rating.count_reviews + 1)

                rating.count_reviews += 1

                rating.rating = (
                                        rating.storage_conditions +
                                        rating.matches_the_description +
                                        rating.accessibility +
                                        rating.communication
                                ) / 4

                self._premises_rating_rep.update(
                    pk_key=rating.id,
                    **rating.dict(
                        exclude={'id', 'created_at', 'updated_at'}
                    )
                )

            else:
                rating = PremisesRatingEntity(
                    premises_id=request.premises_id,
                    count_reviews=1,
                    rating=(
                                   premises_review.storage_conditions +
                                   premises_review.matches_the_description +
                                   premises_review.accessibility +
                                   premises_review.communication
                           ) / 4,
                    storage_conditions=premises_review.storage_conditions,
                    matches_the_description=premises_review.matches_the_description,
                    accessibility=premises_review.accessibility,
                    communication=premises_review.communication
                )

                self._premises_rating_rep.save_instances(instances=[rating])

            await self._api_rep.update_snapshot_premises_rating(
                premises_id=request.premises_id,
                rating=rating.rating
            )

        return UpdatePremisesRatingEntityResponse()

    @serialization_service.serialize_method
    async def create_premises_review_answer(self, request: CreatePremisesReviewAnswerEntityRequest) -> \
            CreatePremisesReviewAnswerEntityResponse:

        premises_review = self._premises_rep.get(id=request.review_id)

        if not premises_review:
            raise PremisesReviewNotFoundException(
                premises_review_id=str(request.review_id)
            )

        if premises_review.owner_id != request.author_id:
            raise UserNoAccessException(
                reason=f"User {request.author_id} is not owner of {premises_review.premises_id}"
            )

        if self._premises_answ_rep.exists(premises_review_id=request.review_id):
            raise ReviewAnswerExistsException(
                review_type="premises",
                review_id=str(request.review_id)
            )

        review_answ = PremisesReviewAnswerEntity(
            author_id=request.author_id,
            text=request.text,
            premises_review_id=request.review_id
        )

        review_answ = self._premises_answ_rep.save_instances(instances=[review_answ])[0]

        return CreatePremisesReviewAnswerEntityResponse(
            author_id=review_answ.author_id,
            review_id=review_answ.premises_review_id,
            text=review_answ.text,
            created_at=str(review_answ.created_at)
        )

    @serialization_service.serialize_method
    async def get_all_user_reviews(self, request: GetAllUserReviewsEntityRequest) -> \
            GetAllUserReviewsModelEntityResponse:
        reviews = self._user_rep.list(
            page=request.pagination.page,
            limit=request.pagination.limit,
            about_user_id=request.tenant_id
        )

        reviews_m = []
        for review in reviews:
            answ = self._user_answ_rep.get(user_review_id=review.id)
            answ_m = None
            if answ:
                answ_m = ReviewAnswerEntity(
                    text=answ.text,
                    review_id=review.id,
                    author_id=answ.author_id,
                    created_at=str(answ.created_at)
                )

            reviews_m.append(
                _UserReviewEntity(
                    created_at=str(review.created_at),
                    text=review.text,
                    author_id=review.author_id,
                    answer=answ_m,
                    premises_id=review.premises_id
                )
            )

        return GetAllUserReviewsModelEntityResponse(
            pagination=_get_pagination_entity(reviews),
            result=reviews_m
        )

    @serialization_service.serialize_method
    async def create_user_review(self, request: CreateUserReviewEntityRequest) -> \
            CreateUserReviewEntityResponse:

        if self._user_rep.exists(order_id=request.order_id):
            raise UserReviewExists(order_id=request.order_id)

        if not await self._api_rep.is_premises_exists(premises_id=request.premises_id):
            raise PremisesNotExistsException(premises_id=request.premises_id)

        if not await self._api_rep.has_user_order(user_id=request.author_id, order_id=request.order_id):
            raise UserHasNotOrderException(user_id=request.author_id, order_id=request.order_id)

        if not await self._api_rep.is_user_owner(user_id=request.author_id, premises_id=request.premises_id):
            raise UserNoAccessException(
                reason=f"User id: {request.author_id} is not owner of premises id: {request.premises_id}"
            )

        user_review = UserReviewEntity(
            author_id=request.author_id,
            premises_id=request.premises_id,
            order_id=request.order_id,
            about_user_id=request.tenant_id,
            is_visible=True,
            text=request.text
        )

        user_review = self._user_rep.save_instances(instances=[user_review])[0]

        self._celery_rep.review_created(
            order_id=request.order_id,
            premises_id=request.premises_id
        )

        return CreateUserReviewEntityResponse(
            id=user_review.id,
            author_id=user_review.author_id,
            about_user_id=user_review.about_user_id,
            order_id=user_review.order_id,
            premises_id=user_review.premises_id,
            created_at=str(user_review.created_at),
            text=user_review.text
        )

    @serialization_service.serialize_method
    async def create_user_review_answer(self, request: CreateUserReviewAnswerEntityRequest) -> \
            CreateUserReviewAnswerEntityResponse:

        user_review = self._user_rep.get(id=request.review_id)

        if not user_review:
            raise UserReviewNotExistsException(
                user_review_id=str(request.review_id)
            )

        if request.author_id != user_review.about_user_id:
            raise UserNoAccessException(
                reason=f"User review {request.review_id} is not about user {request.author_id}"
            )

        if self._user_answ_rep.exists(user_review_id=request.review_id):
            raise ReviewAnswerExistsException(
                review_type="user",
                review_id=str(request.review_id)
            )

        review_answ = UserReviewAnswerEntity(
            author_id=request.author_id,
            text=request.text,
            user_review_id=request.review_id
        )

        review_answ = self._user_answ_rep.save_instances(instances=[review_answ])[0]

        return CreateUserReviewAnswerEntityResponse(
            author_id=review_answ.author_id,
            review_id=review_answ.user_review_id,
            text=review_answ.text,
            created_at=str(review_answ.created_at)
        )

    @serialization_service.serialize_method
    async def create_chaos_review(self, request: CreateChaosReviewEntityRequest) -> \
            CreateChaosReviewEntityResponse:

        if not await self._api_rep.has_user_order(user_id=request.author_id, order_id=request.order_id):
            raise UserNoAccessException(
                reason=f"User {request.author_id} has not order {request.order_id}"
            )

        if self._chaos_rep.exists(order_id=request.order_id):
            raise ChaosReviewExists(order_id=request.order_id)

        review = ChaosReviewEntity(
            author_id=request.author_id,
            order_id=request.order_id,
            text=request.text,
            mark=request.mark,
            storage_experience=request.storage_experience
        )

        self._chaos_rep.save_instances(instances=[review])

        return CreateChaosReviewEntityResponse(
            author_id=review.author_id,
            order_id=review.order_id,
            created_at=str(review.created_at),
            text=review.text,
            mark=review.mark,
            storage_experience=review.storage_experience
        )

    @serialization_service.serialize_method
    async def get_chaos_reviews(self, request: GetChaosReviewsEntityRequest) -> \
            GetChaosReviewsEntityResponse:

        reviews = self._chaos_rep.list()

        return GetChaosReviewsEntityResponse(
            reviews=[
                _ChaosReviewEntity(
                    author_id=review.author_id,
                    order_id=review.order_id,
                    created_at=str(review.created_at),
                    text=review.text,
                    mark=review.mark,
                    storage_experience=review.storage_experience
                )
                for review in reviews
            ]
        )

    @serialization_service.serialize_method
    async def inner_get_premises_rating(self, request: InnerGetPremisesRatingEntityRequest) -> \
            InnerGetPremisesRatingEntityResponse:
        rate = self._premises_rating_rep.get(premises_id=request.premises_id)

        if not rate:
            raise PremisesRatingNotFound(
                premises_id=request.premises_id
            )

        return InnerGetPremisesRatingEntityResponse(
            rating=rate.rating,
            count_reviews=rate.count_reviews,
            marks=Marks(
                storage_conditions=rate.storage_conditions,
                matches_the_description=rate.matches_the_description,
                accessibility=rate.accessibility,
                communication=rate.communication
            )
        )
