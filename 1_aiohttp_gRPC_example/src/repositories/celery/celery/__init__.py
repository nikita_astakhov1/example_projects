from repositories.celery import CeleryAbstractRepository
from repositories.celery.celery.client import CeleryClient


class CeleryRepository(CeleryAbstractRepository):

    def __init__(self):
        self._client = CeleryClient.get_client()

    def review_created(
            self,
            order_id: str,
            premises_id: str
    ):
        """Check created tenant and premises reviews for order, update their visibility and premises rating."""
        self._client.send_task(
            'worker.premises_or_tenant_review_created',
            args=(
                order_id,
                premises_id
            )
        )


class CeleryDeveloperRepository(CeleryAbstractRepository):

    async def review_created(
            self,
            order_id: str,
            premises_id: str
    ):
        print(f"Send task 'worker.premises_or_tenant_review_created' order_id={order_id} premises_id={premises_id}")
