import abc
from datetime import datetime
from threading import Thread
from time import sleep

from celery import Celery
from kombu.exceptions import OperationalError
from pydantic import BaseModel as Base

from constants import CELERY_BROKER

RETRY_SEND_TIME = 10


class _RequestBody(Base):
    args: list
    kwargs: dict


class AbstractCeleryClient(abc.ABC):

    @classmethod
    @abc.abstractmethod
    def get_client(cls):
        ...

    @abc.abstractmethod
    def send_task(self, *args, **kwargs) -> bool:
        ...


class CeleryClient(AbstractCeleryClient):
    _app: Celery = None

    def __init__(self):
        # Tasks that failed to send, awaiting reistablishing the connection
        self._waiting_tasks: list[_RequestBody] = []
        self._start_retry_send_thread()

    @staticmethod
    def _generate_app(celery_broker: str):
        return Celery(
            'tasks',
            broker=celery_broker,
            config_source={
                "BROKER_TRANSPORT_OPTIONS":
                    {
                        "max_retries": 3,
                        "interval_start": 0,
                        "interval_step": 0.2,
                        "interval_max": 0.5
                    }
            }
        )

    def _delete_sent_task(self, sent_tasks: list[_RequestBody]):
        for i in range(len(sent_tasks)):
            self._waiting_tasks.remove(sent_tasks[i])

    def _retry_send_thread(self):
        while True:
            if len(self._waiting_tasks) != 0:
                sent_tasks: list[_RequestBody] = []

                for task in self._waiting_tasks:
                    if self.send_task(
                            *task.args,
                            **task.kwargs,
                            retry_in_progress=True
                    ):
                        sent_tasks.append(task)
                    else:
                        break

                self._delete_sent_task(sent_tasks)

            sleep(RETRY_SEND_TIME)

    def _start_retry_send_thread(self):
        Thread(target=self._retry_send_thread).start()

    @classmethod
    def get_client(cls):
        if cls._app is None:
            cls._app = cls._generate_app(CELERY_BROKER)

        return CeleryClient()

    def send_task(self, *args, retry_in_progress: bool = False, **kwargs) -> bool:
        """
        :param retry_in_progress: Default is False, and if True, message will not be added to awaiting list.
        """
        try:
            self._app.send_task(*args, **kwargs)
            return True

        except OperationalError:
            print(f"{datetime.now()} broker is unreachable")

            if not retry_in_progress:
                self._waiting_tasks.append(
                    _RequestBody(
                        args=args,
                        kwargs=kwargs
                    )
                )

            return False
