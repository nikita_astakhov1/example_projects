import abc


class CeleryAbstractRepository(abc.ABC):
    @abc.abstractmethod
    def review_created(
            self,
            order_id: str,
            premises_id: str
    ):
        ...
