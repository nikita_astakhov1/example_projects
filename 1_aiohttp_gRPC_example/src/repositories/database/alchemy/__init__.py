from functools import wraps
from typing import List
from uuid import UUID

from sqlalchemy.orm.session import Session

from db import init_db_connection
from entities import BaseModelEntity
from entities import PremisesRatingEntity, PremisesReviewEntity, \
    UserReviewEntity, UserReviewAnswerEntity, PremisesReviewAnswerEntity, ChaosReviewEntity, \
    PageEntity
from models import PremisesReviewModel, UserReviewModel, ChaosReviewModel, PremisesRatingModel, \
    PremisesReviewAnswerModel, UserReviewAnswerModel
from repositories.database import AbstractRepo, PremisesReviewAbstractRepo, UserReviewAbstractRepo, \
    ChaosReviewAbstractRepo, PremisesReviewAnswerAbstractRepo, UserReviewAnswerAbstractRepo, PremisesRatingAbstractRepo
from repositories.database.alchemy.pagination import paginate

SessionMaker = init_db_connection()


def session_maker(func):
    @wraps(func)
    def wrapper(*args, **params):
        with SessionMaker() as session:
            return func(*args, **params, session=session)

    return wrapper


class AlchemyRepo(AbstractRepo):
    model = None
    entity = None

    @session_maker
    def update(
            self,
            session: Session,
            pk_key: UUID,
            **params
    ) -> None:
        session.query(self.model).filter_by(id=pk_key).update(params, synchronize_session="fetch")
        session.commit()

    @session_maker
    def save_instances(self, session: Session, instances: List[BaseModelEntity]) -> list[entity]:
        save_objects = [
            self.model(**instance.dict()) for instance in instances
        ]
        session.add_all(instances=save_objects)
        session.commit()

        for inst in save_objects:
            session.refresh(inst)

        return [
            self.entity(**instance.dict()) for instance in save_objects
        ]

    @session_maker
    def list(self, session: Session, page: int = 0, limit: int = 0, **params):
        q = session.query(self.model).filter_by(**params).order_by(self.model.created_at.desc())

        if page and limit:
            q = paginate(q, page=page, limit=limit)

            return PageEntity(
                page=q.page,
                limit=limit,
                total=q.total,
                items=[
                    self.entity(**i.dict()) for i in q
                ]
            )
        else:
            return [
                self.entity(**i.dict()) for i in q.all()
            ]

    @session_maker
    def get(self, session: Session, **params) -> entity:
        instance = session.query(self.model).filter_by(**params).order_by(self.model.created_at.desc()).first()
        if instance:
            return self.entity(**instance.dict())

        return None

    @session_maker
    def exists(self, session: Session, **params) -> bool:
        return session.query(session.query(self.model).filter_by(**params).exists()).scalar()

    @session_maker
    def count(self, session: Session, **params) -> int:
        return session.query(self.model).filter_by(**params).count()


class PremisesReviewRepo(PremisesReviewAbstractRepo, AlchemyRepo):
    model = PremisesReviewModel
    entity = PremisesReviewEntity


class PremisesReviewAnswerRepo(PremisesReviewAnswerAbstractRepo, AlchemyRepo):
    model = PremisesReviewAnswerModel
    entity = PremisesReviewAnswerEntity


class UserReviewRepo(UserReviewAbstractRepo, AlchemyRepo):
    model = UserReviewModel
    entity = UserReviewEntity


class UserReviewAnswerRepo(UserReviewAnswerAbstractRepo, AlchemyRepo):
    model = UserReviewAnswerModel
    entity = UserReviewAnswerEntity


class ChaosReviewRepo(ChaosReviewAbstractRepo, AlchemyRepo):
    model = ChaosReviewModel
    entity = ChaosReviewEntity


class PremisesRatingRepo(PremisesRatingAbstractRepo, AlchemyRepo):
    model = PremisesRatingModel
    entity = PremisesRatingEntity
