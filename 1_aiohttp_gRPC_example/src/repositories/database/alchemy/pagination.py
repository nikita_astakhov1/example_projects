import math


class PageIterator:

    def __init__(self, items):
        self.index = -1
        self.max = len(items)
        self.items = items

    def __next__(self):
        self.index += 1
        if self.index == self.max:
            raise StopIteration
        return self.items[self.index]


class Page:

    def __init__(self, items, page, limit, total):
        self.items = items
        self.page = page
        self.max_page = int(math.ceil(total / float(limit)))
        self.count = len(items)
        self.total = total

    def __iter__(self):
        return PageIterator(items=self.items)

    def __len__(self):
        return len(self.items)

    def __getitem__(self, index):
        return self.items[index]


def paginate(query, page, limit) -> Page:
    if page <= 0:
        raise AttributeError('page needs to be >= 1')
    if limit <= 0:
        raise AttributeError('limit needs to be >= 1')
    items = query.limit(limit).offset((page - 1) * limit).all()
    total = query.count()
    return Page(items, page, limit, total)
