import abc
from dataclasses import dataclass
from functools import wraps
from typing import Callable

import aiohttp
from aiohttp.client import ClientResponse


def session_maker(func: Callable) -> Callable:
    @wraps(func)
    async def wrapper(*args, **kwargs):
        async with aiohttp.ClientSession() as session:
            return await func(session=session, *args, **kwargs)

    return wrapper


@dataclass
class AsyncRestResponse:
    http_status: int
    status: int
    json: dict = None
    payload: dict = None
    error: dict = None

    def __init__(self, http_status: int):
        self.http_status = http_status
        self.status = http_status

    def is_okay(self) -> bool:
        return 200 <= self.status <= 300 and self.error is None

    def is_payload_empty(self) -> bool:
        return self.payload is None or self.payload == {}

    async def set_json(self, response: ClientResponse):
        if 200 <= self.http_status <= 300:
            self.json = await response.json()
            self.status = self.json.get('status')
            self.payload = self.json.get('payload')
            self.error = self.json.get('error')

    @staticmethod
    async def gen_response(response: ClientResponse):
        """
        Necessary because ClientResponse.json() is an asynchronous function that reads connections.
         It will be impossible to get an answer after closing.
        """
        r = AsyncRestResponse(response.status)
        await r.set_json(response)
        return r


class AbstractAsyncRestClient(abc.ABC):

    @abc.abstractmethod
    async def get(self, url: str, params: dict = None, **kwargs) -> AsyncRestResponse:
        ...

    @abc.abstractmethod
    async def post(self, url: str, params: dict = None, data: dict = None, **kwargs) -> AsyncRestResponse:
        ...

    @abc.abstractmethod
    async def put(self, url: str, params: dict = None, data: dict = None, **kwargs) -> AsyncRestResponse:
        ...

    @abc.abstractmethod
    async def delete(self, url: str, params: dict = None, data: dict = None, **kwargs) -> AsyncRestResponse:
        ...


class AsyncRestClient(AbstractAsyncRestClient):

    @session_maker
    async def get(self, session: aiohttp.ClientSession, url: str, params: dict = None) -> AsyncRestResponse:
        async with session.get(url, params=params) as resp:
            return await AsyncRestResponse.gen_response(resp)

    @session_maker
    async def post(self, session: aiohttp.ClientSession, url: str, params: dict = None,
                   data: dict = None) -> AsyncRestResponse:
        async with session.post(url, params=params, data=data) as resp:
            return await AsyncRestResponse.gen_response(resp)

    @session_maker
    async def put(self, session: aiohttp.ClientSession, url: str, params: dict = None,
                  data: dict = None) -> AsyncRestResponse:
        async with session.put(url, params=params, data=data) as resp:
            return await AsyncRestResponse.gen_response(resp)

    @session_maker
    async def delete(self, session: aiohttp.ClientSession, url: str, params: dict = None,
                     data: dict = None) -> AsyncRestResponse:
        async with session.delete(url, params=params, dict=dict) as resp:
            return await AsyncRestResponse.gen_response(resp)
