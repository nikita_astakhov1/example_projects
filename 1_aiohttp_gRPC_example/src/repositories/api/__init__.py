import abc
import datetime

from constants import API_URL
from entities import User
from repositories.api.client import AsyncRestClient


class ApiAbstractRepository(abc.ABC):

    @abc.abstractmethod
    async def get_users_info(self, user_ids: list):
        ...

    @abc.abstractmethod
    async def is_premises_exists(self, premises_id: str) -> bool:
        ...

    @abc.abstractmethod
    async def has_user_order(self, user_id: str, order_id: str) -> bool:
        ...

    @abc.abstractmethod
    async def is_user_owner(self, user_id: str, premises_id: str) -> bool:
        ...

    @abc.abstractmethod
    async def update_snapshot_premises_rating(self, premises_id: str, rating: float):
        ...


class ApiRepository(ApiAbstractRepository):

    def __init__(self):
        self._client = AsyncRestClient()

    async def update_snapshot_premises_rating(self, premises_id: str, rating: float):
        await self._client.post(
            url=API_URL + "/api/premises/v1/inner/update_premises_snapshot_rating/",
            data={
                "premises": premises_id,
                "rating": rating
            }
        )

    async def is_premises_exists(self, premises_id: str) -> bool:
        resp = await self._client.get(
            url=API_URL + f"/api/premises/v1/inner/premises_short_data/{premises_id}/",
        )
        return resp.is_okay() and not resp.is_payload_empty()

    async def has_user_order(self, user_id: str, order_id: str) -> bool:
        resp = await self._client.post(
            url=API_URL + "/api/order/v1/inner/get_order_obj_by_id/",
            data={
                "order_id": order_id
            }
        )

        return resp.is_okay() and (resp.payload.get('owner_id') == user_id or resp.payload.get('tenant_id') == user_id)

    async def is_user_owner(self, user_id: str, premises_id: str) -> bool:
        resp = await self._client.get(
            url=API_URL + f"/api/premises/v1/inner/premises_short_data/{premises_id}/",
        )

        return resp.is_okay() and resp.payload.get('owner_id') == user_id

    async def get_users_info(self, user_ids: list) -> dict:
        user_ids = list(set(user_ids))

        if len(user_ids) == 0:
            return {}

        resp = await self._client.get(
            url=API_URL + "/api/authentication/v1/users_profiles/",
            params={
                "ids": user_ids
            }
        )

        users = {}
        for user in resp.payload:
            users[user.get('id')] = User(
                id=user.get('id'),
                created_at=str(user.get('created_at')),
                firstname=user.get('firstname'),
                lastname=user.get('lastname'),
                about_me=user.get('about_me'),
                avatar_url=user.get('avatar')
            )

        return users


class ApiDeveloperRepository(ApiAbstractRepository):

    async def update_snapshot_premises_rating(self, premises_id: str, rating: float):
        pass

    async def is_premises_exists(self, premises_id: str) -> bool:
        return True

    async def has_user_order(self, user_id: str, order_id: str) -> bool:
        return True

    async def is_user_owner(self, user_id: str, premises_id: str) -> bool:
        return True

    async def get_users_info(self, user_ids: str) -> dict:
        result = {}
        for user_id in user_ids:
            result[user_id] = User(
                id=user_id,
                created_at=str(datetime.datetime.now()),
                firstname="",
                lastname="",
                about_me="",
                avatar_url=""
            )

        return result
