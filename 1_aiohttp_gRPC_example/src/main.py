import asyncio
import logging

import grpc
from chaos_grpc.reviews.service_pb2_grpc import add_ReviewsServicer_to_server

from constants import REVIEWS_ADDRESS


async def serve(address: str) -> None:
    from controllers import ReviewController

    server = grpc.aio.server()
    add_ReviewsServicer_to_server(
        ReviewController(),
        server
    )

    server.add_insecure_port(address)
    logging.info("Starting server on %s", address)
    print("Starting server on %s", address)

    await server.start()
    await server.wait_for_termination()


if __name__ == '__main__':
    import di

    logging.basicConfig(
        filename="review.log",
        format='%(asctime)s: %(thread)d - %(levelname)s - %(module)s - %(funcName)s - %(lineno)d - %(message)s',
        level=logging.DEBUG
    )

    di.DI()
    logging.basicConfig(level=logging.INFO)
    asyncio.run(
        serve(
            address=REVIEWS_ADDRESS
        )
    )
