from typing import List, Mapping, Optional, Type

from databases import Database
from sqlalchemy import Boolean, Column, DateTime, String, Table, Integer, Enum, and_
from sqlalchemy.sql import text
from app.db.db import BaseWarehouseDatabase
from app.models import UD
import enum


class WarehouseType(str, enum.Enum):
    self_storage = 'Self-storage'
    storage = 'Storage'


class AccessType(str, enum.Enum):
    qr = 'QR'
    staff = 'Staff'


class WarehouseClass(str, enum.Enum):
    a = 'A'
    b = 'B'
    c = 'C'
    d = 'D'


class WorkDays(str, enum.Enum):
    full = "Everyday"
    working = "Mon - Fri"


class WorkHourse(str, enum.Enum):
    full = "Non-stop"
    first = "8 - 17"
    second = "9 - 18"
    third = "10 - 19"


class SQLAlchemyBaseWarehouseTable:
    __tablename__ = "warehouse"

    id = Column(String, primary_key=True)
    name = Column(String, index=True, nullable=False)
    security = Column(Boolean, default=False, nullable=False)
    video_security = Column(Boolean, default=False, nullable=False)
    covered = Column(Boolean, default=True, nullable=False)
    heating = Column(Boolean, default=False, nullable=False)
    address = Column(String, nullable=False)
    phone = Column(String, nullable=False)
    days = Column(
        Enum(WorkDays), nullable=False,
        default=WorkDays.full
    )
    hours = Column(
        Enum(WorkHourse), nullable=False,
        default=WorkHourse.full
    )
    is_active = Column(Boolean, default=True, nullable=False)
    created_at = Column(DateTime)
    created_by = Column(String, nullable=False)
    sorting_weight = Column(Integer, nullable=False, default=1)
    warehouse_type = Column(
        Enum(WarehouseType), nullable=False,
        default=WarehouseType.self_storage
    )
    access_type = Column(
        Enum(AccessType), nullable=False,
        default=AccessType.qr
    )
    warehouse_class = Column(
        Enum(WarehouseClass), nullable=False,
        default=WarehouseClass.a
    )
    total_capacity = Column(Integer, nullable=False, default=100)
    free_capacity = Column(Integer, nullable=False, default=100)
    total_cells = Column(Integer, nullable=False, default=1)
    free_cells = Column(Integer, nullable=False, default=1)
    partner_id = Column(String, nullable=False)


class SQLAlchemyWarehouseDatabase(BaseWarehouseDatabase[UD]):
    """
    Database adapter for SQLAlchemy.

    :param warehouse_db_model: Pydantic model of a DB representation of a user.
    :param database: `Database` instance.
    :param warehouse: SQLAlchemy users table instance.
    """

    database: Database
    warehouses: Table

    def __init__(
        self,
        warehouse_db_model: Type[UD],
        database: Database,
        warehouses: Table
    ):
        super().__init__(warehouse_db_model)
        self.database = database
        self.warehouses = warehouses

    async def list(
            self, video_security: Optional[bool] = None, security: Optional[bool] = None,
            covered: Optional[bool] = None, heating: Optional[bool] = None
        ) -> List[UD]:
        parameters = list()
        if video_security is not None:
            parameters.append(text("video_security = %s" % (video_security)))
        if security is not None:
            parameters.append(text("security = %s" % (security)))
        if covered is not None:
            parameters.append(text("covered = %s" % (covered)))
        if heating is not None:
            parameters.append(text("heating = %s" % (heating)))
        if parameters:  
            query = self.warehouses.select().where((and_(*parameters)))
        else:
            query = self.warehouses.select()
        warehouses = await self.database.fetch_all(query)
        return [await self._make_warehouse(warehouse) for warehouse in warehouses]

    async def get(self, id: str) -> Optional[UD]:
        query = self.warehouses.select().where(self.warehouses.c.id == id)
        warehouse = await self.database.fetch_one(query)
        return await self._make_warehouse(warehouse) if warehouse else None

    async def create(self, warehouse: UD) -> UD:
        warehouse_dict = warehouse.dict()
        query = self.warehouses.insert()
        await self.database.execute(query, warehouse_dict)
        return warehouse

    async def update(self, warehouse: UD) -> UD:
        warehouse_dict = warehouse.dict()
        query = self.warehouses.update().where(
            self.warehouses.c.id == warehouse.id
        ).values(warehouse_dict)
        await self.database.execute(query)
        return warehouse

    async def _make_warehouse(self, warehouse: Mapping) -> UD:
        warehouse_dict = {**warehouse}
        return self.warehouse_db_model(**warehouse_dict)
