from typing import Generic, List, Optional, Type
from app.models import UD


class BaseWarehouseDatabase(Generic[UD]):
    """
    Base adapter for retrieving, creating and updating users from a database.

    :param user_db_model: Pydantic model of a DB representation of a user.
    """

    warehouse_db_model: Type[UD]

    def __init__(self, warehouse_db_model: Type[UD]):
        self.warehouse_db_model = warehouse_db_model

    async def list(
            self, video_security: Optional[bool] = None, security: Optional[bool] = None,
            covered: Optional[bool] = None, heating: Optional[bool] = None
        ) -> List[UD]:
        raise NotImplementedError()

    async def get(self, id: str) -> Optional[UD]:
        raise NotImplementedError()

    async def create(self, warehouse: UD) -> UD:
        raise NotImplementedError()

    async def update(self, warehouse: UD) -> UD:
        raise NotImplementedError()

    async def delete(self, warehouse: UD) -> None:
        raise NotImplementedError()