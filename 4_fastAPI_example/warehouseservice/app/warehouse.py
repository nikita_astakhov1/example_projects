from collections import defaultdict
from typing import (
    Callable, DefaultDict, List, Type
)

from app import models
from app.db import BaseWarehouseDatabase
from app.router import (
    Event,
    EventHandlersRouter,
    get_warehouse_router
)
from fastapi import FastAPI


class Warehouses:
    """
    Main object that ties together the component for warehouse.
    
    :param app: FastApi instance
    :param db: Database adapter instance.
    :param warehouse_model: Pydantic model of a warehouse.
    :param warehouse_create_model: Pydantic model for creating a warehouse.
    :param warehouse_update_model: Pydantic model for updating a warehouse.
    :param warehouse_db_model: Pydantic model of a DB representation of a warehouse.

    :attribute router: Router exposing warehouse crud routes.
    """
    app: FastAPI
    db: BaseWarehouseDatabase
    router: EventHandlersRouter
    _warehouse_db_model: Type[models.BaseWarehouseDB]
    _event_handlers: DefaultDict[Event, List[Callable]]

    def __init__(
        self,
        app: FastAPI,
        db: BaseWarehouseDatabase,
        warehouse_model: Type[models.BaseWarehouse],
        warehouse_create_model: Type[models.BaseWarehouseCreate],
        warehouse_update_model: Type[models.BaseWarehouseUpdate],
        warehouse_db_model: Type[models.BaseWarehouseDB]
    ):
        self.app = app
        self.db = db
        self.router = get_warehouse_router(
            self.db,
            warehouse_model,
            warehouse_create_model,
            warehouse_update_model,
            warehouse_db_model
        )
        self._warehouse_db_model = warehouse_db_model
        self._event_handlers = defaultdict(list)
        self.app.include_router(
            self.router, prefix='/api/v1/warehouse',
            tags=['warehouse']
        )

    def on_after_create(self) -> Callable:
        return self._on_event(Event.ON_AFTER_CREATE)

    def on_after_update(self) -> Callable:
        return self._on_event(Event.ON_AFTER_UPDATE)

    def _on_event(self, event_type: Event) -> Callable:
        def decorator(func: Callable) -> Callable:
            self._event_handlers[event_type].append(func)
            self.router.add_event_handler(event_type, func)
            return func

        return decorator
