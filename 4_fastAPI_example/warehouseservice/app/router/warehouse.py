from typing import Any, Dict, List, Type, cast, Optional
from datetime import datetime
from fastapi import Depends, HTTPException
from starlette import status
from starlette.requests import Request

from app import models
from app.db import BaseWarehouseDatabase
from app.router.common import (
    ErrorCode, Event, EventHandlersRouter
)


def get_warehouse_router(
    warehouse_db: BaseWarehouseDatabase[models.BaseWarehouseDB],
    warehouse_model: Type[models.BaseWarehouse],
    warehouse_create_model: Type[models.BaseWarehouseCreate],
    warehouse_update_model: Type[models.BaseWarehouseUpdate],
    warehouse_db_model: Type[models.BaseWarehouseDB]
) -> EventHandlersRouter:
    router = EventHandlersRouter()

    async def _get_or_404(id: str) -> models.BaseWarehouseDB:
        house = await warehouse_db.get(id)
        if house is None or not house.is_active:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND)
        return house

    async def _update_warehouse(house: models.BaseWarehouseDB, update_dict: Dict[str, Any]):
        for field in update_dict:
            setattr(house, field, update_dict[field])
        return await warehouse_db.update(house)

    @router.post(
        "/", response_model=warehouse_model, status_code=status.HTTP_201_CREATED
    )
    async def create(request: Request, house: warehouse_create_model):
        house = cast(models.BaseWarehouseCreate, house)

        db_house = warehouse_db_model(
            **house.create_dict(), created_at=datetime.now()
        )
        created_house = await warehouse_db.create(db_house)

        await router.run_handlers(
            Event.ON_AFTER_CREATE, created_house,
            request
        )
        return created_house

    @router.get(
        "/",
        response_model=List[warehouse_model]
    )
    async def list_warehouses(
            video_security: Optional[bool] = None, security: Optional[bool] = None,
            covered: Optional[bool] = None, heating: Optional[bool] = None
        ):
        return await warehouse_db.list(video_security, security, covered, heating)

    @router.get(
        "/{id}/",
        response_model=warehouse_model,
    )
    async def get_warehouse(id: str):
        return await _get_or_404(id)

    @router.patch(
        "/{id}/",
        response_model=warehouse_model
    )
    async def update_warehouse(
        id: str, updated_warehouse: warehouse_update_model,
    ):
        updated_warehouse = cast(
            models.BaseWarehouseUpdate, updated_warehouse,
        )
        house = await _get_or_404(id)
        updated_warehouse_data = updated_warehouse.create_update_dict()
        return await _update_warehouse(house, updated_warehouse_data)

    @router.delete(
        "/{id}/",
        status_code=status.HTTP_204_NO_CONTENT
    )
    async def delete_warehouse(id: str):
        house = await _get_or_404(id)
        return await _update_warehouse(house, {"is_active": False})

    return router
