from functools import lru_cache

from pydantic import BaseSettings, AnyHttpUrl


class APPSettings(BaseSettings):
    project_name: str = 'WarehouseService'
    debug: bool = True
    database_uri: str = "postgresql://postgres:admin@localhost/warehouses"

    class Config:
        env_file = 'app/.env'


@lru_cache()
def get_app_settings() -> APPSettings:
    return APPSettings()
