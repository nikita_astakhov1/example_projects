__version__ = "0.1.0"

import databases
import sqlalchemy
from sqlalchemy.ext.declarative import DeclarativeMeta, declarative_base
from fastapi import FastAPI
from app import models
from app.db import SQLAlchemyBaseWarehouseTable, SQLAlchemyWarehouseDatabase

from app.warehouse import Warehouses
from app.config import get_app_settings

config = get_app_settings()

class Warehouse(models.BaseWarehouse):
    pass


class WarehouseCreate(Warehouse, models.BaseWarehouseCreate):
    pass


class WarehouseUpdate(Warehouse, models.BaseWarehouseUpdate):
    pass


class WarehouseDB(Warehouse, models.BaseWarehouseDB):
    pass


DATABASE_URL = config.database_uri
database = databases.Database(DATABASE_URL)

Base: DeclarativeMeta = declarative_base()


class WarehouseTable(Base, SQLAlchemyBaseWarehouseTable):
    pass


engine = sqlalchemy.create_engine(DATABASE_URL)

Base.metadata.create_all(engine)

houses = WarehouseTable.__table__
warehouse_db = SQLAlchemyWarehouseDatabase(WarehouseDB, database, houses)

app = FastAPI(
    openapi_url="/api/v1/warehouse/openapi.json",
    docs_url="/api/v1/warehouse/docs"
)


@app.on_event("startup")
async def startup():
    await database.connect()


@app.on_event("shutdown")
async def shutdown():
    await database.disconnect()


Warehouses = Warehouses(
    app=app, db=warehouse_db, warehouse_model=Warehouse, warehouse_create_model=WarehouseCreate,
    warehouse_update_model=WarehouseUpdate, warehouse_db_model=WarehouseDB
)
