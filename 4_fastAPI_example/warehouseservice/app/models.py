import uuid
from typing import Optional, TypeVar
from datetime import datetime

import pydantic
from pydantic import BaseModel
from enum import Enum


class TypeEnum(str, Enum):
    self_storage = 'Self-storage'
    storage = 'Storage'


class AccessTypeEnum(str, Enum):
    qr = 'QR'
    staff = 'Staff'


class WarehouseClassEnum(str, Enum):
    a = 'A'
    b = 'B'
    c = 'C'
    d = 'D'


class WorkDaysEnum(str, Enum):
    full = "Everyday"
    working = "Mon - Fri"


class WorkHourseEnum(str, Enum):
    full = "Non-stop"
    first = "8 - 17"
    second = "9 - 18"
    third = "10 - 19"


class BaseWarehouse(BaseModel):

    id: Optional[str] = None
    name: Optional[str] = None
    security: Optional[bool] = False
    video_security: Optional[bool] = False
    covered: Optional[bool] = True
    heating: Optional[bool] = False
    address: Optional[str] = None
    phone: Optional[str] = None
    sorting_weight: Optional[int] = 1
    is_active: Optional[bool] = True
    created_at: Optional[datetime] = None
    created_by: Optional[str] = None
    warehouse_type: Optional[TypeEnum] = TypeEnum.self_storage
    access_type: Optional[AccessTypeEnum] = AccessTypeEnum.qr
    warehouse_class: Optional[WarehouseClassEnum] = WarehouseClassEnum.a
    hours: Optional[WorkHourseEnum] = WorkHourseEnum.full
    days: Optional[WorkDaysEnum] = WorkDaysEnum.full
    total_capacity: Optional[int] = 100
    free_capacity: Optional[int] = 100
    total_cells: Optional[int] = 1
    free_cells: Optional[int] = 1
    partner_id: Optional[str] = None

    @pydantic.validator("id", pre=True, always=True)
    def default_id(cls, v):
        return v or str(uuid.uuid4())

    def create_dict(self):
        return self.dict(
            exclude={"id", "is_active", "created_at"},
        )

    def create_update_dict(self):
        return self.dict(
            exclude_unset=True,
            exclude={"id", "is_active", "created_at", "partner_id"},
        )


class BaseWarehouseCreate(BaseWarehouse):
    pass


class BaseWarehouseUpdate(BaseWarehouse):
    pass


class BaseWarehouseDB(BaseWarehouse):
    pass

    class Config:
        orm_mode = True


UD = TypeVar("UD", bound=BaseWarehouse)
