# Code examples by Nikita Sheverdin


## About

Welcome to my code repository, a showcase of my coding skills across various technical domains.
Within, you will find selected examples from my diverse project portfolio, written using Python and various frameworks.

All examples are parts of production code; in some cases, business logic is significantly removed, as well as deployment settings. Each of them was written with a different amount of available resources, so the quality can vary.

### Repository Highlights

1. aiohttp_gRPC_example: The most interesting one. A microservice designed to process user reviews. Tech stack: Python, aiohttp, gRPC, SQLAlchemy, Alembic.
2. flask_example: Here is an interesting example of pure SQL queries. Most of the business logic is deleted. Tech stack: Python, flask, SQL, SQLAlchemy.
3. gRPC_external_APIs_example: Here is a basic implementation of working with APIs from AssemblyAI and OpenAI. The first one creates transcriptions from video files, and the second one generates summarizations. Tech stack: Python, gRPC, SQLAlchemy, Alembic.
4. fastAPI_example: The implementation of a basic microservice with FastAPI. Tech stack: Python, FastAPI, SQLAlchemy, Alembic.

### Usage and Contact

This repository serves as a showcase of my work and is not intended for public use without prior authorization.
I warmly invite those interested in utilizing or building upon these code examples to reach out to me directly.
